// import 'core-js/es6/map';
// import 'core-js/es6/set';
import 'raf/polyfill';
import 'babel-polyfill';
import 'core-js/client/shim';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { addLocaleData } from 'react-intl';
import Main from './main';
import store from './config/store';
import registerServiceWorker from './services/registerServiceWorker';
import * as en from 'react-intl/locale-data/en';
import * as da from 'react-intl/locale-data/da';
import './styles/style.scss';
// import { LicenseManager } from 'ag-grid-enterprise/main';
// LicenseManager.setLicenseKey('Evaluation_License_Valid_Until_4_July_2018__MTUzMDY1ODgwMDAwMA==e5bda1e1a8dc6e37ae41df70aeaffb86');

addLocaleData([...en, ...da]);

ReactDOM.render(
	<Provider store={store}>
		<Main />
	</Provider>,
	document.getElementById('root')
);

registerServiceWorker();
