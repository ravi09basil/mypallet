import * as React from 'react';
import { AgGridReact, AgGridReactProps } from 'ag-grid-react';
import './style.scss';

class AgGeidTheme extends React.Component<AgGridReactProps> {
	render() {
		let props = this.props;
		return <AgGridReact {...props} />;
	}
}

export default AgGeidTheme;
