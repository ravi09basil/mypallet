import * as React from 'react';
import { Field, FieldApi } from 'react-form';

class InputFile extends React.Component<Props> {
	onChange(e: React.ChangeEvent<HTMLInputElement>, fieldApi: FieldApi) {
		// SET THE VALUE
		fieldApi.setValue(e.target.value);
		// CHECK IF THE USER HAVE ON CHANGE EVENT
		if (this.props.onChange) {
			this.props.onChange(e);
		}
	}

	render() {
		const { onChange, field, ...rest } = this.props;
		return (
			<Field field={field} defaultValue={this.props.value}>
				{fieldApi => {
					return (
						<input {...rest} value={fieldApi.value || ''} type="file" onChange={e => { this.onChange(e, fieldApi); }} />
					);
				}}
			</Field>
		);
	}
}

export default InputFile;

interface Props extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
	field: string;
}
