import * as React from 'react';
import './style.scss';
import { Lightbox } from 'react-modal-image';
import { FormattedMessage } from 'react-intl';

class InfoCard extends React.Component<Props, State> {
	state: State = {
		isOpen: false,
	};

	render() {
		return (
			<div className="info-holder">
				<div className="info-title">{this.props.title}</div>
				<div className="media info-info">
					<div className="media-left info-img-holder">
						<i className={this.props.icon} />
					</div>
					{this.props.valueIsImage && this.props.value ?
						<div className="media-body info-name">
							<a onClick={() => this.setState({ isOpen: true })}>
								<FormattedMessage id="comps.infoCard.seeConsignmentNote" />
							</a>
							{this.state.isOpen && (<Lightbox
								medium={this.props.value}
								large={this.props.value}
								onClose={() => this.setState({ isOpen: false })}
							/>)}
						</div>
						:
						<div className="media-body info-name">
							{this.props.value ? this.props.value : this.props.defaultText}
						</div>
					}
				</div>
			</div>
		);
	}
}

export default InfoCard;

interface State {
	isOpen: boolean;
}

interface Props {
	title: string;
	defaultText: string;
	value: string;
	icon: string;
	valueIsImage: boolean;
}
