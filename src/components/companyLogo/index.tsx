import * as React from 'react';
import './style.scss';

class CompanyLogo extends React.Component<Props> {
	render() {
		return (
			<div className="company-image">
				{this.props.companyLogo ? <div className="company-img-holder"><img className="company-img" src={this.props.companyLogo} alt={this.props.companyLogo} /></div> : ''}
			</div>
		);
	}
}

export default CompanyLogo;

interface Props {
	companyLogo: string;
}
