import * as React from 'react';
import { connect } from 'react-redux';
import { InjectedIntlProps, FormattedMessage, injectIntl } from 'react-intl';
import { NavLink, withRouter, RouteComponentProps } from 'react-router-dom';
import routes from '../../../routes';
import { logout, changeLanguage } from '../../../config/actions';
import { getLanguages } from '../../../config/localization';
import { GetAdministrationUserInfoPayload } from '../../../scenes/login/types';
import './style.scss';

class Header extends React.Component<Props & InjectedIntlProps & RouteComponentProps<PathParamsType>> {
	render() {
		let className = 'main-header';
		let devLabelClassName = 'no-devlabel';
		if (location.hostname.includes('mypalletcustomerdev')) {
			className = 'main-header-debug';
			devLabelClassName = 'show-devlabel';
		}
		if (location.hostname.includes('localhost')) {
			className = 'main-header-debug';
			devLabelClassName = 'show-devlabel';
		}
		return (
			<header className={className}>
				<div className="header-tools clearfix">
					<div className="pull-left">
						<div className="logo">
							<NavLink to={routes.DASHBOARD.url}><img src={`${process.env.PUBLIC_URL}/assets/img/logo.svg`} className="App-logo" alt="logo" /></NavLink>
						</div>
					</div>
					<div className={devLabelClassName}>
					<label>DEVELOPMENT</label>
					</div>
					<div className="pull-right">
						{
							(this.props.userInfo && this.props.userInfo.shouldChangePassword === false) ?
								<a onClick={() => { this.props.logout(); }} className="link">
									<span className="ico-holder"><i className="glyphicon glyphicon-user" /></span> Log {this.props.userInfo.name} ud
								</a> : ''
						}
						{
							(this.props.userInfo && this.props.userInfo.shouldChangePassword === false) ? <span className="seprator">-</span> : ''
						}
						{getLanguages().map((lang: string) => (
							<a key={lang} className={`lang-link ${(this.props.currentLanguage === lang) ? 'active' : ''}`} onClick={() => { this.props.changeLanguage(lang); }}>
								<img src={`${process.env.PUBLIC_URL}/assets/img/my-pallet/flags/${lang}.png`} alt={lang} />
							</a>
						))}
					</div>
				</div>
				{
					(this.props.userInfo && this.props.userInfo.shouldChangePassword === false) ?
						<nav className="main-nav">
							<ul className="main-nav-list">
								{
									Object.keys(routes).map((k) => {
										let item = routes[k];
										return (item.needAuth && item.accessible && item.showInNav) ? <li key={k}><NavLink activeClassName="active" className="link" to={item.url}><FormattedMessage id={`navigation.${item.name}`} /></NavLink></li> : '';
									})
								}
							</ul>
						</nav> : ''
				}
			</header>
		);
	}
}

const mapDispatchToProps = (d): Actions => {
	return {
		logout: () => d(logout()),
		changeLanguage: (lang: string) => d(changeLanguage(lang)),
	};
};

const mapStateToProps = (state) => {
	return {
		userInfo: state.Login.administrationUserInfo,
		currentLanguage: state.Language.currentLanguage
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(injectIntl(Header)));

interface Actions {
	logout();
	changeLanguage(lang: string);
}
interface Props extends Actions {
	userInfo: GetAdministrationUserInfoPayload;
	currentLanguage: string;
}
interface PathParamsType { }
