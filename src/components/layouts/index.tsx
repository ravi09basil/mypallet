import * as React from 'react';
import Header from './header';

class Layout extends React.Component {
	render() {
		return (
			<div className="main-layout">
				<Header />
				<main className="main-content">
					{this.props.children}
				</main>
			</div>
		);
	}
}

export default Layout;
