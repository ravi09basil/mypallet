import * as React from 'react';
import Loader from '../loader';

class ConfirmationModal extends React.Component<Props> {
	constructor(props: Props) {
		super(props);
		this.confirm = this.confirm.bind(this);
	}

	confirm(e: React.MouseEvent<HTMLElement>) {
		this.props.confirmEvent(e);
	}

	render() {
		return (
			<div className="modal fade" id={this.props.modalID} data-backdrop="static">
				<div className="modal-dialog">
					<div className="modal-content">
						<div className="modal-header">
							<button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 className="modal-title">{this.props.title}</h4>
						</div>
						<div className="modal-body">
							{this.props.children}
						</div>
						<div className="modal-footer">
							<button type="button" className="btn btn-default" data-dismiss="modal">{this.props.cancelTxt}</button>
							<button type="button" className="btn btn-success" onClick={this.confirm}>{this.props.confirmTxt}</button>
						</div>
					</div>
					<Loader status={this.props.loaderStatus} />
				</div>
			</div >
		);
	}
}

export default ConfirmationModal;

interface Props {
	modalID: string;
	title: string;
	confirmEvent: Function;
	cancelTxt: string;
	confirmTxt: string;
	loaderStatus: boolean;
}
