import * as React from 'react';
// https://react-form.js.org/#/
import { Form, FormApi, Text } from 'react-form';
import { InjectedIntlProps, FormattedMessage, injectIntl } from 'react-intl';
import InputMessage from '../inputMsg';

class EnterEmailModal extends React.Component<Props & InjectedIntlProps, State> {
    formApi: FormApi;

    componentDidMount() {
        // WHEN THE MODAL IS CLOSED
        $(`#${this.props.modalID}`).on('hidden.bs.modal', () => {
            // RESET ALL THE FIELDS AFTER CLOSE
            this.formApi.resetAll();
        });
    }

    validate = values => {
        return {
            email: values.email ? null : this.props.intl.formatMessage({ id: 'general.validation.email' })
        };
    }

    submit(e: React.FormEvent<HTMLFormElement>, formApi: FormApi) {
        formApi.submitForm(e).then((res) => {
            let formState = formApi.getFormState();
            let values = formState.values;
            let errorsCount = formState.errors ? Object.keys(formState.errors).length : 0;
            let validationFailures = formState.validationFailures;
            if (errorsCount === 0 && validationFailures === 0) {
                // SEND THE VALUES
                values.transactionID = this.props.transactionID;
                this.props.handleSubmit(values);
                $(`#${this.props.modalID}`).modal('hide');
            }
        });
    }

    formRender(modaID: string) {
        return (
            <div id={this.props.modalID} className="modal fade" data-backdrop="static">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <Form validate={this.validate} validateOnSubmit={true}>
                            {
                                (formApi: FormApi) => {
                                    this.formApi = formApi;
                                    return (
                                        <form onSubmit={(e) => { this.submit(e, formApi); }}>
                                            <div className="modal-header">
                                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 className="modal-title"><FormattedMessage id="comps.enterEmail.title" /></h4>
                                            </div>
                                            <div className="modal-body">
                                                <div className="form-st1">
                                                    <div className="row">
                                                        <div className="col-sm-12">
                                                            <InputMessage name="email" formApi={formApi}>
                                                                <label><FormattedMessage id="general.form.email" /></label>
                                                                <Text className="form-control" field="email" placeholder={this.props.intl.formatMessage({ id: 'general.form.email' })} />
                                                            </InputMessage>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="modal-footer">
                                                <button type="button" className="btn btn-default" data-dismiss="modal"><FormattedMessage id="general.close" /></button>
                                                <button type="submit" className="btn btn-primary"><FormattedMessage id={`comps.enterEmail.submitButton`} /></button>
                                            </div>
                                        </form>
                                    );
                                }
                            }
                        </Form>
                    </div>
                </div>
            </div >
        );
    }

    render() {
        return this.formRender(this.props.modalID);
    }
}

const EnterEmail = injectIntl(EnterEmailModal);

export {
    EnterEmail,
};

interface State {
    error: null | {
        successfull: boolean;
        message: string;
    };
}
interface Props {
    modalID: string;
    handleSubmit: Function;
    transactionID: string | undefined;
}
