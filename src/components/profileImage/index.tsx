import * as React from 'react';
import './style.scss';

class ProfileImage extends React.Component<Props> {
	render() {
		return (
			<div className="profile-image">
				<span className="username">{this.props.userName}</span>
				{this.props.userImage ? <div className="img-holder"><img className="img" src={this.props.userImage} alt={this.props.userName} /></div> : ''}
			</div>
		);
	}
}

export default ProfileImage;

interface Props {
	userName: string;
	userImage: string;
}
