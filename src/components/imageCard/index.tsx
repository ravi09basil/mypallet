import * as React from 'react';
import './style.scss';
import { Lightbox } from 'react-modal-image';

class ImageCard extends React.Component<Props, State> {
	state: State = {
		isOpen: false
	};

	render() {
		return (
			<div className="imageCard-holder">
				<div className="imageCard-title">{this.props.title}</div>
				<div className="media imageCard-info">
					<div className="media-left imageCard-img-holder">
						<i className={this.props.icon} />
					</div>
					<div className="media-body imageCard-name">
						{
							this.props.images.length ?
								this.props.images.map((name, index) => (
									<div key={index}>
										<a onClick={() => this.setState({ isOpen: true })}>
											{this.props.linkText}
										</a>
										{this.state.isOpen && (<Lightbox medium={name} large={name} onClose={() => this.setState({ isOpen: false })} />)}
									</div>
								)) : this.props.defaultText}
					</div>
				</div>
			</div>
		);
	}
}

export default ImageCard;

interface State {
	isOpen: boolean;
}

interface Props {
	title: string;
	images: string[];
	icon: string;
	linkText: string;
	defaultText: string;
}
