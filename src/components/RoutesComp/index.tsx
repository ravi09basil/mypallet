import * as React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import Layout from '../../components/layouts';
import routes from '../../routes';
import Auth from '../../components/auth';

const RoutesComp = (p) => {
	return (
		<BrowserRouter>
			<Layout>
				<Switch>
					<Route path={routes.LOGIN.url} component={routes.LOGIN.comp} />
					<Route path={routes.CHANGE_PASSWORD.url} component={routes.CHANGE_PASSWORD.comp} />
					<Route path={routes.RECEIPT.url} component={routes.RECEIPT.comp} />
					<Auth>
						<Switch>
							{
								Object.keys(routes).map((k) => {
									let item = routes[k];
									return (item.needAuth && item.accessible) ? <Route key={k} path={item.url} component={item.comp} /> : '';
								})
							}
							<Redirect exact={true} from="/" to={routes.DASHBOARD.url} />
							<Route component={routes.Page404.comp} />
						</Switch>
					</Auth>
				</Switch>
			</Layout>
		</BrowserRouter>
	);
};

export default RoutesComp;
