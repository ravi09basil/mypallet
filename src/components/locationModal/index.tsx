import * as React from 'react';
// https://react-form.js.org/#/
import { Form, FormApi, Text } from 'react-form';
import { InjectedIntlProps, FormattedMessage, injectIntl } from 'react-intl';
import Loader from '../loader';
import InputMessage from '../inputMsg';
import InputAddress from '../inputAddress';
import PhoneInput from '../inputPhone';
import ErrorHolder from '../errorHolder';

class NewLocation extends React.Component<Props & InjectedIntlProps, State> {
	formApi: FormApi;

	componentDidMount() {
		// WHEN THE MODAL IS CLOSED
		$(`#${this.props.modalID}`).on('hidden.bs.modal', () => {
			// RESET ALL THE FIELDS AFTER CLOSE
			this.formApi.resetAll();
		});
	}

	validate = values => {
		return {
			name: values.name ? null : this.props.intl.formatMessage({ id: 'general.validation.name' }),
			phoneNumber: values.phoneNumber ? null : this.props.intl.formatMessage({ id: 'general.validation.phoneNumber' }),
			address: values.address ? null : this.props.intl.formatMessage({ id: 'general.validation.address' }),
			email: values.email ? null : this.props.intl.formatMessage({ id: 'general.validation.email' })
		};
	}

	submit(e: React.FormEvent<HTMLFormElement>, formApi: FormApi) {
		formApi.submitForm(e).then((res) => {
			let locationData = (this.props.locationData) ? this.props.locationData : false;
			let formState = formApi.getFormState();
			let values = formState.values;
			if (locationData) {
				values.id = locationData.id;
			}
			let errorsCount = formState.errors ? Object.keys(formState.errors).length : 0;
			if (errorsCount === 0) {
				// SEND THE VALUES
				this.props.handleSubmit(values).then(() => {
					// CLOSE THE MODAL
					$(`#${this.props.modalID}`).modal('hide');
					// RESET
					this.setState({
						error: null
					});
				}).catch((error) => {
					// ERROR
					this.setState({
						error: error
					});
				});
			}
		});
	}

	formRender(modaID: string) {
		// VARS
		let state = this.state;
		let message: string = '';
		let successfull = true;
		if (state && state.error != null && state.error.message) {
			message = state.error.message;
			successfull = state.error.successfull;
		}
		let locationData = (this.props.locationData) ? this.props.locationData : {
			name: ''
		};
		return (
			<div id={this.props.modalID} className="modal fade" data-backdrop="static">
				<div className="modal-dialog">
					<div className="modal-content">
						<Form validate={this.validate} validateOnSubmit={true}>
							{
								(formApi: FormApi) => {
									this.formApi = formApi;
									return (
										<form onSubmit={(e) => { this.submit(e, formApi); }}>
											<div className="modal-header">
												<button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 className="modal-title"><FormattedMessage id={`comps.${modaID}.title`} values={locationData} /></h4>
											</div>
											<div className="modal-body">
												<ErrorHolder message={message} hide={successfull} />
												<div className="form-st1">
													<div className="row">
														<div className="col-sm-6">
															<InputMessage name="name" formApi={formApi}>
																<label><FormattedMessage id="general.form.name" /></label>
																<Text className="form-control" field="name" placeholder={this.props.intl.formatMessage({ id: 'general.form.name' })} />
															</InputMessage>
														</div>
														<div className="col-sm-6">
															<InputMessage name="phoneNumber" formApi={formApi}>
																<label><FormattedMessage id="general.form.phoneNumber.label" /></label>
																<PhoneInput className="form-control" field="phoneNumber" defaultCountry="dk" placeholder={this.props.intl.formatMessage({ id: 'general.form.phoneNumber.placeholder' })} />
															</InputMessage>
														</div>
													</div>
													<div className="row">
														<div className="col-sm-6">
															<InputMessage name="address" formApi={formApi}>
																<label><FormattedMessage id="general.form.address" /></label>
																<InputAddress className="form-control" field="address" placeholder={this.props.intl.formatMessage({ id: 'general.form.address' })} />
															</InputMessage>
														</div>
														<div className="col-sm-6">
															<InputMessage name="email" formApi={formApi}>
																<label><FormattedMessage id="general.form.email" /></label>
																<Text className="form-control" field="email" type="email" placeholder={this.props.intl.formatMessage({ id: 'general.form.email' })} />
															</InputMessage>
														</div>
													</div>
												</div>
											</div>
											<div className="modal-footer">
												<button type="button" className="btn btn-default" data-dismiss="modal"><FormattedMessage id="general.close" /></button>
												<button type="submit" className="btn btn-primary"><FormattedMessage id={`comps.${modaID}.submitButton`} /></button>
											</div>
										</form>
									);
								}
							}
						</Form>
						<Loader status={this.props.loaderStatus} />
					</div>
				</div>
			</div >
		);
	}

	render() {
		return this.formRender(this.props.modalID);
	}
}

class EditLocation extends NewLocation {
	componentDidMount() {
		// CALL THE PARENT
		super.componentDidMount();
		// WHEN THE MODAL IS SHOWN
		$(`#${this.props.modalID}`).on('show.bs.modal', () => {
			if (this.formApi && this.props.locationData) {
				this.formApi.setAllValues(this.props.locationData);
			}
		});
	}
}

const NewLocationModal = injectIntl(NewLocation);
const EditLocationModal = injectIntl(EditLocation);

export {
	NewLocationModal,
	EditLocationModal
};

interface State {
	error: null | {
		successfull: boolean;
		message: string;
	};
}
interface Props {
	modalID: string;
	handleSubmit: Function;
	loaderStatus: boolean;
	locationData?: any;
}
