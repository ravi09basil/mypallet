import * as React from 'react';
import './style.scss';

class CompanyInfoCard extends React.Component<Props> {
	render() {
		return (
			<div className="company-holder">
				<div className="media company-info">
					<div className="media-left company-img-holder">
						{this.props.logo ? <img className="company-img" src={this.props.logo} alt={this.props.name} /> : <i className="fa fa-file-image-o fa-5x company-ico" />}
					</div>
					<div className="media-body company-details">
						<p><strong>{this.props.name}</strong></p>
						<p>{this.props.vatNumber}</p>
						<p>{this.props.address}</p>
						<p>{this.props.postCode} {this.props.city}</p>
						<p>{this.props.phoneNumber}</p>
					</div>
				</div>
			</div>
		);
	}
}

export default CompanyInfoCard;

interface Props {
	address: string;
	logo: string;
	name: string;
	city: string;
	phoneNumber: string;
	vatNumber: string;
	postCode: string;
}
