import * as React from 'react';
import { Field, FieldApi } from 'react-form';
import ReactPhoneInput from 'react-phone-input-2';

class PhoneInput extends React.Component<Props> {
	onChange(value: any, fieldApi: FieldApi) {
		// SET THE VALUE
		fieldApi.setValue(value);
		// CHECK IF THE USER HAVE ON CHANGE EVENT
		if (this.props.onChange) {
			this.props.onChange(value);
		}
	}

	render() {
		const { onChange, field, ...rest } = this.props;
		return (
			<Field field={field}>
				{fieldApi => {
					return (
						<ReactPhoneInput {...rest} countryCodeEditable={false} enableLongNumbers={true} onChange={value => { this.onChange(value, fieldApi); }} value={fieldApi.value || ''} />
					);
				}}
			</Field>
		);
	}
}

export default PhoneInput;

interface Props extends React.DetailedHTMLProps<React.SelectHTMLAttributes<HTMLTextAreaElement>, HTMLTextAreaElement> {
	field: string;
	defaultCountry?: string;
}
