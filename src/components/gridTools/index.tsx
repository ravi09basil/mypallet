import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { GridOptions, GridApi, GridReadyEvent } from 'ag-grid';
// https://www.npmjs.com/package/print-html-element
import * as PHE from 'print-html-element';

class GridTools extends React.Component<Props> {
	gridApi: GridApi | undefined;

	constructor(props: Props) {
		super(props);
		this.printGrid = this.printGrid.bind(this);
		this.exportGrid = this.exportGrid.bind(this);
		// WHEN GRID IS READY
		let onGridReady = this.props.gridOptions.onGridReady;
		this.props.gridOptions.onGridReady = (params: GridReadyEvent) => {
			if (onGridReady) {
				onGridReady();
			}
			// SAVE THE API
			this.gridApi = params.api;

			this.gridApi.sizeColumnsToFit();

			window.addEventListener('resize', function () {
				setTimeout(function () {
					params.api.sizeColumnsToFit();
				});
			});
		};
	}

	printGrid() {
		let gridApi = this.gridApi;
		if (gridApi) {
			let eRoot = (gridApi as any).gridPanel.eRoot;
			PHE.printElement(eRoot);
		}
	}

	exportGrid() {
		let gridApi = this.gridApi;
		if (gridApi) {
			gridApi.exportDataAsCsv();
		}
	}

	render() {
		return (
			<span className="grid-tools">
				<a className="ico-btn color-green" data-toggle="modal" data-target={`#${this.props.modalID}`}><i className="fa fa-plus-circle" /> <span className="txt"> <FormattedMessage id="general.createNew" /></span></a>
				<a onClick={this.printGrid} className="ico-btn color-black"><i className="fa fa-print" /> <span className="txt"><FormattedMessage id="general.print" /></span></a>
				<a onClick={this.exportGrid} className="ico-btn color-black"><i className="fa fa-download" /> <span className="txt"><FormattedMessage id="general.extract" /></span></a>
			</span>
		);
	}
}

export default GridTools;

interface Props {
	gridOptions: GridOptions;
	modalID: string;
}
