import * as React from 'react';
// https://react-form.js.org/#/
import { Form, FormApi, Select, Text } from 'react-form';
import Loader from '../loader';
import InputMessage from '../inputMsg';
import PhoneInput from '../../components/inputPhone';
import ErrorHolder from '../errorHolder';
import { InjectedIntlProps, FormattedMessage, injectIntl } from 'react-intl';

class NewUser extends React.Component<Props & InjectedIntlProps, State> {
	formApi: FormApi;

	userTypeOptions = [
		{
			label: this.props.intl.formatMessage({ id: 'general.form.userTypeOptions.driver' }),
			value: '0',
		},
		{
			label: this.props.intl.formatMessage({ id: 'general.form.userTypeOptions.warehouse' }),
			value: '1',
		},
		// {
		// 	label: this.props.intl.formatMessage({ id: 'general.form.userTypeOptions.shippingAgent' }),
		// 	value: '2',
		// },
	];

	componentDidMount() {
		// WHEN THE MODAL IS CLOSED
		$(`#${this.props.modeID}`).on('hidden.bs.modal', () => {
			// RESET ALL THE FIELDS AFTER CLOSE
			this.formApi.resetAll();
		});
	}

	validate = values => {
		return {
			name: values.name ? null : this.props.intl.formatMessage({ id: 'general.validation.name' }),
			phoneNumber: (values.phoneNumber && values.phoneNumber.toString().length > 5) ? null : this.props.intl.formatMessage({ id: 'general.validation.phoneNumber' }),
			userType: values.userType ? null : this.props.intl.formatMessage({ id: 'general.validation.userType' })
		};
	}

	submit(e: React.FormEvent<HTMLFormElement>, formApi: FormApi) {
		formApi.submitForm(e).then((res) => {
			let formState = formApi.getFormState();
			let values = formState.values;
			let errorsCount = formState.errors ? Object.keys(formState.errors).length : 0;
			if (errorsCount === 0) {
				// SEND THE VALUES
				this.props.handleSubmit(values).then(() => {
					// CLOSE THE MODAL
					$(`#${this.props.modeID}`).modal('hide');
					// RESET
					this.setState({
						error: null
					});
				}).catch((error) => {
					// ERROR
					this.setState({
						error: error
					});
				});
			}
		});
	}

	render() {
		// VARS
		let state = this.state;
		let message: string = '';
		let successfull = true;
		if (state && state.error != null && state.error.message) {
			message = state.error.message;
			successfull = state.error.successfull;
		}
		return (
			<div id={this.props.modeID} className="modal fade" data-backdrop="static">
				<div className="modal-dialog">
					<div className="modal-content">
						<Form validate={this.validate} validateOnSubmit={true}>
							{
								(formApi: FormApi) => {
									this.formApi = formApi;
									return (
										<form onSubmit={(e) => { this.submit(e, formApi); }}>
											<div className="modal-header">
												<button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 className="modal-title"><FormattedMessage id="comps.newUserModal.title" /></h4>
											</div>
											<div className="modal-body">
												<ErrorHolder message={message} hide={successfull} />
												<div className="form-st1">
													<InputMessage name="name" formApi={formApi}>
														<label><FormattedMessage id="general.form.name" /></label>
														<Text className="form-control" field="name" placeholder={this.props.intl.formatMessage({ id: 'general.form.name' })} />
													</InputMessage>
													<InputMessage name="phoneNumber" formApi={formApi}>
														<label><FormattedMessage id="general.form.phoneNumber.label" /></label>
														<PhoneInput className="form-control" field="phoneNumber" defaultCountry="dk" placeholder={this.props.intl.formatMessage({ id: 'general.form.phoneNumber.placeholder' })} />
													</InputMessage>
													<InputMessage name="userType" formApi={formApi}>
														<label><FormattedMessage id="general.form.userType" /></label>
														<Select className="form-control" field="userType" options={this.userTypeOptions} />
													</InputMessage>
												</div>
											</div>
											<div className="modal-footer">
												<button type="button" className="btn btn-default" data-dismiss="modal"><FormattedMessage id="general.close" /></button>
												<button type="submit" className="btn btn-primary"><FormattedMessage id="general.create" /></button>
											</div>
										</form>
									);
								}
							}
						</Form>
						<Loader status={this.props.loaderStatus} />
					</div>
				</div>
			</div >
		);
	}
}

export default injectIntl(NewUser);

interface State {
	error: null | {
		successfull: boolean;
		message: string;
	};
}
interface Props {
	modeID: string;
	handleSubmit: Function;
	loaderStatus: boolean;
}
