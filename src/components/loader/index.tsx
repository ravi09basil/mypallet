import * as React from 'react';
import './style.scss';

class Loader extends React.Component<Props> {
	render() {
		let flag = this.props.status;
		let loaderClass = 'loader-holder';
		let loadingClass = flag ? `${loaderClass} loading` : `${loaderClass}`;
		return (
			<div className={loadingClass} />
		);
	}
}

export default Loader;

interface Props {
	status: boolean;
}
