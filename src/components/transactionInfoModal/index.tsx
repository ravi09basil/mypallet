import * as React from 'react';
import { GridOptions } from 'ag-grid';
import { InjectedIntlProps, FormattedMessage, injectIntl } from 'react-intl';
import Loader from '../loader';
import UserCard from '../userCard';
import InfoCard from '../infoCard';
import ImageCard from '../imageCard';
import GoogleMaps from '../googleMaps';
import AgGridTheme from '../agGridTheme';
import { GetTransactionInfoPayload, Registrations } from '../../scenes/customer/types';

class TransactionInfoModal extends React.Component<Props & InjectedIntlProps, State> {
	state: State = {
		gridOptions: {
			enableSorting: true,
			domLayout: 'autoHeight',
			onGridReady: () => {
				if (this.state.gridOptions.api) {
					this.state.gridOptions.api.sizeColumnsToFit();
				}
			},
			columnDefs: [
				{
					headerName: this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.columns.product' }),
					field: 'name'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.columns.received' }),
					field: 'received'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.columns.delivered' }),
					field: 'delivered'
				},
			]
		}
	};

	render() {
		// VARS
		let transaction = (this.props.data && this.props.data.transaction) ? this.props.data.transaction : false;
		let rowData = transaction ? transaction.groupedRegistrationLines : [];
		let collectImages = (accumulator: any[], currentValue: Registrations) => {
			return accumulator.concat(currentValue.imagePaths);
		};
		let registrationsImagePaths = (transaction && transaction.registrations.length) ? transaction.registrations.reduce(collectImages, []) : [];
		return (
			<div className="modal fade" id={this.props.modalID} data-backdrop="static">
				<div className="modal-dialog modal-lg">
					<div className="modal-content">
						<header className="modal-header">
							<button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 className="modal-title" id="myModalLabel"><FormattedMessage id="comps.transactionInfoModal.title" /></h4>
						</header>
						<div className="modal-body">
							<div className="row mb-10">
								<div className="col-sm-12">
									{transaction && transaction.latitude && transaction.longitude ?
										<GoogleMaps
											defaultZoom={15}
											position={{ lat: transaction.latitude, lng: transaction.longitude }}
											defaultCenter={{ lat: transaction.latitude, lng: transaction.longitude }}
											containerElement={<div style={{ height: `400px` }} />}
											mapElement={<div style={{ height: `100%` }} />}
										/> : ''}
								</div></div>
							{
								(rowData && rowData.length) ?
									<div className="mb-30 ag-theme-balham">
										<AgGridTheme rowData={rowData} gridOptions={this.state.gridOptions} />
									</div> : ''
							}
							<div className="row mb-10">
								<div className="col-md-6">
									{transaction ?
										<UserCard title={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.receiverTitle' })} username={transaction.receivingUserName} companyName={transaction.receivingUserCompanyName} userImagePath={transaction.receivingUserImagePath} /> : ''}
								</div>
								<div className="col-md-6">
									{transaction ?
										<UserCard title={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.senderTitle' })} username={transaction.sendingUserName} companyName={transaction.sendingUserCompanyName} userImagePath={transaction.sendingUserImagePath} /> : ''}
								</div>
							</div>
							<div className="row mb-10">
								<div className="col-sm-6">
									{transaction && transaction.note ?
										<InfoCard
											title={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.note.title' })}
											valueIsImage={false}
											defaultText={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.note.defaultText' })}
											icon="fa fa-edit fa-3x"
											value={transaction.note}
										/> : ''}
								</div>
								<div className="col-sm-6">
									{transaction && transaction.consignmentNotePath ?
										<InfoCard
											title={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.consignmentNote.title' })}
											valueIsImage={true}
											defaultText={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.consignmentNote.defaultText' })}
											icon="fa fa-file-image-o fa-3x"
											value={transaction.consignmentNotePath}
										/> : ''}
								</div>
							</div>
							<div className="row mb-10">
								<div className="col-sm-6">
									{transaction && transaction.trailerNumber ?
										<InfoCard
											title={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.trailerNumber.title' })}
											valueIsImage={false}
											defaultText={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.trailerNumber.defaultText' })}
											icon="fa fa-truck fa-3x"
											value={transaction.trailerNumber}
										/> : ''}
								</div>
								<div className="col-sm-6">
									{transaction && transaction.registrationNumber ?
										<InfoCard
											title={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.registrationNumber.title' })}
											valueIsImage={false}
											defaultText={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.registrationNumber.defaultText' })}
											icon="fa fa-truck fa-3x"
											value={transaction.registrationNumber}
										/> : ''}

								</div>
							</div>
							<div className="row mb-10">
								{transaction && transaction.signaturePath ?
									<div className="col-sm-6">
										<ImageCard
											title={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.signaturePath.title' })}
											linkText={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.signaturePath.openSignature' })}
											defaultText=""
											icon="fa fa-file-image-o fa-3x"
											images={[transaction.signaturePath]}
										/>
									</div> : ''}
								{registrationsImagePaths.length ?
									<div className="col-sm-6">
										<ImageCard
											title={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.imageCard.title' })}
											linkText={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.imageCard.seeProductImage' })}
											defaultText={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.imageCard.defaultText' })}
											icon="fa fa-file-image-o fa-3x"
											images={registrationsImagePaths}
										/>
									</div>
									: ''}
							</div>
						</div>
						<footer className="modal-footer">
							<button type="button" className="btn btn-default" data-dismiss="modal"><FormattedMessage id="general.close" /></button>
						</footer>
						<Loader status={this.props.loaderStatus} />
					</div>
				</div>
			</div >
		);
	}
}

export default injectIntl(TransactionInfoModal);

interface State {
	gridOptions: GridOptions;
}
interface Props {
	modalID: string;
	data: GetTransactionInfoPayload | undefined;
	loaderStatus: boolean;
}
