import * as React from 'react';
// https://react-form.js.org/#/
import { Form, Text, FormApi } from 'react-form';
import { InjectedIntlProps, FormattedMessage, injectIntl } from 'react-intl';
import InputMessage from '../inputMsg';
import Loader from '../loader';
import PhoneInput from '../inputPhone';
import InputAddress from '../inputAddress';
import ErrorHolder from '../errorHolder';
import { GetCheckVATNumberPayload } from '../../../src/scenes/customers/types';

class ChangeCustomerModal extends React.Component<Props & InjectedIntlProps, State> {
	place: google.maps.places.PlaceResult;
	formApi: FormApi;
	defaultCountry = 'dk';
	vatData = null;
	vatCheckTimer;

	constructor(props: Props & InjectedIntlProps) {
		super(props);
		this.vatNumberOnChange = this.vatNumberOnChange.bind(this);
		this.onSelectAddress = this.onSelectAddress.bind(this);
	}

	componentDidMount() {
		// WHEN THE MODAL IS CLOSED
		$(`#${this.props.modalID}`).on('hidden.bs.modal', () => {
			// RESET ALL THE FIELDS
			this.formApi.resetAll();
		});
	}

	validate = values => {
		let valid = {
			vatNumber: values.vatNumber ? null : this.props.intl.formatMessage({ id: 'general.validation.vatNumber' }),
		};
		if (this.vatNumberIsOk()) {
			let clone = {
				name: values.name ? null : this.props.intl.formatMessage({ id: 'general.validation.name' }),
				address: values.address ? null : this.props.intl.formatMessage({ id: 'general.validation.adresse.required' }),
				phoneNumber: values.phoneNumber ? null : this.props.intl.formatMessage({ id: 'general.validation.phoneNumber' }),
			};
			valid = Object.assign(valid, clone);
		}
		return valid;
	}

	vatNumberIsOk() {
		let values = this.formApi.values;
		if (values && values.vatNumber) {
			return true;
		}
		return false;
	}

	submit(e: React.FormEvent<HTMLFormElement>, formApi: FormApi) {
		formApi.submitForm(e).then((res) => {
			let formState = formApi.getFormState();
			let values = formState.values;
			let errorsCount = formState.errors ? Object.keys(formState.errors).length : 0;
			let validationFailures = formState.validationFailures;
			if (errorsCount === 0 && validationFailures === 0) {
				// SEND THE VALUES
				this.props.handleSubmit(values).then(() => {
					// CLOSE THE MODAL
					$(`#${this.props.modalID}`).modal('hide');
					// RESET
					this.setState({
						error: null
					});
				}).catch((error) => {
					// ERROR
					this.setState({
						error: error
					});
				});
			}
		});
	}

	vatNumberOnChange(vatNumber: string) {
		// CLEAR THE TIMER
		if (this.vatCheckTimer) { clearTimeout(this.vatCheckTimer); }
		// CHECK THE NUMBER
		if (vatNumber && vatNumber.length === 8) {
			// VARS
			let formFields = {
				vatNumber: vatNumber,
				name: '',
				address: '',
				phoneNumber: ''
			};
			var callAPI = () => {
				// CHECK VAT NUMBER
				this.props.vatNumberCheck(vatNumber).then(res => {
					this.setState({
						vatData: res
					});
					if (res.isExsisting) {
						this.formApi.setAllValues(res.customerInfo);
					}
				});
				// SET THE VALUES
				this.formApi.setAllValues(formFields);
			};
			// CALL THE NEW TMER
			this.vatCheckTimer = setTimeout(callAPI, 1000);
		}
		// RESET
		this.setState({
			vatData: null
		});
	}

	onSelectAddress(place: google.maps.places.PlaceResult) {
		// VARS
		let zipCode = '';
		let country = '';
		// LOOP
		place.address_components.forEach((i) => {
			if (i.types.indexOf('postal_code') !== -1) {
				zipCode = i.long_name;
			} else if (i.types.indexOf('country') !== -1) {
				country = i.long_name;
			}
		});
		this.setState({
			place: {
				zipCode,
				country
			}
		});
	}

	render() {
		// VARS
		let state = this.state;
		let message: string = '';
		let successfull: boolean = true;
		let vatData = (this.state) ? this.state.vatData : null;
		if (state) {
			if (state.error != null && state.error.message) {
				message = state.error.message;
				successfull = state.error.successfull;
			}
		}

		return (
			<div id={this.props.modalID} className="modal fade" data-backdrop="static">
				<div className="modal-dialog">
					<div className="modal-content">
						<Form validate={this.validate} validateOnSubmit={true}>
							{
								(formApi: FormApi) => {
									this.formApi = formApi;
									return (
										<form onSubmit={(e) => { this.submit(e, formApi); }}>
											<div className="modal-header">
												<button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 className="modal-title"><FormattedMessage id="comps.changeCustomerModal.title" /></h4>
											</div>
											<div className="modal-body">
												<ErrorHolder message={message} hide={successfull} />
												<div className="form-st1">
													<InputMessage name="vatNumber" formApi={formApi}>
														<label><FormattedMessage id="general.form.vatNumber" /></label>
														<div className="input-group">
															<span className="input-group-addon">{(formApi.values && formApi.values.vatNumber) ? formApi.values.vatNumber.length : 0} / 8</span>
															<Text className="form-control" field="vatNumber" placeholder={this.props.intl.formatMessage({ id: 'general.form.vatNumber' })} onChange={this.vatNumberOnChange} maxLength="8" />
														</div>
													</InputMessage>
													{
														(this.vatNumberIsOk() && vatData) ?
															<div>
																<InputMessage name="name" formApi={formApi}>
																	<label><FormattedMessage id="general.form.name" /></label>
																	<Text className="form-control" field="name" placeholder={this.props.intl.formatMessage({ id: 'general.form.name' })} />
																</InputMessage>
																<InputMessage name="address" formApi={formApi}>
																	<label><FormattedMessage id="general.form.adresse" /></label>
																	<InputAddress className="form-control" field="address" placeholder={this.props.intl.formatMessage({ id: 'general.form.adresse' })} onSelectAddress={this.onSelectAddress} />
																	{(state && state.place) ? <small className="help-block">{`${state.place.zipCode}, ${state.place.country}`}</small> : ''}
																</InputMessage>
																<div className="row">
																	<div className="col col-sm-6">
																		<InputMessage name="phoneNumber" formApi={formApi}>
																			<label><FormattedMessage id="general.form.phoneNumber.label" /></label>
																			<PhoneInput defaultCountry={this.defaultCountry} className="form-control" field="phoneNumber" placeholder={this.props.intl.formatMessage({ id: 'general.form.phoneNumber.placeholder' })} />
																		</InputMessage>
																	</div>
																</div>
															</div> : ''
													}
												</div>
											</div>
											<div className="modal-footer">
												<button type="button" className="btn btn-default" data-dismiss="modal"><FormattedMessage id="general.close" /></button>
												<button type="submit" className="btn btn-primary"><FormattedMessage id="general.create" /></button>
											</div>
										</form>
									);
								}
							}
						</Form>
						<Loader status={this.props.loaderStatus} />
					</div>
				</div>
			</div >
		);
	}
}

export default injectIntl(ChangeCustomerModal);

interface State {
	place: {
		zipCode: string;
		country: string;
	};
	vatData: GetCheckVATNumberPayload | null;
	error: null | {
		successfull: boolean;
		message: string;
	};
}
interface Props {
	modalID: string;
	handleSubmit: Function;
	loaderStatus: boolean;
	vatNumberCheck(vatNumber: string): Promise<GetCheckVATNumberPayload>;
}
