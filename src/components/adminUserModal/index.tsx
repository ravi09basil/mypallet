import * as React from 'react';
// https://react-form.js.org/#/
import { Form, Text, FormApi } from 'react-form';
import Loader from '../loader';
import InputFile from '../inputFile';
import InputMessage from '../inputMsg';
import PhoneInput from '../inputPhone';
import ErrorHolder from '../errorHolder';
import { InjectedIntlProps, FormattedMessage, injectIntl } from 'react-intl';

class NewAdminUser extends React.Component<Props & InjectedIntlProps, State> {
	formApi: FormApi;

	constructor(props: Props & InjectedIntlProps) {
		super(props);
		this.resetPassword = this.resetPassword.bind(this);
	}

	componentDidMount() {
		// WHEN THE MODAL IS CLOSED
		$(`#${this.props.modalID}`).on('hidden.bs.modal', () => {
			// RESET ALL THE FIELDS AFTER CLOSE
			this.formApi.resetAll();
		});
	}

	validate = values => {
		return {
			name: values.name ? null : this.props.intl.formatMessage({ id: 'general.validation.name' }),
			username: values.username ? null : this.props.intl.formatMessage({ id: 'general.validation.username' }),
			email: values.email ? null : this.props.intl.formatMessage({ id: 'general.validation.email' }),
			phoneNumber: values.phoneNumber ? null : this.props.intl.formatMessage({ id: 'general.validation.phoneNumber' }),
			profileImagePath: values.profileImagePath ? null : this.props.intl.formatMessage({ id: 'general.validation.profileImagePath' })
		};
	}

	submit(e: React.FormEvent<HTMLFormElement>, formApi: FormApi) {
		formApi.submitForm(e).then((res) => {
			let formState = formApi.getFormState();
			let values = formState.values;
			let errorsCount = formState.errors ? Object.keys(formState.errors).length : 0;
			if (errorsCount === 0) {
				// SEND THE VALUES
				this.props.handleSubmit(values).then(() => {
					// CLOSE THE MODAL
					$(`#${this.props.modalID}`).modal('hide');
					// RESET
					this.setState({
						error: null
					});
				}).catch((error) => {
					// ERROR
					this.setState({
						error: error
					});
				});
			}
		});
	}

	resetPassword() {
		return this.props.resetPassword ? this.props.resetPassword() : false;
	}

	formRender(modaID: string) {
		// VARS
		let state = this.state;
		let message: string = '';
		let successfull = true;
		let editUserFlag = !!this.props.userData;
		if (state && state.error != null && state.error.message) {
			message = state.error.message;
			successfull = state.error.successfull;
		}
		return (
			<div id={this.props.modalID} className="modal fade" data-backdrop="static">
				<div className="modal-dialog">
					<div className="modal-content">
						<Form validate={this.validate} validateOnSubmit={true}>
							{
								(formApi: FormApi) => {
									this.formApi = formApi;
									return (
										<form onSubmit={(e) => { this.submit(e, formApi); }}>
											<div className="modal-header">
												<button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 className="modal-title"><FormattedMessage id={`comps.${modaID}.title`} /></h4>
											</div>
											<div className="modal-body">
												<ErrorHolder message={message} hide={successfull} />
												<div className="form-st1">
													<div className="row">
														<div className="col-sm-6">
															<InputMessage name="name" formApi={formApi}>
																<label><FormattedMessage id="general.form.name" /></label>
																<Text className="form-control" field="name" placeholder={this.props.intl.formatMessage({ id: 'general.form.name' })} />
															</InputMessage>
														</div>
														<div className="col-sm-6">
															<InputMessage name="username" formApi={formApi}>
																<label><FormattedMessage id="general.form.username" /></label>
																<Text className="form-control" field="username" placeholder={this.props.intl.formatMessage({ id: 'general.form.username' })} />
															</InputMessage>
														</div>
													</div>
													<div className="row">
														<div className="col-sm-6">
															<InputMessage name="email" formApi={formApi}>
																<label><FormattedMessage id="general.form.email" /></label>
																<Text className="form-control" field="email" type="email" placeholder={this.props.intl.formatMessage({ id: 'general.form.email' })} />
															</InputMessage>
														</div>
														<div className="col-sm-6">
															<InputMessage name="phoneNumber" formApi={formApi}>
																<label><FormattedMessage id="general.form.phoneNumber.label" /></label>
																<PhoneInput className="form-control" field="phoneNumber" defaultCountry="dk" placeholder={this.props.intl.formatMessage({ id: 'general.form.phoneNumber.placeholder' })} />
															</InputMessage>
														</div>
													</div>
													<InputMessage name="profileImagePath" formApi={formApi}>
														<label><FormattedMessage id="general.form.profileImage" /></label>
														<InputFile id="profileImagePath" field="profileImagePath" accept="image/*" />
													</InputMessage>
												</div>
											</div>
											<div className="modal-footer">
												<button type="button" className="btn btn-default" data-dismiss="modal"><FormattedMessage id="general.close" /></button>
												{editUserFlag ? <button type="button" onClick={this.resetPassword} className="btn btn-primary"><FormattedMessage id="general.resetPass" /></button> : ''}
												<button type="submit" className="btn btn-primary"><FormattedMessage id={`comps.${modaID}.submitButton`} /></button>
											</div>
										</form>
									);
								}
							}
						</Form>
						<Loader status={this.props.loaderStatus} />
					</div>
				</div>
			</div >
		);
	}

	render() {
		return this.formRender('newAdminUserModal');
	}
}

class EditAdminUser extends NewAdminUser {
	componentDidMount() {
		// CALL THE PARENT
		super.componentDidMount();
		// WHEN THE MODAL IS SHOWN
		$(`#${this.props.modalID}`).on('show.bs.modal', () => {
			if (this.formApi && this.props.userData) {
				this.formApi.setAllValues(this.props.userData);
			}
		});
	}

	validate = values => {
		return {
			name: values.name ? null : this.props.intl.formatMessage({ id: 'general.validation.name' }),
			username: values.username ? null : this.props.intl.formatMessage({ id: 'general.validation.username' }),
			email: values.email ? null : this.props.intl.formatMessage({ id: 'general.validation.email' }),
			phoneNumber: null,
			profileImagePath: null
		};
	}

	render() {
		return this.formRender('editAdminUserModal');
	}
}

const NewAdminUserModal = injectIntl(NewAdminUser);
const EditAdminUserModal = injectIntl(EditAdminUser);

export {
	NewAdminUserModal,
	EditAdminUserModal
};

interface State {
	error: null | {
		successfull: boolean;
		message: string;
	};
}
interface Props {
	modalID: string;
	handleSubmit: Function;
	loaderStatus: boolean;
	userData?: any;
	resetPassword?: Function;
}
