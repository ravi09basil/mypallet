import * as React from 'react';
import { Redirect, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { GetAdministrationUserInfoPayload } from '../../scenes/login/types';
import routes from '../../routes';

class Auth extends React.Component {
	props: Props;

	render() {
		return (
			(this.props.userInfo && this.props.userInfo.shouldChangePassword === false) ? this.props.children : <Redirect to={{ 'pathname': routes.LOGIN.url, 'state': { 'from': this.props.location } }} />
		);
	}
}

const mapStateToProps = (state) => {
	return {
		'userInfo': state.Login.administrationUserInfo
	};
};

export default connect(mapStateToProps)(Auth);

interface Actions {
}
type PropsType = {
	userInfo: GetAdministrationUserInfoPayload
};
type PathParamsType = {};
type Props = RouteComponentProps<PathParamsType> & Actions & React.Props<{}> & PropsType;
