import * as React from 'react';
import { Field, FieldApi } from 'react-form';

class InputAddress extends React.Component<Props> {
	e: HTMLInputElement;
	fieldApi: FieldApi;
	autocomplete: google.maps.places.Autocomplete;

	constructor(props: Props) {
		super(props);
		this.fillInAddress = this.fillInAddress.bind(this);
	}

	componentDidMount() {
		// Create the autocomplete object, restricting the search to geographical
		// location types.
		this.autocomplete = new google.maps.places.Autocomplete(this.e, { types: ['geocode'] });
		// When the user selects an address from the dropdown, populate the address
		// fields in the form.
		this.autocomplete.addListener('place_changed', this.fillInAddress);
		// if user tried to clear the address
		this.e.addEventListener('input', () => {
			this.fieldApi.setValue(false);
		});
	}

	componentWillReceiveProps() {
		// GET THE ADDRESS FROM GOOGLE
		let checkAddress = () => {
			let value = this.fieldApi.value;
			if (typeof value === 'string') {
				var geocoder = new google.maps.Geocoder();
				geocoder.geocode({ 'address': value }, (results: google.maps.GeocoderResult[], status: any) => {
					if (status === 'OK') {
						this.fieldApi.setValue(results[0]);
						this.e.value = value;
					}
				});
				this.fieldApi.setValue(false);
			}
		};
		setTimeout(checkAddress, 100);
	}

	fillInAddress() {
		// Get the place details from the autocomplete object.
		var place = this.autocomplete.getPlace();
		// SET THE VALUE
		this.fieldApi.setValue(place);
		// CHECK IF THE USER HAVE ON CHANGE EVENT
		if (this.props.onSelectAddress) {
			this.props.onSelectAddress(place);
		}
	}

	render() {
		const { onSelectAddress, onChange, field, ...rest } = this.props;
		return (
			<Field field={field}>
				{fieldApi => {
					this.fieldApi = fieldApi;
					return (
						<input ref={(e: HTMLInputElement) => { this.e = e; }} {...rest} type="search" autoComplete="off" />
					);
				}}
			</Field>
		);
	}
}

export default InputAddress;

interface Props extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
	field: string;
	onSelectAddress?(place: google.maps.places.PlaceResult);
}
