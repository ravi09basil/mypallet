import * as React from 'react';
// https://tomchentw.github.io/react-google-maps/#introduction
import { withGoogleMap, GoogleMap, Marker } from 'react-google-maps';

const GoogleMaps = (withGoogleMap((props: Props) => (
	<GoogleMap defaultZoom={props.defaultZoom} defaultCenter={props.defaultCenter}>{<Marker position={props.position} />}</GoogleMap>
)));

export default GoogleMaps;

interface Props {
	defaultZoom: number;
	defaultCenter: { lat: number, lng: number };
	position: { lat: number, lng: number };
	containerElement: JSX.Element;
	mapElement: JSX.Element;
}