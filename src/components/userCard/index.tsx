import * as React from 'react';
import './style.scss';

class UserCard extends React.Component<Props> {
	render() {
		let userImagePath = this.props.userImagePath;
		return (
			<div className="user-holder">
				<div className="user-title">{this.props.title}</div>
				<div className="media user-info">
					<div className="media-left user-img-holder">
						<img className="user-img" src={userImagePath ? userImagePath : `${process.env.PUBLIC_URL}/assets/img/user-image-with-black-background.png`} alt={this.props.username} />
					</div>
					<div className="media-body user-name">
						{this.props.username}
						<br />
						{this.props.companyName}
					</div>
				</div>
			</div>
		);
	}
}

export default UserCard;

interface Props {
	title: string;
	userImagePath: string;
	username: string;
	companyName: string;
}
