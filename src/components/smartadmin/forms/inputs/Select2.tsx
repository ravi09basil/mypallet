import * as React from 'react';
import * as $ from 'jquery';
import 'select2';

export default class Select2 extends React.Component<Props> {
	_input;

	componentDidMount() {
		var $eventSelect = $(this._input).select2(this.props.options || {});
		// $eventSelect.on('select2:open', function (e: object) { console.log('select2:open', e); });
		// $eventSelect.on('select2:close', function (e: object) { console.log('select2:close', e); });
		$eventSelect.on('select2:select', (e) => {
			if (this.props.onSelect) {
				this.props.onSelect(e);
			}
		});
		$eventSelect.on('select2:unselect', (e) => {
			if (this.props.onUnselect) {
				this.props.onUnselect(e);
			}
		});
	}

	componentWillUnmount() {
		$(this._input).select2('destroy');
	}

	render() {
		let { children } = this.props;
		return (
			<select ref={input => this._input = input}>
				{children}
			</select>
		);
	}
}

type Props = {
	className?: string;
	options?: object;
	onSelect?: Function;
	onUnselect?: Function;
};
