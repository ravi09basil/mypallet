import * as React from 'react';

export default class UiTabs extends React.Component<Props> {
	elem;

	render() {
		let { children, ...rest } = this.props;
		return (
			<div ref={(el) => { this.elem = el; }} {...rest}>
				{children}
			</div>
		);
	}
}

interface Props extends React.HTMLAttributes<{}> {

}
