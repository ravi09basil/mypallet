/// <reference types="jquery"/>

interface JQueryStatic {
	smallBox(data: object, cb: Function | undefined): JQuery;
	bigBox(data: object, cb: Function | undefined): JQuery;
	SmartMessageBox(data: object, cb: Function | undefined): JQuery;
}
