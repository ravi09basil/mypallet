/// <reference path="MessageActions.d.ts" />
require('smartadmin-plugins/notification/SmartNotification.min.js');

export function smallBox(data: object, cb: Function | undefined = undefined) {
	$.smallBox(data, cb);
}

export function bigBox(data: object, cb: Function | undefined = undefined) {
	$.bigBox(data, cb);
}

export function SmartMessageBox(data: object, cb: Function | undefined = undefined) {
	$.SmartMessageBox(data, cb);
}
