import * as React from 'react';
import { CustomerInfoPayload } from '../../scenes/customer/types';
import { InjectedIntlProps, FormattedMessage, injectIntl } from 'react-intl';
import Loader from '../loader';
import './style.scss';

class CustomerInfo extends React.Component<Props & InjectedIntlProps> {
	render() {
		return (
			<div className="customer-info relative">
				<div className="jarviswidget">
					<header>
						<h2 className="title"><FormattedMessage id="general.customer" /></h2>
					</header>
					<div>
						<div className="widget-body no-padding">
							<form className="smart-form">
								<fieldset>
									<section>
										<div className="row">
											<div className="col col-4">
												<div className="form-group input input-block">
													<label className="mb-5"><FormattedMessage id="general.form.name" /></label>
													<input readOnly={true} type="text" className="filter form-control" placeholder={this.props.intl.formatMessage({ id: 'general.form.name' })} defaultValue={this.props.data.name} />
												</div>
											</div>
											<div className="col col-8">
												<div className="form-group input input-block">
													<label className="mb-5"><FormattedMessage id="general.form.address" /></label>
													<input readOnly={true} type="text" className="filter form-control" placeholder={this.props.intl.formatMessage({ id: 'general.form.address' })} defaultValue={this.props.data.address} />
												</div>
											</div>
										</div>
									</section>
									<section>
										<div className="row">
											<div className="col col-4">
												<div className="form-group input input-block">
													<label className="mb-5"><FormattedMessage id="general.form.zipCode" /></label>
													<input readOnly={true} type="text" className="filter form-control" placeholder={this.props.intl.formatMessage({ id: 'general.form.zipCode' })} defaultValue={this.props.data.postCode} />
												</div>
											</div>
											<div className="col col-8">
												<div className="form-group input input-block">
													<label className="mb-5"><FormattedMessage id="general.form.city" /></label>
													<input readOnly={true} type="text" className="filter form-control" placeholder={this.props.intl.formatMessage({ id: 'general.form.city' })} defaultValue={this.props.data.city} />
												</div>
											</div>
										</div>
									</section>
									<section>
										<div className="row">
											<div className="col col-4">
												<div className="form-group input input-block">
													<label className="mb-5"><FormattedMessage id="general.land" /></label>
													<input readOnly={true} type="text" className="filter form-control" placeholder={this.props.intl.formatMessage({ id: 'general.land' })} defaultValue={this.props.data.country} />
												</div>
											</div>
											<div className="col col-8">
												<div className="form-group input input-block">
													<label className="mb-5"><FormattedMessage id="general.phoneNumber" /></label>
													<input readOnly={true} type="text" className="filter form-control" placeholder={this.props.intl.formatMessage({ id: 'general.phoneNumber' })} defaultValue={this.props.data.phoneNumber} />
												</div>
											</div>
										</div>
									</section>
									<section>
										<div className="row">
											<div className="col col-4">
												<div className="form-group input input-block">
													<label className="mb-5"><FormattedMessage id="general.contactPerson" /></label>
													<input readOnly={true} type="text" className="filter form-control" placeholder={this.props.intl.formatMessage({ id: 'general.contactPerson' })} defaultValue={this.props.data.contactPerson} />
												</div>
											</div>
											<div className="col col-8">
												<div className="form-group input input-block">
													<label className="mb-5"><FormattedMessage id="general.form.vatNumber" /></label>
													<input readOnly={true} type="text" className="filter form-control" placeholder={this.props.intl.formatMessage({ id: 'general.form.vatNumber' })} defaultValue={this.props.data.vatNumber} />
												</div>
											</div>
										</div>
									</section>
									<section>
										<div className="row">
											<div className="col col-10">
												<div className="form-group input input-block">
													<label className="mb-5"><FormattedMessage id="general.form.email" /></label>
													<input readOnly={true} type="text" className="filter form-control" placeholder={this.props.intl.formatMessage({ id: 'general.form.email' })} defaultValue={this.props.data.email} />
												</div>
											</div>
										</div>
									</section>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
				<Loader status={this.props.customerInfoFetchStatus} />
			</div>
		);
	}
}

export default injectIntl(CustomerInfo);

interface Props {
	data: CustomerInfoPayload;
	customerInfoFetchStatus: boolean;
}
