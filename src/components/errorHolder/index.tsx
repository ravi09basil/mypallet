import * as React from 'react';

class ErrorHolder extends React.Component<Props> {
	render() {
		return (this.props.message && !this.props.hide) ? (
			<div className="alert alert-danger alert-block">
				<p>{this.props.message}</p>
			</div>
		) : '';
	}
}

export default ErrorHolder;

interface Props {
	hide: boolean;
	message: string;
}
