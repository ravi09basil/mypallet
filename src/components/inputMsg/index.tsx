import * as React from 'react';
import { FormApi } from 'react-form';

class InputMessage extends React.Component<Props> {
	errorHandler(formApi: FormApi, name: string, callback: Function) {
		let str = '';
		if (formApi.validationFailed || formApi.errors) {
			let clone = Object.assign(formApi.validationFailed || {}, formApi.errors || {});
			str = clone[name] ? callback(clone[name]) : '';
		}
		return str;
	}

	render() {
		return (
			<div className={`form-group ${this.errorHandler(this.props.formApi, this.props.name, () => ('has-error'))}`}>
				{this.props.children}
				{this.errorHandler(this.props.formApi, this.props.name, (val) => (<small className="help-block">{val}</small>))}
			</div>
		);
	}
}

export default InputMessage;

interface Props {
	name: string;
	formApi: FormApi;
}
