// VARS
const account = 'mypallet';
const sasKey = process.env.REACT_APP_SAS as string;
const blobUri = `https://${account}.blob.core.windows.net`;
/* tslint:disable */ let AzureStorage = window['AzureStorage'].Blob;
var blobService = AzureStorage.createBlobServiceWithSas(blobUri, sasKey);
export const conatiners = {
	userscontainer: 'users-images-container',
	companyLogo: 'company-logo-container'
};

export const azureCreateBlobFromBrowserFile = function (container: string, imageName: string, file: File, callback: Function) {
	azureCreateContainerIfNotExists(container, (error: any) => {
		if (error) {
			alert(error.message);
		} else {
			blobService.createBlockBlobFromBrowserFile(container, imageName, file, callback);
		}
	});
}

export const azureCreateContainerIfNotExists = function (conatinerName: string, callback: Function) {
	blobService.createContainerIfNotExists(conatinerName, { publicAccessLevel: 'blob' }, callback);
}

export const AzureURL = (imageName: string, container: string) => (`https://${account}.blob.core.windows.net/${container}/${imageName}`);
