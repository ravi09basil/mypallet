export const appStateKey = 'appState';

export const getUserState = () => {
	try {
		if (localStorage[appStateKey]) {
			return JSON.parse(localStorage[appStateKey]);
		}
	} catch (e) {
		return undefined;
	}
};
