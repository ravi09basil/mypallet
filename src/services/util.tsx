export const returnGoogleLocationDetails = (location: google.maps.places.PlaceResult) => {
	// VARS
	let address = location.name;
	let zipCode;
	let city;
	let country;
	let lat: any = location.geometry.location.lat;
	let lng: any = location.geometry.location.lng;
	// LOOP
	location.address_components.forEach((i) => {
		if (i.types.indexOf('postal_code') !== -1) {
			zipCode = i;
		} else if (i.types.indexOf('locality') !== -1) {
			city = i;
		} else if (i.types.indexOf('country') !== -1) {
			country = i;
		}
	});
	return {
		address: address,
		city: city ? city.long_name : '',
		country: country ? country.long_name : '',
		zipCode: zipCode ? zipCode.long_name : '',
		latitude: lat,
		longitude: lng
	};
};
