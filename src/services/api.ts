import { bigBox } from '../components/smartadmin/utils/actions/MessageActions';

export class API {
	GET(request: RequestInfo, options: ApiOptions = {}) {
		let globalOptions: ApiOptions = {
			method: 'get'
		};
		// QUERY STRING
		if (options.queryString !== undefined) {
			var params = Object.keys(options.queryString)
				.map((key) => (options.queryString && options.queryString[key]) ? `${encodeURIComponent(key)}=${encodeURIComponent(options.queryString[key])}` : '')
				.join('&')
				.replace(/%20/g, '+');
			request = `${request}?${params}`;
		}
		let mergeOptions = Object.assign(options, globalOptions);
		return this.SEND(request, mergeOptions);
	}

	POST(request: RequestInfo, options: ApiOptions = {}) {
		let globalOptions = {
			method: 'post'
		};
		let mergeOptions = Object.assign(options, globalOptions);
		return this.SEND(request, mergeOptions);
	}

	PUT(request: RequestInfo, options: ApiOptions = {}) {
		let globalOptions = {
			method: 'put'
		};
		let mergeOptions = Object.assign(options, globalOptions);
		return this.SEND(request, mergeOptions);
	}

	DELETE(request: RequestInfo, options: ApiOptions = {}) {
		let globalOptions = {
			method: 'delete'
		};
		let mergeOptions = Object.assign(options, globalOptions);
		return this.SEND(request, mergeOptions);
	}

	private handleNotification(fetchPromise: Promise<any>) {
		// HANDLE ERRORS
		return fetchPromise.catch((error) => {
			bigBox({
				title: 'A failure occurred during initialization of services',
				content: error.message,
				color: '#C46A69',
				icon: 'fa fa-warning shake animated',
				timeout: 6000
			});
			return error;
		}).then((res) => {
			if (res.hasOwnProperty('successfull') && !!!res.successfull && res.message) {
				bigBox({
					title: 'A failure occurred during initialization of services',
					content: res.message,
					color: '#c79121',
					icon: 'fa fa-warning shake animated',
					timeout: 6000
				});
			}
			return res;
		});
	}

	private SEND(request: RequestInfo, options: ApiOptions = {}) {
		// VARS
		let appType = 'application/json';
		let globalOptions = {
			mode: 'cors',
			credentials: 'same-origin',
			headers: new Headers({
				'accept': appType,
				'Content-Type': appType
			})
		};
		let mergeOptions = Object.assign(globalOptions, options);
		let fetchPromise = fetch(request, mergeOptions).then(res => {
			// CHECK CONTENT TYPE
			// RETURN THE JSON OBJECT
			return (mergeOptions.headers.get('Content-Type') === appType) ? res.json() : res;
		});
		// HANDLE NOTIFICATIONS
		return this.handleNotification(fetchPromise);
	}
}

interface ApiOptions extends RequestInit {
	queryString?: object | string | number;
	method?: string;
}
