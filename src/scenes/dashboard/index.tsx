import * as React from 'react';
import Clock from 'react-live-clock';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import CompanyLogo from '../../components/companyLogo';
import { GetAdministrationUserInfoPayload } from '../login/types';
import './style.scss';

class Dashboard extends React.Component<Props> {
	render() {
		let companyLogo = (this.props.userInfo.companyLogo) ? this.props.userInfo.companyLogo : 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/300px-No_image_available.svg.png';
		return (
			<div className="page-layout dashboard-page">
				<div className="companyLogo">
					<CompanyLogo companyLogo={companyLogo} />
					<h1 className="welcomeText"><FormattedMessage id="scenes.dashboard.welcome" /></h1>
				</div>
				<Clock className="clock" format={'DD MMM, YYYY, HH:mm:ss'} ticking={true} timezone={'Europe/Copenhagen'} />
				<img className="dashboard-bg" src="../../assets/img/my-pallet/background.jpg" />
			</div>
		);
	}
}
const mapStateToProps = (state) => {
	return {
		'userInfo': state.Login.administrationUserInfo
	};
};

export default connect(mapStateToProps)(Dashboard);

interface Props {
	userInfo: GetAdministrationUserInfoPayload;
}