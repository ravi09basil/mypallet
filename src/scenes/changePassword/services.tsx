import { API } from '../../services/api';
import API_URLS from '../../config/apiUrls';
import { PostChangeAdministrationUserPasswordPayload } from './types';

export class ChangePasswordSrvc {
	API: API;

	constructor() {
		this.API = new API();
	}

	PostChangeAdministrationUserPassword(userID: string, newPassword: string): Promise<PostChangeAdministrationUserPasswordPayload> {
		return this.API.POST(API_URLS.PostChangeAdministrationUserPassword, {
			body: JSON.stringify({
				userID,
				newPassword
			})
		});
	}
}
