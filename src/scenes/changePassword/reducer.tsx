import { ActionType } from '../../config/types';
import { combineReducers } from 'redux';
import C from './constants';

function changePassword(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.CHANGE_PASSWORD_FETCH_SUCCESS:
			return action.payload;
		case C.CHANGE_PASSWORD_FETCH_FAILURE:
			return action.payload;
		default:
			return state;
	}
}

function changePasswordFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.CHANGE_PASSWORD_FETCH_STATUS:
			return action.payload;
		default:
			return state;
	}
}

const ChangePassword = {
	changePassword,
	changePasswordFetchStatus
};

export default combineReducers(ChangePassword);
