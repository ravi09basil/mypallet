import C from './constants';
import { ChangePasswordSrvc } from './services';
import { GetAdministrationUserInfo } from '../login/actions';

export const changeAdminUserPassword = (userID: string, newPassword: string) => dispatch => {
	// VARS
	let loginSrvc: ChangePasswordSrvc = new ChangePasswordSrvc();
	// LOADING STATUS
	dispatch({
		type: C.CHANGE_PASSWORD_FETCH_STATUS,
		payload: true
	});
	// SEND
	return loginSrvc.PostChangeAdministrationUserPassword(userID, newPassword).then(res => {
		// SAVE AUTHENTICATION
		dispatch({
			type: C.CHANGE_PASSWORD_FETCH_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.CHANGE_PASSWORD_FETCH_STATUS,
			payload: false
		});
		return GetAdministrationUserInfo(userID)(dispatch);
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.CHANGE_PASSWORD_FETCH_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.CHANGE_PASSWORD_FETCH_STATUS,
			payload: false
		});
	});
};
