import ChangePasswordTemplate from './comp';
import { changeAdminUserPassword } from './actions';
import { connect } from 'react-redux';

const mapDispatchToProps = (dispatch): Actions => ({
	changeAdminUserPassword(userID: string, newPassword: string) {
		return dispatch(changeAdminUserPassword(userID, newPassword));
	}
});

const mapStateToProps = (state) => {
	return {
		'AdministrationUserInfo': state.Login.administrationUserInfo,
		'ChangePassword': state.ChangePassword.changePassword,
		'ChangePasswordFetchStatus': state.ChangePassword.changePasswordFetchStatus
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(ChangePasswordTemplate);

type Actions = {
	changeAdminUserPassword: Function;
};
