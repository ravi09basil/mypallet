import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Form, Text, FormApi } from 'react-form';
import routes from '../../routes';
import Loader from '../../components/loader';
import ErrorHolder from '../../components/errorHolder';
import InputMessages from '../../components/inputMsg';
import { GetAdministrationUserInfoPayload } from '../login/types';
import { PostChangeAdministrationUserPasswordPayload } from './types';
import { InjectedIntlProps, FormattedMessage, injectIntl } from 'react-intl';
import './style.scss';

class ChangePasswordTemplate extends React.Component<Props & InjectedIntlProps> {
	componentWillMount() {
		this.redirectUser();
	}

	redirectUser() {
		// VARS
		let URL;
		let ChangePassword = this.props.ChangePassword;
		let AdministrationUserInfo = this.props.AdministrationUserInfo;
		// CHECK IF THE USER IS AUTHONTICATED
		if (AdministrationUserInfo) {
			if (ChangePassword && ChangePassword.successfull) {
				URL = routes.DASHBOARD.url;
			}
		} else {
			URL = routes.LOGIN.url;
		}
		if (URL) {
			// REDIRECT THE USER
			this.props.history.push(URL);
		}
	}

	validate = (values: FormFields) => {
		// PASSWORD RULES
		let confirmPassRule;
		if (!values.confirmPass) {
			confirmPassRule = this.props.intl.formatMessage({ id: 'scenes.changePassword.validation.confirmPass.required' });
		} else if (values.confirmPass !== values.newPassword) {
			confirmPassRule = this.props.intl.formatMessage({ id: 'scenes.changePassword.validation.confirmPass.match' });
		}
		return {
			newPassword: values.newPassword ? null : this.props.intl.formatMessage({ id: 'scenes.changePassword.validation.newPassword' }),
			confirmPass: confirmPassRule
		};
	}

	handleSubmit(e: FormFields) {
		// LOGIN
		this.props.changeAdminUserPassword(this.props.AdministrationUserInfo.id, e.newPassword).then(() => {
			// REDIRECT THE USER
			this.redirectUser();
		});
	}

	render() {
		// VARS
		var message: string = '';
		var successfull = true;
		var ChangePassword = this.props.ChangePassword;
		if (ChangePassword) {
			message = ChangePassword.message;
			successfull = ChangePassword.successfull;
		}

		return (
			<div className="page-layout login-page">
				<Form onSubmit={(e) => { this.handleSubmit(e); }} validate={this.validate} validateOnSubmit={true}>
					{
						(formApi: FormApi) => {
							return (
								<div className="jarviswidget" role="widget">
									<header role="heading">
										<h2><FormattedMessage id="scenes.changePassword.title" /></h2>
									</header>
									<div role="content">
										<div className="widget-body no-padding">
											<form className="form-st1 p-25" onSubmit={formApi.submitForm}>
												<fieldset>
													<ErrorHolder message={message} hide={successfull} />
													<InputMessages name="newPassword" formApi={formApi}>
														<label><FormattedMessage id="scenes.changePassword.form.newPass" /></label>
														<div className="input-group">
															<div className="input-group-addon"><i className="icon-prepend fa fa-lock" /></div>
															<Text className="form-control" field="newPassword" placeholder={this.props.intl.formatMessage({ id: 'scenes.changePassword.form.newPass' })} type="password" autoComplete="new-password" />
														</div>
													</InputMessages>
													<InputMessages name="confirmPass" formApi={formApi}>
														<label><FormattedMessage id="scenes.changePassword.form.confPass" /></label>
														<div className="input-group">
															<div className="input-group-addon"><i className="icon-prepend fa fa-lock" /></div>
															<Text className="form-control" field="confirmPass" placeholder={this.props.intl.formatMessage({ id: 'scenes.changePassword.form.confPass' })} type="password" autoComplete="new-password" />
														</div>
													</InputMessages>
												</fieldset>
												<footer className="text-right">
													<button type="submit" className="btn btn-success btn-lg"><FormattedMessage id="general.change" /></button>
												</footer>
											</form>
										</div>
									</div>
								</div>
							);
						}
					}
				</Form>
				<Loader status={this.props.ChangePasswordFetchStatus} />
			</div>
		);
	}
}

export default injectIntl(ChangePasswordTemplate);

interface Actions {
	changeAdminUserPassword(userID: string, newPassword: string);
}
type PropsType = {
	AdministrationUserInfo: GetAdministrationUserInfoPayload;
	ChangePassword: PostChangeAdministrationUserPasswordPayload;
	ChangePasswordFetchStatus: boolean;
};
type Props = RouteComponentProps<{}> & Actions & React.Props<{}> & PropsType;
type FormFields = {
	newPassword: string;
	confirmPass: string;
};
