import { API } from '../../services/api';
import apisUrls from '../../config/apiUrls';
import { GetReceiptsPayload, SendReceiptPayload } from './types';
import { GetTransactionInfoPayload } from '../customer/types';

export class ReceiptsSrvc {
	API: API;

	constructor() {
		this.API = new API();
	}

	getReceiptsList(CompanyID: string): Promise<GetReceiptsPayload> {
		return this.API.GET(apisUrls.GetReceipts, {
			queryString: { CompanyID }
		});
	}

	getTransactionInfo(TransactionID: string): Promise<GetTransactionInfoPayload> {
		return this.API.GET(apisUrls.GetTransactionInfo, {
			queryString: { TransactionID }
		});
	}

	sendReceipt(Payload: SendReceiptPayload) {
		return this.API.POST(apisUrls.PostSendReceipt, {
			body: JSON.stringify(Payload)
		});
	}
}
