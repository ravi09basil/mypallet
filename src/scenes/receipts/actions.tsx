import C from './constants';
import { ReceiptsSrvc } from './services';
import { SendReceiptPayload } from './types';

export const getReceiptsList = (CompanyID: string) => dispatch => {
	// VARS
	let srvc: ReceiptsSrvc = new ReceiptsSrvc();
	// LOADING STATUS
	dispatch({
		type: C.RECEIPTS_FETCH_DATA_STATUS,
		payload: true
	});
	// SEND
	return srvc.getReceiptsList(CompanyID).then(res => {
		// SAVE
		dispatch({
			type: C.RECEIPTS_FETCH_DATA_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.RECEIPTS_FETCH_DATA_STATUS,
			payload: false
		});
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.RECEIPTS_FETCH_DATA_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.RECEIPTS_FETCH_DATA_STATUS,
			payload: false
		});
	});
};
export const sendReceipt = (SendReceipt: SendReceiptPayload)  => {
	let srvc: ReceiptsSrvc = new ReceiptsSrvc();
	console.log(srvc.sendReceipt(SendReceipt));
};

export const getTransactionInfo = (TransactionID: string) => dispatch => {
	// VARS
	let Srvc: ReceiptsSrvc = new ReceiptsSrvc();
	// LOADING STATUS
	dispatch({
		type: C.RECEIPTS_TRANSACTION_INFO_FETCH_DATA_STATUS,
		payload: true
	});
	// SEND
	return Srvc.getTransactionInfo(TransactionID).then(res => {
		// SAVE
		dispatch({
			type: C.RECEIPTS_TRANSACTION_INFO_FETCH_DATA_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.RECEIPTS_TRANSACTION_INFO_FETCH_DATA_STATUS,
			payload: false
		});
		return res;
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.RECEIPTS_TRANSACTION_INFO_FETCH_DATA_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.RECEIPTS_TRANSACTION_INFO_FETCH_DATA_STATUS,
			payload: false
		});
		return error;
	});
};
