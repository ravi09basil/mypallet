import { connect } from 'react-redux';
import { getReceiptsList, getTransactionInfo, sendReceipt } from './actions';
import ReceiptsTemplate from './comp';
import { SendReceiptPayload } from './types';

const mapDispatchToProps = (dispatch, props) => {
	return ({
		getReceiptsList(CompanyID: string) {
			return dispatch(getReceiptsList(CompanyID));
		},
		getTransactionInfo(TransactionID: string) {
			return dispatch(getTransactionInfo(TransactionID));
		},
		sendReceipt(SendReceipt: SendReceiptPayload) {
			return sendReceipt(SendReceipt);
		}

	});
};

const mapStateToProps = (state) => {
	return {
		'companyID': state.Login.authentication.companyID,
		'receiptsList': state.Receipts.receiptsList,
		'receiptsFetchStatus': state.Receipts.receiptsFetchStatus,
		'transactionInfo': state.Receipts.transactionInfo,
		'transactionInfoFetchStatus': state.Receipts.transactionInfoFetchStatus
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(ReceiptsTemplate);
