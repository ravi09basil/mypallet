import * as React from 'react';
import moment from 'moment-timezone';
import { GridOptions, ICellRendererParams } from 'ag-grid';
import { InjectedIntlProps, injectIntl } from 'react-intl';
import Loader from '../../components/loader';
import AgGridTheme from '../../components/agGridTheme';
import { GetTransactionInfoPayload } from '../customer/types';
import TransactionInfoModal from '../../components/transactionInfoModal';
import { GetReceiptsPayload, SendReceiptPayload } from './types';
import { EnterEmail } from '../../components/enterEmailModal';

class ReceiptsTemplate extends React.Component<Props & InjectedIntlProps> {
	TransactionInfoModalID = 'TransactionInfoModal';
	EnterEmailModalID = 'EnterEmailModal';
	state: State = {
		filterVal: '',
		transactionInfo: undefined,
		transactionID: undefined,
		gridOptions: {
			enableSorting: true,
			enableFilter: true,
			onGridReady: () => {
				if (this.state.gridOptions.columnApi) {
					setTimeout(() => { if (this.state.gridOptions.api) { this.state.gridOptions.api.resetRowHeights(); if (this.state.gridOptions.columnApi) { this.state.gridOptions.columnApi.autoSizeAllColumns(); } if (this.state.gridOptions.api) { this.state.gridOptions.api.sizeColumnsToFit(); } } }, 500);
				}
			},
			columnDefs: [
				{
					headerName: this.props.intl.formatMessage({ id: 'general.info' }),
					field: 'info',
					cellRenderer: (params: ICellRendererParams) => {
						// VARS
						let divHolder = document.createElement('div');
						let transactionID = params.data.transactionID;
						let link = document.createElement('a');
						// LINK
						link.className = 'mr-10 fa fa-lg fa-info-circle color-black';
						link.onclick = (e) => {
							// OPEN THE MODAL
							this.openInfoModal(transactionID);
							// DISABLE THE DEFAULT ACTION
							e.preventDefault();
						};
						// HOLDER
						divHolder.className = 'text-center';
						divHolder.appendChild(link);
						return divHolder;
					}
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.transactionID' }),
					field: 'transactionID'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.sender' }),
					field: 'sendingCompanyName'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.reciever' }),
					field: 'receivingCompanyName'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.land' }),
					field: 'receivingCompanyCountry'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.ref' }),
					field: 'receivingUserName'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.time' }),
					field: 'confirmedAt',
					cellRenderer: (params: ICellRendererParams) => {
						return moment(params.value).format('HH:mm:ss');
					}
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.date' }),
					field: 'confirmedAt',
					cellRenderer: (params: ICellRendererParams) => {
						return moment(params.value).format('L');
					}
				},
				{
					headerName: '',
					field: 'actions',
					cellRenderer: (params: ICellRendererParams) => {
						// VARS
						let divHolder = document.createElement('div');
						let transactionID = params.data.transactionID;
						let sendAsMail = document.createElement('a');
						// LINK
						sendAsMail.className = 'fa fa-lg fa-envelope btn btn-primary btn-s';
						sendAsMail.onclick = (e) => {
							// OPEN THE MODAL
							this.setState({ transactionID: transactionID });
							this.openEnterEmailModal(transactionID);
							// DISABLE THE DEFAULT ACTION
							e.preventDefault();
						};
						divHolder.className = 'text-center';
						divHolder.appendChild(sendAsMail);
						return divHolder;
					}
				}
			]
		}
	};
	constructor(props: Props & InjectedIntlProps) {
		super(props);
		this.sendReceipt = this.sendReceipt.bind(this);
	}
	componentDidMount() {
		this.props.getReceiptsList(this.props.companyID);
		// WHEN THE MODAL IS CLOSED
		$(`#${this.TransactionInfoModalID}`).on('hidden.bs.modal', () => {
			// RESRT
			this.setState({
				transactionInfo: undefined
			});
		});
	}

	sendReceipt(SendReceipt: SendReceiptPayload) {
		console.log(this.props);

		let payload: SendReceiptPayload = {
			transactionID: SendReceipt.transactionID,
			email: SendReceipt.email,
		};
		console.log(payload);

		this.props.sendReceipt(payload);
	}

	openEnterEmailModal(transactionID: string) {
		$(`#${this.EnterEmailModalID}`).modal('show');
		this.state.transactionID = transactionID;
	}

	filterTheGrid(filterKey: string, value: string, key: string) {
		// VARS
		let gridApi = this.state.gridOptions.api;
		if (gridApi) {
			// FILTER THE GRID
			gridApi.setFilterModel({
				[filterKey]: {
					type: 'equals',
					filter: value
				}
			});
			gridApi.onFilterChanged();
			// SAVE THE FILTER VALUE
			this.setState({
				filterVal: key
			});
		}
	}

	searchTheGrid(e: React.SyntheticEvent<{ value: string }>) {
		// VARS
		let gridApi = this.state.gridOptions.api;
		if (gridApi) {
			gridApi.setQuickFilter(e.currentTarget.value);
		}
	}

	openInfoModal(TransactionID: string) {
		// OPEN THE MODAL
		$(`#${this.TransactionInfoModalID}`).modal('show');
		// GET TRANSATION INFO
		this.props.getTransactionInfo(TransactionID).then(res => {
			this.setState({
				transactionInfo: this.props.transactionInfo
			});
		});
	}

	render() {
		let receipts = (this.props.receiptsList && this.props.receiptsList.receipts) ? this.props.receiptsList.receipts : [];
		return (
			<div className="page-layout customers-page">
				<div className="container-fluid">
					<div className="row mb-15">
						<div className="col-sm-2">
							<input type="text" className="filter form-control" placeholder={this.props.intl.formatMessage({ id: 'general.searchFields' })} onChange={(e) => { this.searchTheGrid(e); }} />
						</div>
					</div>
					<div className="row mb-15">
						<div className="col-sm-12">
							<div className="grid-st1 ag-theme-balham">
								<AgGridTheme rowData={receipts} gridOptions={this.state.gridOptions} />
							</div>
						</div>
					</div>
				</div>
				<EnterEmail modalID={this.EnterEmailModalID} handleSubmit={this.sendReceipt} transactionID={this.state.transactionID} />
				<TransactionInfoModal modalID={this.TransactionInfoModalID} loaderStatus={this.props.transactionInfoFetchStatus} data={this.state.transactionInfo} />
				<Loader status={this.props.receiptsFetchStatus} />
			</div>
		);
	}
}

export default injectIntl(ReceiptsTemplate);

interface State {
	gridOptions: GridOptions;
	filterVal: string;
	transactionInfo: undefined | GetTransactionInfoPayload;
	transactionID: string |  undefined;
}
interface Actions {
	getReceiptsList(CompanyID: string);
	getTransactionInfo(TransactionID: string);
	sendReceipt(SendReceipt: SendReceiptPayload);
}
interface Props extends Actions {
	companyID: string;
	receiptsList: GetReceiptsPayload;
	receiptsFetchStatus: boolean;
	transactionInfoFetchStatus: boolean;
	transactionInfo: GetTransactionInfoPayload;
}
