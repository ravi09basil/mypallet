import { ActionType } from '../../config/types';
import { combineReducers } from 'redux';
import C from './constants';

function receiptsList(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.RECEIPTS_FETCH_DATA_SUCCESS:
			return action.payload;
		case C.RECEIPTS_FETCH_DATA_FAILURE:
			return null;
		default:
			return state;
	}
}

function receiptsFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.RECEIPTS_FETCH_DATA_STATUS:
			return action.payload;
		default:
			return state;
	}
}

function transactionInfo(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.RECEIPTS_TRANSACTION_INFO_FETCH_DATA_SUCCESS:
			return action.payload;
		case C.RECEIPTS_TRANSACTION_INFO_FETCH_DATA_FAILURE:
			return null;
		default:
			return state;
	}
}

function transactionInfoFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.RECEIPTS_TRANSACTION_INFO_FETCH_DATA_STATUS:
			return action.payload;
		default:
			return state;
	}
}

const Receipts = {
	receiptsList,
	receiptsFetchStatus,
	transactionInfo,
	transactionInfoFetchStatus
};

export default combineReducers(Receipts);
