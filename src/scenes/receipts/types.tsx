export interface GetReceiptsPayload {
	receipts: {
		transactionID: string;
		receivingCompanyName: string;
		receivingCompanyCountry: string;
		receivingUserName: string;
		confirmedAt: string;
	}[];
	successfull: boolean;
	message: string;
}

export interface SendReceiptPayload {
	transactionID: string | undefined;
	email: string;
}