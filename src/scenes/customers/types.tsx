export interface CustomerInterfacePayload {
	name: string;
	address: string;
	postCode: string;
	city: string;
	country: string;
	phoneNumber: string;
	sum: number;
	isMyPalletCustomer: boolean;
	vatNumber: string;
	id: string;
	type: number;
	latitude: number;
	longitude: number;
	contactPerson?: any;
	hasPalletBank: boolean;
	status: number;
}

export interface GetCustomersListPayload {
	customers: CustomerInterfacePayload[];
	message: string;
	successfull: boolean;
}

export interface PostNewCustomerBody {
	name: string;
	address: string;
	postCode: string;
	city: string;
	country: string;
	phoneNumber: string;
	vatNumber: string;
	longitude: number;
	latitude: number;
}

export interface NewCustomerInterface {
	companyID: string;
	name: string;
	postCode: string;
	city: string;
	country: string;
	phoneNumber: string;
	vatNumber: string;
	longitude: number;
	latitude: number;
}

export interface PostNewCustomerInterfaceBody extends NewCustomerInterface {
	address: string;
}

export interface NewCustomerInterfaceBody extends NewCustomerInterface {
	address: google.maps.places.PlaceResult;
}

export interface GetCheckVATNumberPayload {
	isExsisting: boolean;
	customerInfo: {
		name: string;
		address: string;
		postCode: string;
		city: string;
		country: string;
		phonenumber: string;
		status: string;
		isMyPalletCustomer: boolean;
		vatNumber: string;
		id: string;
		type: string;
	};
	successfull: boolean;
	message: string;
}
