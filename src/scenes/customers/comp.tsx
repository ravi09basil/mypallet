import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { GridOptions, ICellRendererParams } from 'ag-grid';
import { InjectedIntlProps, FormattedMessage, injectIntl } from 'react-intl';
import Routes from '../../routes';
import Loader from '../../components/loader';
import GridTools from '../../components/gridTools';
import NewCustomer from '../../components/newCustomerModal';
import AgGridTheme from '../../components/agGridTheme';
import { bigBox } from '../../components/smartadmin/utils/actions/MessageActions';
import { GetCustomersListPayload, NewCustomerInterfaceBody, PostNewCustomerInterfaceBody } from './types';

class CustomersTemplate extends React.Component<Props & InjectedIntlProps & RouteComponentProps<{}>> {
	newCustomerModalID = 'newCustomerModalID';

	state: State = {
		filterVal: '',
		gridOptions: {
			onGridReady: () => {
				if (this.state.gridOptions.columnApi) {
					setTimeout(() => { if (this.state.gridOptions.api) { this.state.gridOptions.api.resetRowHeights(); if (this.state.gridOptions.columnApi) { this.state.gridOptions.columnApi.autoSizeAllColumns(); } if (this.state.gridOptions.api) { this.state.gridOptions.api.sizeColumnsToFit(); } } }, 500);
				}
			},
			rowHeight: 40,
			enableSorting: true,
			columnDefs: [
				{
					headerName: this.props.intl.formatMessage({ id: 'general.name' }),
					field: 'name',
					cellRenderer: (params: ICellRendererParams) => {
						var div = document.createElement('div');
						div.className = 'text-left';
						console.log(params.data.hasPalletBank);
						
						if (params.data.hasPalletBank) {
							var link = document.createElement('i');
							link.className = 'fa fa-university mr-10';
							div.appendChild(link);
						}

						var name = document.createElement('label');
						name.className = 'text-left';
						name.innerHTML = params.data.name,
							div.appendChild(name);
						return div;
					}
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.contactPerson' }),
					field: 'contactPerson',
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.address' }),
					field: 'address'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.postCode' }),
					field: 'postCode'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.city' }),
					field: 'city'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.country' }),
					field: 'country'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.phoneNumber' }),
					field: 'phoneNumber'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.usesmyPallet' }),
					field: 'isMyPalletCustomer',
					cellRenderer: (params: ICellRendererParams) => {
						return params.value ? '<div class="text-center"><span class="color-green fa fa-check"></span></div>' : '';
					}
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.sum' }),
					field: 'sum'
				},
				{
					field: 'status',
					hide: true,
				},
				{
					headerName: '',
					field: 'actions',
					cellRenderer: (params: ICellRendererParams) => {
						var div = document.createElement('div');
						div.className = 'text-center';
						var link = document.createElement('a');
						link.href = `${Routes.CUSTOMER.url.replace(':id', params.data.id)}`;
						link.className = 'btn btn-success btn-s';
						link.innerText = this.props.intl.formatMessage({ id: 'general.open' });
						link.addEventListener('click', (e) => {
							this.props.history.push(`${Routes.CUSTOMER.url.replace(':id', params.data.id)}`);
							e.preventDefault();
						});
						div.appendChild(link);
						return div;
					}
				}
			]
		}
	};

	constructor(props: Props & InjectedIntlProps & RouteComponentProps<{}>) {
		super(props);
		this.createNewCustomer = this.createNewCustomer.bind(this);
	}

	componentDidMount() {
		this.props.getCustomersList(this.props.companyID);
	}

	filterTheGrid(filterKey: string, value: string | boolean, key: string, cond: string = 'equals') {
		// VARS
		let gridApi = this.state.gridOptions.api;
		if (gridApi) {
			// FILTER THE GRID
			gridApi.setFilterModel({
				[filterKey]: {
					type: cond,
					filter: value
				}
			});
			gridApi.onFilterChanged();
			// SAVE THE FILTER VALUE
			this.setState({
				filterVal: key
			});
		}
	}

	activeFilterClass(filterValue: string) {
		if (filterValue === this.state.filterVal) {
			return 'btn-primary';
		}
		return 'btn-default';
	}

	searchTheGrid(e: React.SyntheticEvent<{ value: string }>) {
		// VARS
		let gridApi = this.state.gridOptions.api;
		if (gridApi) {
			gridApi.setQuickFilter(e.currentTarget.value);
		}
	}

	createNewCustomer(customer: NewCustomerInterfaceBody) {
		// VARS
		let address = customer.address;
		let zipCode;
		let city;
		let country;
		let lat: any = address.geometry.location.lat;
		let lng: any = address.geometry.location.lng;
		// LOOP
		address.address_components.forEach((i) => {
			if (i.types.indexOf('postal_code') !== -1) {
				zipCode = i.long_name;
			} else if (i.types.indexOf('administrative_area_level_1') !== -1) {
				city = i;
			} else if (i.types.indexOf('country') !== -1) {
				country = i.long_name;
			}
		});
		// CHANGE VALUES
		let newCustomer: PostNewCustomerInterfaceBody = {
			companyID: this.props.companyID,
			name: customer.name,
			postCode: zipCode,
			city: city ? city.long_name : '',
			country: country ? country : '',
			phoneNumber: customer.phoneNumber,
			vatNumber: customer.vatNumber,
			address: address.name,
			latitude: lat,
			longitude: lng
		};
		// POST
		return this.props.postNewCustomer(newCustomer).then(res => {
			if (res.successfull) {
				// NOTE
				bigBox({
					title: this.props.intl.formatMessage({ id: 'scenes.customers.newCustomer.title' }),
					content: this.props.intl.formatMessage({ id: 'scenes.customers.newCustomer.content' }),
					color: '#739E73',
					timeout: 15000,
					icon: 'fa fa-check'
				});
				// UPDATE CUSTOMER LIST
				this.props.getCustomersList(this.props.companyID);
			}
		});
	}

	render() {
		let rowData = (this.props.customersList && this.props.customersList.customers) ? this.props.customersList.customers : [];
		return (
			<div className="page-layout customers-page">
				<div className="container-fluid">
					<div className="row mb-15">
						<div className="col-sm-4">
							<div className="btn-group mb-5">
								<a className={`btn ${this.activeFilterClass('')}`} onClick={() => { this.filterTheGrid('status', '', ''); }}>
									<FormattedMessage id="general.showAll" />
								</a>
								<a className={`btn ${this.activeFilterClass('1')}`} onClick={() => { this.filterTheGrid('status', '1', '1'); }}>
									<FormattedMessage id="general.receivable" />
								</a>
								<a className={`btn ${this.activeFilterClass('2')}`} onClick={() => { this.filterTheGrid('status', '2', '2'); }}>
									<FormattedMessage id="general.owes" />
								</a>
								<a className={`btn ${this.activeFilterClass('3')}`} onClick={() => { this.filterTheGrid('isMyPalletCustomer', true, '3', 'notEqual'); }}>
									<FormattedMessage id="general.newCustomers" />
								</a>
							</div>
							<div className="row">
								<div className="col-sm-9">
									<input type="text" className="filter form-control" placeholder={this.props.intl.formatMessage({ id: 'general.search' })} onChange={(e) => { this.searchTheGrid(e); }} />
								</div>
							</div>
						</div>
						<div className="col-sm-8 text-right">
							<GridTools modalID={this.newCustomerModalID} gridOptions={this.state.gridOptions} />
						</div>
					</div>
					<div className="row mb-15">
						<div className="col-sm-12">
							<div className="grid-st1 ag-theme-balham">
								<AgGridTheme rowData={rowData} gridOptions={this.state.gridOptions} />
							</div>
							<div className="text-right">
								<FormattedMessage id="general.total" /> : {rowData.reduce((i, c) => i + c.sum, 0)}
							</div>
						</div>
					</div>
				</div>
				<NewCustomer modalID={this.newCustomerModalID} handleSubmit={this.createNewCustomer} loaderStatus={this.props.newCustomerFetchStatus} vatNumberCheck={this.props.getCheckVATNumber} />
				<Loader status={this.props.customersFetchStatus} />
			</div>
		);
	}
}

export default injectIntl(CustomersTemplate);

interface State {
	gridOptions: GridOptions;
	filterVal: string;
}
interface Actions {
	getCustomersList(CompanyID: string);
	postNewCustomer(customer: PostNewCustomerInterfaceBody);
	getCheckVATNumber(VATNumber: string);
}
interface Props extends Actions {
	companyID: string;
	customersList: GetCustomersListPayload;
	customersFetchStatus: boolean;
	newCustomerFetchStatus: boolean;
}
