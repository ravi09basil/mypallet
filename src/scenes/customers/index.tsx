import { connect } from 'react-redux';
import { getCustomersList, newCustomer, checkVATNumber } from './actions';
import CustomersTemplate from './comp';
import { PostNewCustomerInterfaceBody } from './types';

const mapDispatchToProps = (dispatch, props) => {
	return ({
		getCustomersList(CompanyID: string) {
			return dispatch(getCustomersList(CompanyID));
		},
		postNewCustomer(customer: PostNewCustomerInterfaceBody) {
			return dispatch(newCustomer(customer));
		},
		getCheckVATNumber(VATNumber: string) {
			return dispatch(checkVATNumber(VATNumber));
		}
	});
};

const mapStateToProps = (state) => {
	return {
		'companyID': state.Login.authentication.companyID,
		'customersList': state.Customers.customersList,
		'customersFetchStatus': state.Customers.customersFetchStatus,
		'newCustomerFetchStatus': state.Customers.newCustomerFetchStatus
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomersTemplate);
