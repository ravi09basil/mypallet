import C from './constants';
import { CustomersSrvc } from './services';
import { PostNewCustomerInterfaceBody } from './types';
import { PostChangeCustomerInterfaceBody } from '../customer/types';

export const getCustomersList = (CompanyID: string) => dispatch => {
	// VARS
	let srvc: CustomersSrvc = new CustomersSrvc();
	// LOADING STATUS
	dispatch({
		type: C.CUSTOMERS_FETCH_DATA_STATUS,
		payload: true
	});
	// SEND
	return srvc.getCustomersList(CompanyID).then(res => {
		// SAVE
		dispatch({
			type: C.CUSTOMERS_FETCH_DATA_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.CUSTOMERS_FETCH_DATA_STATUS,
			payload: false
		});
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.CUSTOMERS_FETCH_DATA_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.CUSTOMERS_FETCH_DATA_STATUS,
			payload: false
		});
	});
};

export const newCustomer = (customer: PostNewCustomerInterfaceBody) => dispatch => {
	// VARS
	let srvc: CustomersSrvc = new CustomersSrvc();
	let googleMaps = new google.maps.Geocoder();
	// LOADING STATUS
	dispatch({
		type: C.NEW_CUSTOMER_FETCH_DATA_STATUS,
		payload: true
	});

	return new Promise((resolve, reject) => {
		// GET MORE DETAILS FROM GOOGLE MAPS
		googleMaps.geocode({ 'address': `${customer.address},${customer.country}` }, function (results: any, status: any) {
			if (status === google.maps.GeocoderStatus.OK) {
				// GET LAT AND LOG FROm GOOGLE MAPS
				var latitude = results[0].geometry.location.lat();
				var longitude = results[0].geometry.location.lng();
				// SAVE
				customer.latitude = latitude;
				customer.longitude = longitude;
				// SEND
				srvc.postNewCustomer(customer).then(res => {
					// SAVE
					dispatch({
						type: C.NEW_CUSTOMER_FETCH_DATA_SUCCESS,
						payload: res
					});
					// LOADING STATUS
					dispatch({
						type: C.NEW_CUSTOMER_FETCH_DATA_STATUS,
						payload: false
					});
					// SUCCESS
					resolve(res);
				}).catch(error => {
					// ERROR
					dispatch({
						type: C.NEW_CUSTOMER_FETCH_DATA_FAILURE,
						payload: error
					});
					// LOADING STATUS
					dispatch({
						type: C.NEW_CUSTOMER_FETCH_DATA_STATUS,
						payload: false
					});
					// ERROR
					reject(error);
				});
			} else {
				// LOADING STATUS
				dispatch({
					type: C.NEW_CUSTOMER_FETCH_DATA_STATUS,
					payload: false
				});
				// ERROR
				reject({
					message: 'The address could not be found.',
					successfull: false
				});
			}
		});
	});
};

export const changeCustomer = (customer: PostChangeCustomerInterfaceBody) => dispatch => {
	// VARS
	let srvc: CustomersSrvc = new CustomersSrvc();
	let googleMaps = new google.maps.Geocoder();
	// LOADING STATUS
	dispatch({
		type: C.NEW_CUSTOMER_FETCH_DATA_STATUS,
		payload: true
	});

	return new Promise((resolve, reject) => {
		// GET MORE DETAILS FROM GOOGLE MAPS
		googleMaps.geocode({ 'address': `${customer.address},${customer.country}` }, function (results: any, status: any) {
			if (status === google.maps.GeocoderStatus.OK) {
				// GET LAT AND LOG FROm GOOGLE MAPS
				var latitude = results[0].geometry.location.lat();
				var longitude = results[0].geometry.location.lng();
				// SAVE
				customer.latitude = latitude;
				customer.longitude = longitude;
				// SEND
				srvc.postChangeCustomer(customer).then(res => {
					// SAVE
					dispatch({
						type: C.NEW_CUSTOMER_FETCH_DATA_SUCCESS,
						payload: res
					});
					// LOADING STATUS
					dispatch({
						type: C.NEW_CUSTOMER_FETCH_DATA_STATUS,
						payload: false
					});
					// SUCCESS
					resolve(res);
				}).catch(error => {
					// ERROR
					dispatch({
						type: C.NEW_CUSTOMER_FETCH_DATA_FAILURE,
						payload: error
					});
					// LOADING STATUS
					dispatch({
						type: C.NEW_CUSTOMER_FETCH_DATA_STATUS,
						payload: false
					});
					// ERROR
					reject(error);
				});
			} else {
				// LOADING STATUS
				dispatch({
					type: C.NEW_CUSTOMER_FETCH_DATA_STATUS,
					payload: false
				});
				// ERROR
				reject({
					message: 'The address could not be found.',
					successfull: false
				});
			}
		});
	});
};

export const checkVATNumber = (VATNumber: string) => dispatch => {
	// VARS
	let srvc: CustomersSrvc = new CustomersSrvc();
	// LOADING STATUS
	dispatch({
		type: C.NEW_CUSTOMER_FETCH_DATA_STATUS,
		payload: true
	});

	// SEND
	return srvc.getCheckVATNumber(VATNumber).then(res => {
		// LOADING STATUS
		dispatch({
			type: C.NEW_CUSTOMER_FETCH_DATA_STATUS,
			payload: false
		});
		return res;
	}).catch(error => {
		// LOADING STATUS
		dispatch({
			type: C.NEW_CUSTOMER_FETCH_DATA_STATUS,
			payload: false
		});
		return error;
	});
};
