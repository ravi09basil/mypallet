import { API } from '../../services/api';
import apisUrls from '../../config/apiUrls';
import { GetCustomersListPayload, PostNewCustomerBody, GetCheckVATNumberPayload } from './types';

export class CustomersSrvc {
	API: API;

	constructor() {
		this.API = new API();
	}

	getCustomersList(CompanyID: string): Promise<GetCustomersListPayload> {
		return this.API.GET(apisUrls.GetCustomers, {
			queryString: { CompanyID }
		});
	}

	postNewCustomer(payload: PostNewCustomerBody) {
		return this.API.POST(apisUrls.PostCreateCustomer, {
			body: JSON.stringify(payload)
		});
	}

	postChangeCustomer(payload: PostNewCustomerBody) {
		return this.API.POST(apisUrls.PostChangeCustomer, {
			body: JSON.stringify(payload)
		});
	}

	getCheckVATNumber(VATNumber: string): Promise<GetCheckVATNumberPayload> {
		return this.API.GET(apisUrls.GetCheckVATNumber, {
			queryString: { VATNumber }
		});
	}
}
