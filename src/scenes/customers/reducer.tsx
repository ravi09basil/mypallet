import { ActionType } from '../../config/types';
import { combineReducers } from 'redux';
import C from './constants';

function customersList(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.CUSTOMERS_FETCH_DATA_SUCCESS:
			return action.payload;
		case C.CUSTOMERS_FETCH_DATA_FAILURE:
			return null;
		default:
			return state;
	}
}

function customersFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.CUSTOMERS_FETCH_DATA_STATUS:
			return action.payload;
		default:
			return state;
	}
}

function newCustomer(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.NEW_CUSTOMER_FETCH_DATA_SUCCESS:
			return action.payload;
		case C.NEW_CUSTOMER_FETCH_DATA_FAILURE:
			return null;
		default:
			return state;
	}
}

function newCustomerFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.NEW_CUSTOMER_FETCH_DATA_STATUS:
			return action.payload;
		default:
			return state;
	}
}

const Customers = {
	customersList,
	customersFetchStatus,
	newCustomer,
	newCustomerFetchStatus
};

export default combineReducers(Customers);
