import { ActionType } from '../../config/types';
import { combineReducers } from 'redux';
import C from './constants';

function peopleList(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.PEOPLE_FETCH_DATA_SUCCESS:
			return action.payload;
		case C.PEOPLE_FETCH_DATA_FAILURE:
			return null;
		default:
			return state;
	}
}

function peopleFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.PEOPLE_FETCH_DATA_STATUS:
			return action.payload;
		default:
			return state;
	}
}

function newUser(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.NEW_USER_FETCH_DATA_SUCCESS:
			return action.payload;
		case C.NEW_USER_FETCH_DATA_FAILURE:
			return null;
		default:
			return state;
	}
}

function newUserFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.NEW_USER_FETCH_DATA_STATUS:
			return action.payload;
		default:
			return state;
	}
}

function removeUser(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.REMOVE_USER_FETCH_DATA_SUCCESS:
			return action.payload;
		case C.REMOVE_USER_FETCH_DATA_FAILURE:
			return null;
		default:
			return state;
	}
}

function removeUserFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.REMOVE_USER_FETCH_DATA_STATUS:
			return action.payload;
		default:
			return state;
	}
}

const People = {
	peopleList,
	peopleFetchStatus,
	newUser,
	newUserFetchStatus,
	removeUser,
	removeUserFetchStatus
};

export default combineReducers(People);
