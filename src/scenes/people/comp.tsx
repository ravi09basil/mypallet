import * as React from 'react';
import { GridOptions, ICellRendererParams } from 'ag-grid';
import { InjectedIntlProps, FormattedMessage, injectIntl } from 'react-intl';
import Loader from '../../components/loader';
import NewUser from '../../components/newUserModal';
import GridTools from '../../components/gridTools';
import AgGridTheme from '../../components/agGridTheme';
import ConfirmationModal from '../../components/confirmationModal';
import { bigBox } from '../../components/smartadmin/utils/actions/MessageActions';
import { GetPeopleListPayload, PostNewUserBody, AppUser } from './types';
import './style.scss';

class PeopleTemplate extends React.Component<Props & InjectedIntlProps> {
	newUserModalID = 'newUserModalID';
	RemoveUserModalID = 'RemoveUserModalID';
	state: State = {
		userToDelete: null,
		gridOptions: {
			rowHeight: 55,
			enableSorting: true,
			onGridReady: () => {
				if (this.state.gridOptions.columnApi) {
					setTimeout(() => { if (this.state.gridOptions.api) { this.state.gridOptions.api.resetRowHeights(); if (this.state.gridOptions.columnApi) {this.state.gridOptions.columnApi.autoSizeAllColumns(); } if (this.state.gridOptions.api) {this.state.gridOptions.api.sizeColumnsToFit(); } } }, 500);
					}
				},
			columnDefs: [
				{
					headerName: this.props.intl.formatMessage({ id: 'general.name' }),
					field: 'name',
					autoHeight: 80,
					cellRenderer: (params: ICellRendererParams) => {
						let value = params.data.isAwaiting;
						let p = document.createElement('p');
						let divWrap = document.createElement('div');
						let text = value ? this.props.intl.formatMessage({ 'id': 'general.pending' }) : this.props.intl.formatMessage({ 'id': 'general.active' });
						p.innerHTML = `<p><img class="user-image" src="${params.data.imagePath}" alt=''/> ${params.data.name} <br/> <label class="label ${params.data.isAwaiting ? 'label-warning' : 'label-success'}">${text}</label></p>`;
						divWrap.appendChild(p);
						divWrap.className = 'text-left';
						return divWrap;
					}
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.phoneNumber' }),
					field: 'phonenumber'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.form.userType' }),
					field: 'userType',
					cellRenderer: (params: ICellRendererParams) => {
						switch (params.value) {
							case 1:
							return `<span>${this.props.intl.formatMessage({id: 'general.form.userTypeOptions.warehouse'})}</span>`;
							case 2:
								return '<span>Shipper</span>';
							default:
								return `<span>${this.props.intl.formatMessage({id: 'general.form.userTypeOptions.driver'})}</span>`;
						}
					}
				},
				// {
				// 	autoHeight: 1,
				// 	headerName: '',
				// 	field: 'isAwaiting',
				// 	width: 40,
				// 	cellRenderer: (params: ICellRendererParams) => {
				// 		let value = params.value;
				// 		let p = document.createElement('p');
				// 		let divWrap = document.createElement('div');
				// 		let text = value ? this.props.intl.formatMessage({ 'id': 'general.pending' }) : this.props.intl.formatMessage({ 'id': 'general.active' });
				// 		p.innerHTML = `<p>${params.data.name}</p><p class="label ${value ? 'label-warning' : 'label-success'}">${text}</p>`;
				// 		divWrap.appendChild(p);
				// 		divWrap.className = 'text-center';
				// 		return divWrap;
				// 	}
				// },
				// {
				// 	autoHeight: 1,
				// 	headerName: '',
				// 	field: 'imagePath',
				// 	cellRenderer: (params: ICellRendererParams) => {
				// 		return (params.value) ? `<div class="text-center">
				// 			<img class="user-image" src="${params.value}" />
				// 			</div>` : '<div class="text-center"><i class="fa fa-camera fa-3x"></i></div>';
				// 	}
				// },
				{
					autoHeight: 1,
					headerName: this.props.intl.formatMessage({ id: 'general.functions' }),
					field: 'actions',
					cellRenderer: (params: ICellRendererParams) => {
						let divWrap = document.createElement('div');
						let remove = document.createElement('a');
						remove.innerHTML = `<a class="btn btn-danger btn-s ml-10 fa fa-minus-square"></a>`;
						// remove.innerHTML = `<a class="btn btn-danger btn-s ml-10">${this.props.intl.formatMessage({ id: 'general.delete' })}</a>`;
						remove.addEventListener('click', () => this.OpenRemoveUserModal(params.data));
						divWrap.appendChild(remove);
						divWrap.className = 'actions';
						return divWrap;
					}
				}
			]
		}
	};

	constructor(props: Props & InjectedIntlProps) {
		super(props);
		this.createNewUser = this.createNewUser.bind(this);
		this.removeUser = this.removeUser.bind(this);
	}

	componentDidMount() {
		this.props.getPeopleList(this.props.companyID);
	}

	OpenRemoveUserModal(user: AppUser) {
		// SAVE THE USER
		this.setState({
			userToDelete: user
		});
		// OPEN THE MODAL
		$(`#${this.RemoveUserModalID}`).modal('show');
	}

	removeUser() {
		if (this.state.userToDelete) {
			// REMOVE USER
			this.props.postRemoveAppUser(this.props.companyID, this.state.userToDelete.id).then(() => {
				// UPDATE THE LIST
				this.props.getPeopleList(this.props.companyID);
			}).then(() => {
				// HIDE THE MODAL
				$(`#${this.RemoveUserModalID}`).modal('hide');
			});
		}
	}

	createNewUser(user: PostNewUserBody) {
		// ADD THE COMPANY ID TO THE PAYLOAD
		user.companyID = this.props.companyID;
		// SEND THE DATA
		return this.props.postNewUser(user).then(() => {
			// NOTE
			bigBox({
				title: this.props.intl.formatMessage({ id: 'scenes.people.createNewUser.title' }),
				content: this.props.intl.formatMessage({ id: 'scenes.people.createNewUser.content' }),
				color: '#739E73',
				timeout: 15000,
				icon: 'fa fa-check'
			});
			// UPDATE CUSTOMER LIST
			this.props.getPeopleList(this.props.companyID);
		});
	}

	render() {
		let rowData = (this.props.peopleList && this.props.peopleList.appUsers) ? this.props.peopleList.appUsers : [];
		let userToDelete = (this.state.userToDelete) ? this.state.userToDelete.name : '';
		return (
			<div className="page-layout people-page">
				<div className="container-fluid">
					<div className="row mb-15">
						<div className="col-sm-12 text-right">
							<GridTools modalID={this.newUserModalID} gridOptions={this.state.gridOptions} />
						</div>
					</div>
					<div className="row mb-15">
						<div className="col-sm-12">
							<div className="grid-st1 ag-theme-balham">
								<AgGridTheme rowData={rowData} gridOptions={this.state.gridOptions} />
							</div>
						</div>
					</div>
				</div>
				<ConfirmationModal
					modalID={this.RemoveUserModalID}
					title={this.props.intl.formatMessage({ id: 'scenes.people.deleteUserModal.title' })}
					cancelTxt={this.props.intl.formatMessage({ id: 'general.cancel' })}
					confirmTxt={this.props.intl.formatMessage({ id: 'general.ok' })}
					loaderStatus={this.props.removeUserFetchStatus}
					confirmEvent={this.removeUser}
				>
					<h1><FormattedMessage id="general.deleteConfirmation" values={{ name: userToDelete }} /></h1>
				</ConfirmationModal>
				<NewUser modeID={this.newUserModalID} handleSubmit={this.createNewUser} loaderStatus={this.props.newUserFetchStatus} />
				<Loader status={this.props.peopleFetchStatus} />
			</div>
		);
	}
}

export default injectIntl(PeopleTemplate);

interface State {
	gridOptions: GridOptions;
	userToDelete: AppUser | null;
}
interface Actions {
	getPeopleList(CompanyID: string);
	postNewUser(user: PostNewUserBody);
	postRemoveAppUser(CompanyID: string, SelectedUserID: string);
}
interface Props extends Actions {
	companyID: string;
	peopleList: GetPeopleListPayload;
	peopleFetchStatus: boolean;
	newUserFetchStatus: boolean;
	removeUserFetchStatus: boolean;
}
