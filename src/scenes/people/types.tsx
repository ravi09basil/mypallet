export interface AppUser {
	id: string;
	name: string;
	phoneNumber: string;
	isAwaiting: boolean;
	userType: string;
	imagePath: string;
}

export interface GetPeopleListPayload {
	appUsers: AppUser[];
	successfull: boolean;
	message: string;
}

export interface PostNewUserBody {
	name: string;
	phoneNumber: string;
	userType: string;
	companyID: string;
}

export interface PostNewUserPayload {
	alreadyExsistingUser: boolean;
	name: string;
	imagePath: string;
	successfull: boolean;
	message: string;
}

export interface PostRemoveAppUserPayload {
	successfull: true;
	message: string;
}
