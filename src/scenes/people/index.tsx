import { connect } from 'react-redux';
import { getPeopleList, newUser, removeAppUser } from './actions';
import PeopleTemplate from './comp';

const mapDispatchToProps = (dispatch, props) => {
	return ({
		getPeopleList(CompanyID: string) {
			return dispatch(getPeopleList(CompanyID));
		},
		postNewUser: (user) => {
			return dispatch(newUser(user));
		},
		postRemoveAppUser(CompanyID: string, AppUserID: string) {
			return dispatch(removeAppUser(CompanyID, AppUserID));
		}
	});
};

const mapStateToProps = (state) => {
	return {
		'companyID': state.Login.authentication.companyID,
		'peopleList': state.People.peopleList,
		'peopleFetchStatus': state.People.peopleFetchStatus,
		'newUserFetchStatus': state.People.newUserFetchStatus,
		'removeUserFetchStatus': state.People.removeUserFetchStatus
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(PeopleTemplate);
