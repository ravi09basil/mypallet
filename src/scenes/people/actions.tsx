import C from './constants';
import { PeopleSrvc } from './services';
import { PostNewUserBody } from './types';

export const removeAppUser = (CompanyID: string, AppUserID: string) => dispatch => {
	// VARS
	let srvc: PeopleSrvc = new PeopleSrvc();
	// LOADING STATUS
	dispatch({
		type: C.REMOVE_USER_FETCH_DATA_STATUS,
		payload: true
	});
	// SEND
	return srvc.postRemoveAppUser(CompanyID, AppUserID).then(res => {
		// SAVE DATA
		dispatch({
			type: C.REMOVE_USER_FETCH_DATA_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.REMOVE_USER_FETCH_DATA_STATUS,
			payload: false
		});
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.REMOVE_USER_FETCH_DATA_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.REMOVE_USER_FETCH_DATA_STATUS,
			payload: false
		});
	});
};

export const getPeopleList = (CompanyID: string) => dispatch => {
	// VARS
	let srvc: PeopleSrvc = new PeopleSrvc();
	// LOADING STATUS
	dispatch({
		type: C.PEOPLE_FETCH_DATA_STATUS,
		payload: true
	});
	// SEND
	return srvc.getPeopleList(CompanyID).then(res => {
		// SAVE DATA
		dispatch({
			type: C.PEOPLE_FETCH_DATA_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.PEOPLE_FETCH_DATA_STATUS,
			payload: false
		});
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.PEOPLE_FETCH_DATA_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.PEOPLE_FETCH_DATA_STATUS,
			payload: false
		});
	});
};

export const newUser = (user: PostNewUserBody) => dispatch => {
	// VARS
	let srvc: PeopleSrvc = new PeopleSrvc();
	// REMOVE SPACES
	user.phoneNumber = user.phoneNumber.replace(/\s+/g, '');
	// LOADING STATUS
	dispatch({
		type: C.NEW_USER_FETCH_DATA_STATUS,
		payload: true
	});
	// SEND
	return srvc.postNewUser(user).then(res => {
		// SAVE
		dispatch({
			type: C.NEW_USER_FETCH_DATA_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.NEW_USER_FETCH_DATA_STATUS,
			payload: false
		});
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.NEW_USER_FETCH_DATA_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.NEW_USER_FETCH_DATA_STATUS,
			payload: false
		});
	});
};
