import { API } from '../../services/api';
import apisUrls from '../../config/apiUrls';
import { GetPeopleListPayload, PostNewUserBody, PostNewUserPayload, PostRemoveAppUserPayload } from './types';

export class PeopleSrvc {
	API: API;

	constructor() {
		this.API = new API();
	}

	getPeopleList(CompanyID: string): Promise<GetPeopleListPayload> {
		return this.API.GET(apisUrls.GetAppUsers, {
			queryString: { CompanyID }
		});
	}

	postNewUser(payload: PostNewUserBody): Promise<PostNewUserPayload> {
		return this.API.POST(apisUrls.PostCreateAppUser, {
			body: JSON.stringify(payload)
		});
	}

	postRemoveAppUser(companyID: string, appUserID: string): Promise<PostRemoveAppUserPayload> {
		return this.API.POST(apisUrls.PostRemoveAppUser, {
			body: JSON.stringify({
				companyID,
				appUserID
			})
		});
	}
}
