import { connect } from 'react-redux';
import { getCustomer } from './actions';
import { checkVATNumber, changeCustomer } from '../customers/actions';
import { PostChangeCustomerInterfaceBody } from '../customer/types';

import CustomersTemplate from './comp';
import { SendReceiptPayload } from '../receipts/types';
import { sendReceipt } from '../receipts/actions';

const mapDispatchToProps = (dispatch, props) => {
	return ({
		getCustomerInfo(customerID: string) {
			return dispatch(getCustomer(customerID));
		},
		getCheckVATNumber(VATNumber: string) {
			return dispatch(checkVATNumber(VATNumber));
		},
		changeNewCustomer(customer: PostChangeCustomerInterfaceBody) {
			return dispatch(changeCustomer(customer));
		},
		sendReceipt(SendReceipt: SendReceiptPayload) {
			return sendReceipt(SendReceipt);
		}
	});
};

const mapStateToProps = (state, props) => {
	return {
		'customer': state.Customer.customer,
		'customerFetchStatus': state.Customer.customerFetchStatus
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomersTemplate);
