import * as React from 'react';
import { connect } from 'react-redux';
import { InjectedIntlProps, FormattedMessage, injectIntl } from 'react-intl';
import { updatePalletBankStatus } from '../actions';
import { PostUpdatePalletBankStatus, CustomerInfoPayload, PostChangeCustomerInterfaceBody } from '../types';
import './styles.scss';
import { bigBox } from '../../../components/smartadmin/utils/actions/MessageActions';
import ChangeCustomer from '../../../components/changeCustomerModal';

class Tab4 extends React.Component<Props & InjectedIntlProps, State> {
	changeCustomerModalID = 'changeCustomerModalID';
	state: State = {
		currentChecBoxState: false,
	};

	constructor(props: Props & InjectedIntlProps) {
		super(props);
		this.state = { currentChecBoxState: this.props.data.hasPalletBank };
		this.changeCustomer = this.changeCustomer.bind(this);
	}

	changeCustomer(customer: any) {
		// VARS
		let address = customer.address;
		let zipCode;
		let city;
		let country;
		let lat: any = address.geometry.location.lat;
		let lng: any = address.geometry.location.lng;
		// LOOP
		address.address_components.forEach((i) => {
			if (i.types.indexOf('postal_code') !== -1) {
				zipCode = i.long_name;
			} else if (i.types.indexOf('administrative_area_level_1') !== -1) {
				city = i;
			} else if (i.types.indexOf('country') !== -1) {
				country = i.long_name;
			}
		});
		// CHANGE VALUES
		let newCustomer: PostChangeCustomerInterfaceBody = {
			customerID: this.props.data.id ? this.props.data.id : '',
			companyID: this.props.companyID ? this.props.companyID : '',
			name: customer.name,
			postCode: zipCode,
			city: city ? city.long_name : '',
			country: country ? country : '',
			phoneNumber: customer.phoneNumber,
			vatNumber: customer.vatNumber,
			address: address.name,
			latitude: lat,
			longitude: lng
		};
		// POST
		return this.props.changeNewCustomer(newCustomer).then(res => {
			if (res.successfull) {
				// NOTE
				bigBox({
					title: this.props.intl.formatMessage({ id: 'scenes.customers.newCustomer.title' }),
					content: this.props.intl.formatMessage({ id: 'scenes.customers.newCustomer.content' }),
					color: '#739E73',
					timeout: 1000,
					icon: 'fa fa-check'
				});
			}
		});
	}

	handleCheck(event: React.ChangeEvent<HTMLInputElement>) {
		this.setState({ currentChecBoxState: event.target.checked });
		let newStateUpdate: PostUpdatePalletBankStatus = {
			newState: event.target.checked,
			customerID: this.props.data.id ? this.props.data.id : '',
		};
		this.props.updatePalletBankStatus(newStateUpdate);
		bigBox({
			title: this.props.intl.formatMessage({ id: 'scenes.customer.customerSettingsTab.updatedPalletBankSuccessfullTitle' }),
			content: this.props.intl.formatMessage({ id: 'scenes.customer.customerSettingsTab.updatedPalletBankSuccessfullMessage' }),
			color: '#739E73',
			timeout: 1000,
			icon: 'fa fa-check'
		});
	}

	render() {
		return (
			<div className="page-layout customers-page">
				<div className="jarviswidget">
					<header>
						<h2 className="title"><FormattedMessage id="general.customersettings" /></h2>
					</header>
					<div>
						<div className="widget-body no-padding">
							<form className="smart-form">
								<fieldset>
									<section>
										<div className="row">
											<div className="col col-4">
												<div className="form-group input input-block">
													<header>
														<i className="componentIcon fa fa-university mr-10" />
														<FormattedMessage id="general.palletBank" />
													</header>
													<div className="ml-10 mt-5">
														<label className="checkbox">
															<input type="checkbox" checked={this.state.currentChecBoxState} onChange={e => this.handleCheck(e)} />
															<i />
															<FormattedMessage id="scenes.customer.customerSettingsTab.userHasPalletBank" />
														</label>
														<label>Her kan der stå en beskrivelse af hvad denne funktion kan</label>
													</div>
													<header>
														<i className="componentIcon fa fa-exchange mr-10" />
														<FormattedMessage id="general.customerMigrationTitle" />
													</header>
													<div className="ml-10 mt-5">
														<a className="mb-5 mt-5 btn btn-primary btn-sm" data-toggle="modal" data-target={`#${this.changeCustomerModalID}`}><FormattedMessage id="general.customerMigrationTitle" /></a>
														<br />
														<label>Her kan der stå en beskrivelse af hvad denne funktion kan</label>
													</div>
												</div>
											</div>
											<div className="col col-8">
												<div className="form-group input input-block">
													<div />
												</div>
											</div>
										</div>
									</section>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
				<ChangeCustomer modalID={this.changeCustomerModalID} handleSubmit={this.changeCustomer} loaderStatus={this.props.newCustomerFetchStatus} vatNumberCheck={this.props.getCheckVATNumber} />
			</div>
		);
	}
}

const mapDispatchToProps = (dispatch) => {
	return ({
		updatePalletBankStatus(payload: PostUpdatePalletBankStatus) {
			return dispatch(updatePalletBankStatus(payload));
		},
	});
};

const mapStateToProps = (state) => {
	return {
		'Language': state.Language.currentLanguage,
		'companyID': state.Login.authentication.companyID,
		'transactionInfo': state.Customer.transactionInfo,
		'transactionInfoFetchStatus': state.Customer.transactionInfoFetchStatus,
		'companyTransactionsHistory': state.Customer.companyTransactionsHistory,
		'companyTransactionsHistoryFetchStatus': state.Customer.companyTransactionsHistoryFetchStatus
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Tab4));

interface Actions {
	changeNewCustomer(customer: PostChangeCustomerInterfaceBody);
	updatePalletBankStatus(payload: PostUpdatePalletBankStatus);
	getCheckVATNumber(VATNumber: string);
}
interface State {
	currentChecBoxState: boolean;
}
interface Props extends Actions {
	data: CustomerInfoPayload;
	customerInfoFetchStatus: boolean;
	newCustomerFetchStatus: boolean;
	companyID: string;
}
