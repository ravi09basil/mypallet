import * as React from 'react';
import { connect } from 'react-redux';
import { GridOptions, ICellRendererParams } from 'ag-grid';
import { InjectedIntlProps, FormattedMessage, injectIntl } from 'react-intl';
import Loader from '../../../components/loader';
import Select2 from '../../../components/smartadmin/forms/inputs/Select2';
import AgGridTheme from '../../../components/agGridTheme';
import { getAggregatedStock } from '../actions';
import { getLocationsList } from '../../stock/actions';
import { GetAggregatedStockPayload } from '../types';
import { GetLocationsPayload } from '../../stock/types';

class Tab1 extends React.Component<Props & InjectedIntlProps, State> {
	state: State = {
		gridOptions: {
			rowHeight: 40,
			enableFilter: true,
			enableSorting: true,
			groupSuppressRow: true,
			animateRows: true,
			groupUseEntireRow: true,
			onGridReady: () => {
				if (this.state.gridOptions.columnApi) {
					setTimeout(() => { if (this.state.gridOptions.api) { this.state.gridOptions.api.resetRowHeights(); if (this.state.gridOptions.columnApi) { this.state.gridOptions.columnApi.autoSizeAllColumns(); } if (this.state.gridOptions.api) { this.state.gridOptions.api.sizeColumnsToFit(); } } }, 500);
				}
			},
			columnDefs: [
				{
					headerName: this.props.intl.formatMessage({ id: 'general.name' }),
					field: 'name'
				},
				{
					headerName: `${this.props.intl.formatMessage({ id: 'general.received' })} / ${this.props.intl.formatMessage({ id: 'general.delivered' })}`,
					field: 'receAndDeli',
					cellRenderer: (params: ICellRendererParams) => {
						let received = params.data.received;
						let delivered = params.data.delivered;
						return `${received} / <span class="color-red">${delivered}</span>`;
					}
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.total' }),
					field: 'total',
					cellRenderer: (params: ICellRendererParams) => {
						let value = params.value;
						return (value < 0) ? `<span class="color-red">${value}</span>` : `${value}`;
					}
				},
				/*{
					headerName: '',
					field: 'actions',
					cellRenderer: (params: ICellRendererParams) => {
						var div = document.createElement('div');
						div.className = 'text-center';
						var link = document.createElement('a');
						link.className = 'btn btn-success btn-s';
						link.innerText = 'Reset';
						link.addEventListener('click', (e) => {
							console.log(params.data);
							e.preventDefault();
						});
						div.appendChild(link);
						return div;
					}
				}*/
			]
		}
	};

	constructor(props: Props & InjectedIntlProps) {
		super(props);
		this.locationChanged = this.locationChanged.bind(this);
	}

	componentDidMount() {
		// CUSTOMER ID
		// let CompanyID = this.props.customerID;
		// GET
		this.props.getAggregatedStock(this.props.customerID);
		this.props.getLocationsList(this.props.companyID);
	}

	locationChanged(v: any) {
		// VARS
		let locationID = v.target.value;
		// GET CUSTOMER AGGREGATED STOCK
		this.props.getAggregatedStock(this.props.customerID, true, locationID);
	}

	render() {
		// VARS
		let rowData = (this.props.aggregatedStock && this.props.aggregatedStock.stocks) ? this.props.aggregatedStock.stocks : [];
		let locations = (this.props.locationsList && this.props.locationsList.locations) ? this.props.locationsList.locations : false;
		return (
			<div className="tab1 relative">
				{
					this.props.locationsList && this.props.locationsList.locations.length > 1 ?
						<div className="row mb-30">
							<div className="col-sm-3">
								<h1><FormattedMessage id="general.locationFilter" /></h1>
								<Select2 options={{ width: '100%' }} className="select2" onSelect={this.locationChanged}>
									<option value="0">{this.props.intl.formatMessage({ id: 'general.allLocations' })}</option>
									{(locations ? locations.map((i) => <option value={i.id} key={i.id}>{i.name}</option>) : '')}
								</Select2>
							</div>
						</div> : ''
				}
				<div className="grid-st1 ag-theme-balham">
					<AgGridTheme rowData={rowData} gridOptions={this.state.gridOptions} />
				</div>
				<Loader status={this.props.aggregatedStockFetchStatus} />
			</div>
		);
	}
}

const mapDispatchToProps = (dispatch) => {
	return ({
		getLocationsList(CompanyID: string) {
			return dispatch(getLocationsList(CompanyID));
		},
		getAggregatedStock(CompanyID: string, LocationFiltered?: boolean, LocationID?: string) {
			return dispatch(getAggregatedStock(CompanyID, LocationFiltered, LocationID));
		}
	});
};

const mapStateToProps = (state) => {
	return {
		'Language': state.Language.currentLanguage,
		'companyID': state.Login.authentication.companyID,
		'aggregatedStock': state.Customer.aggregatedStock,
		'aggregatedStockFetchStatus': state.Customer.aggregatedStockFetchStatus,
		'locationsList': state.Stock.locationsList,
		'locationsListFetchStatus': state.Stock.locationsListFetchStatus
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Tab1));

interface State {
	gridOptions: GridOptions;
}
interface Actions {
	getLocationsList(CompanyID: string);
	getAggregatedStock(CompanyID: string, LocationFiltered?: boolean, LocationID?: string);
}
interface Props extends Actions {
	Language: string;
	customerID: string;
	companyID: string;
	aggregatedStock: GetAggregatedStockPayload;
	aggregatedStockFetchStatus: boolean;
	locationsList: GetLocationsPayload;
	locationsListFetchStatus: boolean;
}
