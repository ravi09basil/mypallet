import * as React from 'react';
import { connect } from 'react-redux';
import moment from 'moment-timezone';
import { GridOptions, ICellRendererParams } from 'ag-grid';
import { InjectedIntlProps, injectIntl } from 'react-intl';
import Loader from '../../../components/loader';
import AgGridTheme from '../../../components/agGridTheme';
import TransactionInfoModal from '../../../components/transactionInfoModal';
import { getTransactionInfo, getCompanyTransactionsHistory } from '../actions';
import { CompanyTransactionHistoriesPayload, GetTransactionInfoPayload } from '../types';
import { EnterEmail } from '../../../components/enterEmailModal';
import { SendReceiptPayload } from '../../receipts/types';

class Tab2 extends React.Component<Props & InjectedIntlProps, State> {
	EnterEmailModalID = 'EnterEmailModal';
	TransactionInfoModalID = 'TransactionInfoModal';
	state: State = {
		transactionInfo: undefined,
		transactionID: undefined,
		gridOptions: {
			rowHeight: 40,
			enableSorting: true,
			enableFilter: true,
			onGridReady: () => {
				if (this.state.gridOptions.api) {
					this.state.gridOptions.api.sizeColumnsToFit();
				}
			},
			columnDefs: [
				{
					headerName: this.props.intl.formatMessage({ id: 'general.info' }),
					field: 'info',
					cellRenderer: (params: ICellRendererParams) => {
						// VARS
						let link = document.createElement('a');
						let divHolder = document.createElement('div');
						let transactionID = params.data.transactionID;
						// LINK
						link.className = 'fa fa-lg fa-info-circle color-black';
						link.onclick = (e) => {
							// OPEN THE MODAL
							this.openInfoModal(transactionID);
							// DISABLE THE DEFAULT ACTION
							e.preventDefault();
						};
						// HOLDER
						divHolder.className = 'text-center';
						divHolder.appendChild(link);
						return divHolder;
					}
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.transactionID' }),
					field: 'transactionID'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.company' }),
					field: 'receivingCompanyName'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.land' }),
					field: 'receivingCompanyCountry'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.receivingUserName' }),
					field: 'receivingUserName'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.time' }),
					field: 'confirmedAt',
					cellRenderer: (params: ICellRendererParams) => {
						return moment(params.value).format('HH:mm:ss');
					}
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.date' }),
					field: 'confirmedAt',
					cellRenderer: (params: ICellRendererParams) => {
						return moment(params.value).format('L');
					}
				}
				,
				{
					headerName: '',
					field: 'actions',
					cellRenderer: (params: ICellRendererParams) => {
						// VARS
						let divHolder = document.createElement('div');
						let transactionID = params.data.transactionID;
						let sendAsMail = document.createElement('a');
						// LINK
						sendAsMail.className = 'fa fa-lg fa-envelope btn btn-primary btn-s';
						sendAsMail.onclick = (e) => {
							// OPEN THE MODAL
							this.setState({ transactionID: transactionID });
							this.openEnterEmailModal(transactionID);
							// DISABLE THE DEFAULT ACTION
							e.preventDefault();
						};
						divHolder.className = 'text-center';
						divHolder.appendChild(sendAsMail);
						return divHolder;
					}
				}
			]
		}
	};
	constructor(props: Props & InjectedIntlProps) {
		super(props);
		this.sendReceipt = this.sendReceipt.bind(this);
	}

	openEnterEmailModal(transactionID: string) {
		$(`#${this.EnterEmailModalID}`).modal('show');
		this.state.transactionID = transactionID;
	}
	sendReceipt(SendReceipt: SendReceiptPayload) {
		let payload: SendReceiptPayload = {
			transactionID: SendReceipt.transactionID,
			email: SendReceipt.email,
		};
		console.log(payload);

		this.props.sendReceipt(payload);
	}
	componentDidMount() {
		// GET CUSTOMER AGGREGATED STOCK
		this.props.getCompanyTransactionsHistory(this.props.customerID, this.props.companyID);
	}

	openInfoModal(transactionID: string) {
		// OPEN THE MODAL
		$(`#${this.TransactionInfoModalID}`).modal('show');
		// GET TRANSATION INFO
		this.props.getTransactionInfo(transactionID).then(res => {
			this.setState({
				transactionInfo: this.props.transactionInfo
			});
		});
	}

	render() {
		// VARS
		let rowData = (this.props.companyTransactionsHistory && this.props.companyTransactionsHistory.companyTransactionHistories) ? this.props.companyTransactionsHistory.companyTransactionHistories : [];
		return (
			<div className="tab2 relative">
				<div className="grid-st1 ag-theme-balham">
					<AgGridTheme rowData={rowData} gridOptions={this.state.gridOptions} />
				</div>
				<EnterEmail modalID={this.EnterEmailModalID} handleSubmit={this.sendReceipt} transactionID={this.state.transactionID} />
				<TransactionInfoModal modalID={this.TransactionInfoModalID} loaderStatus={this.props.transactionInfoFetchStatus} data={this.props.transactionInfo} />
				<Loader status={this.props.companyTransactionsHistoryFetchStatus} />
			</div>
		);
	}
}

const mapDispatchToProps = (dispatch) => {
	return ({
		getTransactionInfo(TransactionID: string) {
			return dispatch(getTransactionInfo(TransactionID));
		},
		getCompanyTransactionsHistory(SelectedCompanyID: string, CompanyID: string) {
			return dispatch(getCompanyTransactionsHistory(SelectedCompanyID, CompanyID));
		}
	});
};

const mapStateToProps = (state) => {
	return {
		'Language': state.Language.currentLanguage,
		'companyID': state.Login.authentication.companyID,
		'transactionInfo': state.Customer.transactionInfo,
		'transactionInfoFetchStatus': state.Customer.transactionInfoFetchStatus,
		'companyTransactionsHistory': state.Customer.companyTransactionsHistory,
		'companyTransactionsHistoryFetchStatus': state.Customer.companyTransactionsHistoryFetchStatus
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Tab2));

interface State {
	gridOptions: GridOptions;
	transactionInfo: undefined | GetTransactionInfoPayload;
	transactionID: string | undefined;
}
interface Actions {
	getTransactionInfo(TransactionID: string);
	getCompanyTransactionsHistory(SelectedCompanyID: string, CompanyID: string);
	sendReceipt(SendReceipt: SendReceiptPayload);
}
interface Props extends Actions {
	Language: string;
	customerID: string;
	companyID: string;
	transactionInfo: GetTransactionInfoPayload;
	transactionInfoFetchStatus: boolean;
	companyTransactionsHistory: CompanyTransactionHistoriesPayload;
	companyTransactionsHistoryFetchStatus: boolean;
}
