import * as React from 'react';
import { GridOptions, ICellRendererParams } from 'ag-grid';
import { InjectedIntlProps, FormattedMessage, injectIntl } from 'react-intl';
import AgGridTheme from '../../../components/agGridTheme';
import UiDatepicker from '../../../components/smartadmin/forms/inputs/UiDatepicker';

class Tab2 extends React.Component<Props & InjectedIntlProps, State> {
	state: State = {
		gridOptions: {
			rowHeight: 40,
			enableSorting: true,
			onGridReady: () => {
				if (this.state.gridOptions.columnApi) {
					setTimeout(() => { if (this.state.gridOptions.api) { this.state.gridOptions.api.resetRowHeights(); if (this.state.gridOptions.columnApi) { this.state.gridOptions.columnApi.autoSizeAllColumns(); } if (this.state.gridOptions.api) { this.state.gridOptions.api.sizeColumnsToFit(); } } }, 500);
				}
			},
			columnDefs: [
				{
					headerName: this.props.intl.formatMessage({ id: 'general.info' }),
					field: 'info',
					cellRenderer: (params: ICellRendererParams) => {
						return `<div className="text-center"><span className="fa fa-info-circle fa-2x"></span></div>`;
					}
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.palletypes' }),
					field: 'palletyper',
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.typeA' }),
					field: 'typeA'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.typeB' }),
					field: 'typeB'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.typeC' }),
					field: 'typeC'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.total' }),
					field: 'ialt'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.selectAll' }),
					field: 'valgalle',
					checkboxSelection: true,
					headerCheckboxSelection: true
				}
			]
		}
	};

	render() {
		let rowData = [];
		return (
			<div className="tab1">
				<div className="form-inline mb-15">
					<div className="form-group mr-10">
						<label><FormattedMessage id="general.selectPeriod" />:</label>
					</div>
					<div className="form-group mr-10">
						<UiDatepicker name="mydate" placeholder={this.props.intl.formatMessage({ id: 'general.selectADate' })} className="form-control datepicker" data-date-format="dd/mm/yy" />
					</div>
					<div className="form-group mr-10">
						<UiDatepicker name="mydate" placeholder={this.props.intl.formatMessage({ id: 'general.selectADate' })} className="form-control datepicker" data-date-format="dd/mm/yy" />
					</div>
				</div>
				<div className="grid-st1 ag-theme-balham">
					<AgGridTheme rowData={rowData} gridOptions={this.state.gridOptions} />
				</div>
			</div>
		);
	}
}

export default injectIntl(Tab2);

interface State {
	gridOptions: GridOptions;
}
interface Props {

}
