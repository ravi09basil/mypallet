import { ActionType } from '../../config/types';
import { combineReducers } from 'redux';
import C from './constants';

function customer(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.CUSTOMER_INFO_FETCH_DATA_SUCCESS:
			return action.payload;
		case C.CUSTOMER_INFO_FETCH_DATA_FAILURE:
			return null;
		default:
			return state;
	}
}

function customerFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.CUSTOMER_INFO_FETCH_DATA_STATUS:
			return action.payload;
		default:
			return state;
	}
}

function aggregatedStock(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.CUSTOMER_AGGREGATED_STOCK_FETCH_DATA_SUCCESS:
			return action.payload;
		case C.CUSTOMER_AGGREGATED_STOCK_FETCH_DATA_FAILURE:
			return null;
		default:
			return state;
	}
}

function aggregatedStockFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.CUSTOMER_AGGREGATED_STOCK_FETCH_DATA_STATUS:
			return action.payload;
		default:
			return state;
	}
}

function companyTransactionsHistory(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.CUSTOMER_TRANSACTIONS_HISTORY_FETCH_DATA_SUCCESS:
			return action.payload;
		case C.CUSTOMER_TRANSACTIONS_HISTORY_FETCH_DATA_FAILURE:
			return null;
		default:
			return state;
	}
}

function companyTransactionsHistoryFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.CUSTOMER_TRANSACTIONS_HISTORY_FETCH_DATA_STATUS:
			return action.payload;
		default:
			return state;
	}
}

function transactionInfo(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.CUSTOMER_TRANSACTION_INFO_FETCH_DATA_SUCCESS:
			return action.payload;
		case C.CUSTOMER_TRANSACTION_INFO_FETCH_DATA_FAILURE:
			return null;
		default:
			return state;
	}
}

function transactionInfoFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.CUSTOMER_TRANSACTION_INFO_FETCH_DATA_STATUS:
			return action.payload;
		default:
			return state;
	}
}

const Customer = {
	customer,
	customerFetchStatus,
	aggregatedStock,
	aggregatedStockFetchStatus,
	companyTransactionsHistory,
	companyTransactionsHistoryFetchStatus,
	transactionInfo,
	transactionInfoFetchStatus
};

export default combineReducers(Customer);
