import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import Tab1 from './comps/tabs1';
import Tab2 from './comps/tabs2';
import Tab3 from './comps/tabs3';
import Tab4 from './comps/tabs4';
import GoogleMaps from '../../components/googleMaps';
import CustomerInfo from '../../components/customerInfo';
import UiTabs from '../../components/smartadmin/ui/UiTabs';
import { FormattedMessage } from 'react-intl';
import { GetCustomerPayload, PostChangeCustomerInterfaceBody } from './types';
import { SendReceiptPayload } from '../receipts/types';

class CustomersTemplate extends React.Component<Props> {
	componentDidMount() {
		// CUSTOMER ID
		let customerID = this.props.match.params.id;
		// GET CUSTOMER INFO
		this.props.getCustomerInfo(customerID);
	}

	render() {
		// VARS
		let customerID = this.props.match.params.id;
		let customerInfo = (this.props.customer && this.props.customer.customerInfo) ? this.props.customer.customerInfo : false;
		// let hasPalletBank = (this.props.customer && this.props.customer.customerInfo) ? this.props.customer.customerInfo.hasPalletBank : false;
		return (
			<div className="page-layout customers-page">
				<div className="container-fluid">
					<div className="row mb-15">
						<div className="col-sm-4">
							{customerInfo ?
								<CustomerInfo data={customerInfo} customerInfoFetchStatus={this.props.customerFetchStatus} /> : ''
							}
						</div>
						<div className="col-sm-8">
							{customerInfo ?
								<GoogleMaps
									defaultZoom={15}
									position={{
										lat: customerInfo.latitude,
										lng: customerInfo.longitude
									}}
									defaultCenter={{
										lat: customerInfo.latitude,
										lng: customerInfo.longitude
									}}
									containerElement={<div style={{ height: `483px` }} />}
									mapElement={<div style={{ height: `87%` }} />}
								/>
								: ''}
						</div>
					</div>
					<UiTabs id="tabs" className="mb-50">
						<ul className="nav nav-tabs">
							<li className="active">
								<a data-toggle="tab" href="#tabs-1"><FormattedMessage id="general.gathered" /></a>
							</li>
							<li>
								<a data-toggle="tab" href="#tabs-2"><FormattedMessage id="general.overview" /></a>
							</li>
							<li>
								<a data-toggle="tab" href="#tabs-3"><FormattedMessage id="general.flashback" /></a>
							</li>
							<li>
								<a data-toggle="tab" href="#tabs-4"><FormattedMessage id="general.customersettings" /></a>
							</li>
						</ul>
						<div className="tab-content p-10">
							<div className="tab-pane fade in active" id="tabs-1">
								<Tab1 customerID={customerID} />
							</div>
							<div className="tab-pane fade in" id="tabs-2">
								<Tab2 customerID={customerID} sendReceipt={this.props.sendReceipt} />
							</div>
							<div className="tab-pane fade in" id="tabs-3">
								<Tab3 />
							</div>
							<div className="tab-pane fade in" id="tabs-4">
								{
									customerInfo ?
										<Tab4 data={customerInfo} changeNewCustomer={this.props.changeNewCustomer} customerInfoFetchStatus={this.props.customerFetchStatus} newCustomerFetchStatus={this.props.customerFetchStatus} getCheckVATNumber={this.props.getCheckVATNumber} />
										: ''
								}
							</div>
						</div>
					</UiTabs>
				</div>
			</div>
		);
	}
}

export default CustomersTemplate;

interface Actions {
	getCustomerInfo(customerID: string);
	getCheckVATNumber(VATNumber: string);
	changeNewCustomer(customer: PostChangeCustomerInterfaceBody);
	sendReceipt(SendReceipt: SendReceiptPayload);
}
interface PathParamsType {
	id: string;
}
interface Props extends RouteComponentProps<PathParamsType>, Actions {
	customer: GetCustomerPayload;
	customerFetchStatus: boolean;
}
