export interface StockPayload {
	name: string;
	received: number;
	delivered: number;
	total: number;
}

export interface GetAggregatedStockPayload {
	stocks: StockPayload[];
	successfull: boolean;
	message: string;
}

export interface GetCustomerPayload {
	customerInfo: CustomerInfoPayload;
	successfull: boolean;
	message: string;
}

export interface CustomerInfoPayload {
	name?: string;
	address?: string;
	email?: string;
	postCode?: string;
	city?: string;
	country?: string;
	phoneNumber?: string;
	status?: string;
	isMyPalletCustomer?: boolean;
	vatNumber?: string;
	id?: string;
	latitude: number;
	longitude: number;
	contactPerson: string;
	hasPalletBank: boolean;
}

export interface CompanyTransactionsHistoryPayload {
	transactionID: string;
	receivingCompanyName: string;
	receivingCompanyCountry: string;
	receivingUserName: string;
	confirmedAt: string;
}

export interface CompanyTransactionHistoriesPayload {
	companyTransactionHistories: CompanyTransactionsHistoryPayload[];
	successfull: boolean;
	message: string;
}

export interface Registrations {
	type: string;
	product: string;
	quality: string;
	quantity: number;
	imagePaths: string[];
	productName: string;
	qualityName: string;
}

export interface GroupedRegistrationLine {
	name: string;
	received: number;
	delivered: number;
	total: number;
}

export interface Transaction {
	id: string;
	registrations: Registrations[];
	longitude: number;
	latitude: number;
	receivingUserName: string;
	receivingUserImagePath: string;
	sendingUserName: string;
	sendingUserImagePath: string;
	note: string;
	consignmentNotePath: string;
	sendingCompanyID: string;
	sendingUserCompanyName: string;
	receivingUserCompanyName: string;
	groupedRegistrationLines: GroupedRegistrationLine[];
	trailerNumber: string;
	registrationNumber: string;
	timestamp: string;
	receivingCompanyID: string;
	signaturePath: string;
}
export interface ChangeCustomerInterface {
	customerID: string;
	companyID: string;
	name: string;
	postCode: string;
	city: string;
	country: string;
	phoneNumber: string;
	vatNumber: string;
	longitude: number;
	latitude: number;
}

export interface PostChangeCustomerInterfaceBody extends ChangeCustomerInterface {
	address: string;
}

export interface ChangeInterfaceBody extends ChangeCustomerInterface {
	address: google.maps.places.PlaceResult;
}
export interface GetTransactionInfoPayload {
	transaction: Transaction;
	successfull: boolean;
	message: string;
}

export interface SendReceiptResponse {
	successfull: boolean;
	message: string;
}

export interface PostUpdatePalletBankStatus {
	customerID: string;
	newState: boolean;
}
