import C from './constants';
import { CustomerSrvc } from './services';
import { PostUpdatePalletBankStatus } from './types';

export const getCustomer = (customerID: string) => dispatch => {
	// VARS
	let Srvc: CustomerSrvc = new CustomerSrvc();
	// LOADING STATUS
	dispatch({
		type: C.CUSTOMER_INFO_FETCH_DATA_STATUS,
		payload: true
	});
	// SAVE
	dispatch({
		type: C.CUSTOMER_INFO_FETCH_DATA_SUCCESS,
		payload: null
	});
	dispatch({
		type: C.CUSTOMER_INFO_FETCH_DATA_FAILURE,
		payload: null
	});
	// SEND
	return Srvc.getCustomer(customerID).then(res => {
		// SAVE
		dispatch({
			type: C.CUSTOMER_INFO_FETCH_DATA_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.CUSTOMER_INFO_FETCH_DATA_STATUS,
			payload: false
		});
		return res;
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.CUSTOMER_INFO_FETCH_DATA_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.CUSTOMER_INFO_FETCH_DATA_STATUS,
			payload: false
		});
		return error;
	});
};

export const getAggregatedStock = (CompanyID: string, LocationFiltered?: boolean, LocationID?: string) => dispatch => {
	// VARS
	let Srvc: CustomerSrvc = new CustomerSrvc();
	// LOADING STATUS
	dispatch({
		type: C.CUSTOMER_AGGREGATED_STOCK_FETCH_DATA_STATUS,
		payload: true
	});
	// SEND
	return Srvc.getAggregatedStock(CompanyID, LocationFiltered, LocationID).then(res => {
		// SAVE
		dispatch({
			type: C.CUSTOMER_AGGREGATED_STOCK_FETCH_DATA_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.CUSTOMER_AGGREGATED_STOCK_FETCH_DATA_STATUS,
			payload: false
		});
		return res;
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.CUSTOMER_AGGREGATED_STOCK_FETCH_DATA_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.CUSTOMER_AGGREGATED_STOCK_FETCH_DATA_STATUS,
			payload: false
		});
		return error;
	});
};

export const getCompanyTransactionsHistory = (SelectedCompanyID: string, CompanyID: string) => dispatch => {
	// VARS
	let Srvc: CustomerSrvc = new CustomerSrvc();
	// LOADING STATUS
	dispatch({
		type: C.CUSTOMER_TRANSACTIONS_HISTORY_FETCH_DATA_STATUS,
		payload: true
	});
	// SEND
	return Srvc.getCompanyTransactionsHistory(SelectedCompanyID, CompanyID).then(res => {
		// SAVE
		dispatch({
			type: C.CUSTOMER_TRANSACTIONS_HISTORY_FETCH_DATA_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.CUSTOMER_TRANSACTIONS_HISTORY_FETCH_DATA_STATUS,
			payload: false
		});
		return res;
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.CUSTOMER_TRANSACTIONS_HISTORY_FETCH_DATA_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.CUSTOMER_TRANSACTIONS_HISTORY_FETCH_DATA_STATUS,
			payload: false
		});
		return error;
	});
};

export const updatePalletBankStatus = (payload: PostUpdatePalletBankStatus) => dispatch => {
	let Srvc: CustomerSrvc = new CustomerSrvc();
	return Srvc.updatePalletBankStatus(payload).then(res => { return res; });
};

export const getTransactionInfo = (TransactionID: string) => dispatch => {
	// VARS
	let Srvc: CustomerSrvc = new CustomerSrvc();
	// LOADING STATUS
	dispatch({
		type: C.CUSTOMER_TRANSACTION_INFO_FETCH_DATA_STATUS,
		payload: true
	});
	// SAVE
	dispatch({
		type: C.CUSTOMER_TRANSACTION_INFO_FETCH_DATA_SUCCESS,
		payload: null
	});
	dispatch({
		type: C.CUSTOMER_TRANSACTION_INFO_FETCH_DATA_FAILURE,
		payload: null
	});
	// SEND
	return Srvc.getTransactionInfo(TransactionID).then(res => {
		// SAVE
		dispatch({
			type: C.CUSTOMER_TRANSACTION_INFO_FETCH_DATA_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.CUSTOMER_TRANSACTION_INFO_FETCH_DATA_STATUS,
			payload: false
		});
		return res;
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.CUSTOMER_TRANSACTION_INFO_FETCH_DATA_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.CUSTOMER_TRANSACTION_INFO_FETCH_DATA_STATUS,
			payload: false
		});
		return error;
	});
};
