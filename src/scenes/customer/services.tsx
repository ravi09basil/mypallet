import { API } from '../../services/api';
import apisUrls from '../../config/apiUrls';
import { GetCustomerPayload, GetAggregatedStockPayload, GetTransactionInfoPayload, PostUpdatePalletBankStatus } from './types';

export class CustomerSrvc {
	API: API;

	constructor() {
		this.API = new API();
	}

	getCustomer(customerID: string): Promise<GetCustomerPayload> {
		return this.API.GET(apisUrls.GetCustomerInfo, {
			queryString: { customerID }
		});
	}

	getAggregatedStock(CompanyID: string, LocationFiltered?: boolean, LocationID?: string): Promise<GetAggregatedStockPayload> {
		return this.API.GET(apisUrls.GetAggregatedStock, {
			queryString: {
				CompanyID,
				LocationFiltered,
				LocationID
			}
		});
	}

	getCompanyTransactionsHistory(SelectedCompanyID: string, CompanyID: string): Promise<GetCustomerPayload> {
		return this.API.GET(apisUrls.GetCompanyTransactionsHistory, {
			queryString: { SelectedCompanyID, CompanyID }
		});
	}

	getTransactionInfo(TransactionID: string): Promise<GetTransactionInfoPayload> {
		return this.API.GET(apisUrls.GetTransactionInfo, {
			queryString: { TransactionID }
		});
	}
	updatePalletBankStatus(payload: PostUpdatePalletBankStatus) {
		return this.API.POST(apisUrls.PostUpdatePalletBankStatus, { body: JSON.stringify(payload)});
	}
}
