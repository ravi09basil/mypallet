import { API } from '../../services/api';
import apisUrls from '../../config/apiUrls';
import { GetTransactionInfoPayload } from '../customer/types';

export class ReceiptSrvc {
	API: API;

	constructor() {
		this.API = new API();
	}

	getTransactionInfo(TransactionID: string): Promise<GetTransactionInfoPayload> {
		return this.API.GET(apisUrls.GetTransactionInfo, {
			queryString: { TransactionID }
		});
	}
}
