import { ActionType } from '../../config/types';
import { combineReducers } from 'redux';
import C from './constants';

function receiptFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.RECEIPT_FETCH_DATA_STATUS:
			return action.payload;
		default:
			return state;
	}
}

function transactionInfo(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.RECEIPT_TRANSACTION_INFO_FETCH_DATA_SUCCESS:
			return action.payload;
		case C.RECEIPT_TRANSACTION_INFO_FETCH_DATA_FAILURE:
			return null;
		default:
			return state;
	}
}

function transactionInfoFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.RECEIPT_TRANSACTION_INFO_FETCH_DATA_STATUS:
			return action.payload;
		default:
			return state;
	}
}

const Receipt = {
	receiptFetchStatus,
	transactionInfo,
	transactionInfoFetchStatus
};

export default combineReducers(Receipt);
