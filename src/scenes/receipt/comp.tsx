import * as React from 'react';
import { GridOptions } from 'ag-grid';
import { RouteComponentProps } from 'react-router-dom';
import UserCard from '../../components/userCard';
import InfoCard from '../../components/infoCard';
import ImageCard from '../../components/imageCard';
import GoogleMaps from '../../components/googleMaps';
import AgGridTheme from '../../components/agGridTheme';
import { InjectedIntlProps, FormattedMessage, injectIntl } from 'react-intl';
import { GetTransactionInfoPayload, Registrations } from '../../scenes/customer/types';

class ReceiptTemplate extends React.Component<Props & InjectedIntlProps> {
    state: State = {
        transactionInfo: undefined,
        transactionInfoFetchStatus: false,
        gridOptions: {
			enableSorting: true,
			domLayout: 'autoHeight',
			onGridReady: () => {
				if (this.state.gridOptions.api) {
					this.state.gridOptions.api.sizeColumnsToFit();
				}
			},
			columnDefs: [
				{
					headerName: this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.columns.product' }),
					field: 'name'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.columns.received' }),
					field: 'received'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.columns.delivered' }),
					field: 'delivered'
				},
			]
		},
    };

    componentDidMount() {
        this.getData(this.props.match.params.id);
    }

    getData(transactionID: string) {
        this.props.getTransactionInfo(this.props.match.params.id).then(res => {
            this.setState({
              transactionInfo: res,
            });
            console.log(this.state.transactionInfo);
            this.state.transactionInfoFetchStatus = false;
          });
    }

	render() {
    let transaction = this.state.transactionInfo;
    let rowData = transaction ? transaction.transaction.groupedRegistrationLines : [];
    let collectImages = (accumulator: any[], currentValue: Registrations) => {
        return accumulator.concat(currentValue.imagePaths);
    };
    let registrationsImagePaths = (transaction && transaction.transaction.registrations.length) ? transaction.transaction.registrations.reduce(collectImages, []) : [];
    return (
        <div>
				<div className="modal-dialog modal-lg">
					<div className="modal-content">
						<header className="modal-header">
							<button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 className="modal-title" id="myModalLabel"><FormattedMessage id="comps.transactionInfoModal.title" /></h4>
						</header>
						<div className="modal-body">
							<div className="row mb-10">
								<div className="col-sm-12">
									{transaction && transaction.transaction && transaction.transaction.latitude && transaction.transaction.longitude ?
										<GoogleMaps
											defaultZoom={15}
											position={{ lat: transaction.transaction.latitude, lng: transaction.transaction.longitude }}
											defaultCenter={{ lat: transaction.transaction.latitude, lng: transaction.transaction.longitude }}
											containerElement={<div style={{ height: `400px` }} />}
											mapElement={<div style={{ height: `100%` }} />}
										/> : ''}
								</div></div>
							{
								(rowData && rowData.length) ?
									<div className="mb-30 ag-theme-balham">
										<AgGridTheme rowData={rowData} gridOptions={this.state.gridOptions} />
									</div> : ''
                            }
							<div className="row mb-10">
								<div className="col-md-6">
									{transaction ?
                                        <UserCard title={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.receiverTitle' })} username={transaction.transaction.receivingUserName} companyName={transaction.transaction.receivingUserCompanyName} userImagePath={transaction.transaction.receivingUserImagePath} /> : ''}
								</div>
								<div className="col-md-6">
									{transaction ?
										<UserCard title={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.senderTitle' })} username={transaction.transaction.sendingUserName} companyName={transaction.transaction.sendingUserCompanyName} userImagePath={transaction.transaction.sendingUserImagePath} /> : ''}
								</div>
							</div>
							<div className="row mb-10">
								<div className="col-sm-6">
									{transaction && transaction.transaction.note ?
										<InfoCard
											title={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.note.title' })}
											valueIsImage={false}
											defaultText={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.note.defaultText' })}
											icon="fa fa-edit fa-3x"
											value={transaction.transaction.note}
										/> : ''}
								</div>
								<div className="col-sm-6">
									{transaction && transaction.transaction.consignmentNotePath ?
										<InfoCard
											title={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.consignmentNote.title' })}
											valueIsImage={true}
											defaultText={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.consignmentNote.defaultText' })}
											icon="fa fa-file-image-o fa-3x"
											value={transaction.transaction.consignmentNotePath}
										/> : ''}
								</div>
							</div>
							<div className="row mb-10">
								<div className="col-sm-6">
									{transaction && transaction.transaction.trailerNumber ?
										<InfoCard
											title={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.trailerNumber.title' })}
											valueIsImage={false}
											defaultText={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.trailerNumber.defaultText' })}
											icon="fa fa-truck fa-3x"
											value={transaction.transaction.trailerNumber}
										/> : ''}
								</div>
								<div className="col-sm-6">
									{transaction && transaction.transaction.registrationNumber ?
										<InfoCard
											title={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.registrationNumber.title' })}
											valueIsImage={false}
											defaultText={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.registrationNumber.defaultText' })}
											icon="fa fa-truck fa-3x"
											value={transaction.transaction.registrationNumber}
										/> : ''}

								</div>
							</div>
							<div className="row mb-10">
								{transaction && transaction.transaction.signaturePath ?
									<div className="col-sm-6">
										<ImageCard
											title={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.signaturePath.title' })}
											linkText={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.signaturePath.openSignature' })}
											defaultText=""
											icon="fa fa-file-image-o fa-3x"
											images={[transaction.transaction.signaturePath]}
										/>
									</div> : ''}
								{registrationsImagePaths.length ?
									<div className="col-sm-6">
										<ImageCard
											title={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.imageCard.title' })}
											linkText={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.imageCard.seeProductImage' })}
											defaultText={this.props.intl.formatMessage({ id: 'comps.transactionInfoModal.cards.imageCard.defaultText' })}
											icon="fa fa-file-image-o fa-3x"
											images={registrationsImagePaths}
										/>
									</div>
									: ''}
							</div>
						</div>
						<footer className="modal-footer">
							<button type="button" className="btn btn-default" data-dismiss="modal"><FormattedMessage id="general.close" /></button>
						</footer>
					</div>
				</div>
			</div >
    );
	}
}

export default injectIntl(ReceiptTemplate);

// interface State {
// 	gridOptions: GridOptions;
// 	filterVal: string;
// 	transactionInfo: undefined | GetTransactionInfoPayload;
// }
interface PathParamsType {
	id: string;
}
interface Actions {
	getReceiptsList(CompanyID: string);
	getTransactionInfo(TransactionID: string);
}
interface Props extends RouteComponentProps<PathParamsType>, Actions {
	receiptsFetchStatus: boolean;
	transactionInfoFetchStatus: boolean;
    transactionInfo: GetTransactionInfoPayload;
    
}
interface State {
    transactionInfo: undefined | GetTransactionInfoPayload;
    transactionInfoFetchStatus: boolean;
    gridOptions: GridOptions;
}