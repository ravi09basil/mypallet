import { connect } from 'react-redux';
import { getTransactionInfo } from './actions';
import ReceiptTemplate from './comp';

const mapDispatchToProps = (dispatch, props) => {
	return ({
		getTransactionInfo(TransactionID: string) {
			return dispatch(getTransactionInfo(TransactionID));
		}
	});
};

const mapStateToProps = (state) => {
	return {
		'receiptsFetchStatus': state.Receipts.receiptsFetchStatus,
		'transactionInfo': state.Receipts.transactionInfo,
		'transactionInfoFetchStatus': state.Receipts.transactionInfoFetchStatus
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(ReceiptTemplate);
