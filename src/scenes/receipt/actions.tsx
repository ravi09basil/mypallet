import C from './constants';
import { ReceiptSrvc } from './services';

export const getTransactionInfo = (TransactionID: string) => dispatch => {
	// VARS
	let Srvc: ReceiptSrvc = new ReceiptSrvc();
	// LOADING STATUS
	dispatch({
		type: C.RECEIPT_TRANSACTION_INFO_FETCH_DATA_STATUS,
		payload: true
	});
	// SEND
	return Srvc.getTransactionInfo(TransactionID).then(res => {
		// SAVE
		dispatch({
			type: C.RECEIPT_TRANSACTION_INFO_FETCH_DATA_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.RECEIPT_TRANSACTION_INFO_FETCH_DATA_STATUS,
			payload: false
		});
		return res;
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.RECEIPT_TRANSACTION_INFO_FETCH_DATA_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.RECEIPT_TRANSACTION_INFO_FETCH_DATA_STATUS,
			payload: false
		});
		return error;
	});
};
