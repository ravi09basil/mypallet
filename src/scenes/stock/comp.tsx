import * as React from 'react';
import { InjectedIntlProps, FormattedMessage, injectIntl } from 'react-intl';
import { GridOptions, ICellRendererParams, CellEditingStoppedEvent } from 'ag-grid';
import Loader from '../../components/loader';
import AgGridTheme from '../../components/agGridTheme';
import Select2 from '../../components/smartadmin/forms/inputs/Select2';
import { GetStockListPayload, GetLocationsPayload, SavePriceInformationForProductBody } from './types';

function formatDollar(num: number) {
	let n = (Number(num).toFixed(2) + '').split('.');
	return n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',') + '.' + (n[1] || '00');
}

class StockTemplate extends React.Component<Props & InjectedIntlProps> {
	state: State = {
		gridOptions: {
			enableSorting: true,
			onCellEditingStopped: (event: CellEditingStoppedEvent) => {
				if (this.state.gridOptions.api) {
					// CALL
					var rowNode = this.state.gridOptions.api.getRowNode(event.node.id);
					rowNode.setDataValue('actions', {
						updated: event
					});
					// UPDATE THE GRID AFTER EDITING
					this.state.gridOptions.api.redrawRows();
					this.state.gridOptions.api.resetRowHeights();
				}
			},
			columnDefs: [
				{
					autoHeight: 1,
					headerName: '',
					field: 'actions',
					cellRenderer: (params: ICellRendererParams) => {
						// VARS
						let div = document.createElement('div');
						let updatedData: CellEditingStoppedEvent = (params.data.actions && params.data.actions.updated) ? params.data.actions.updated : false;
						div.className = 'text-center';
						if (updatedData) {
							var link = document.createElement('a');
							link.className = 'btn btn-success btn-s';
							link.innerText = 'Save';
							link.addEventListener('click', (e) => {
								this.props.postSavePriceInformationForProduct({
									productID: updatedData.data.productID,
									companyID: this.props.companyID,
									typeAPrice: updatedData.data.priceA,
									typeBPrice: updatedData.data.priceB,
									typeCPrice: updatedData.data.priceC
								});
								if (this.state && this.state.gridOptions && this.state.gridOptions.api) {
									// CALL
									var rowNode = this.state.gridOptions.api.getRowNode(updatedData.node.id);
									rowNode.setDataValue('actions', {
										updated: false
									});
									// UPDATE THE GRID AFTER EDITING
									this.state.gridOptions.api.redrawRows();
									this.state.gridOptions.api.resetRowHeights();
								}
								e.preventDefault();
							});
							div.appendChild(link);
						}
						return div;
					}
				},
				{
					children: [
						{
							headerName: this.props.intl.formatMessage({ id: 'general.name' }),
							field: 'name'
						},
					]
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.total' }),
					children: [
						{
							headerName: 'Tom emballage total',
							columnGroupShow: 'closed',
							field: 'totalEmpty'
						},
						{
							headerName: this.props.intl.formatMessage({ id: 'scenes.stock.totalValueInnDKK' }),
							columnGroupShow: 'closed',
							field: 'totalPrice',
							cellRenderer: (params: ICellRendererParams) => {
								let data = params.data;
								return formatDollar(data.priceA + data.priceB + data.priceC);
							}
						},
						{
							headerName: 'Type A',
							columnGroupShow: 'open',
							children: [
								{
									headerName: 'Stk. Pris',
									editable: true,
									field: 'priceA'
								},
								{
									headerName: this.props.intl.formatMessage({ id: 'general.empty' }),
									field: 'qtyA'
								},
							]
						},
						{
							headerName: 'Type B',
							columnGroupShow: 'open',
							children: [
								{
									headerName: 'Stk. Pris',
									editable: true,
									field: 'priceB'
								},
								{
									headerName: this.props.intl.formatMessage({ id: 'general.empty' }),
									field: 'qtyB'
								},
							]
						},
						{
							headerName: 'Type C',
							columnGroupShow: 'open',
							children: [
								{
									headerName: 'Stk. Pris',
									editable: true,
									field: 'priceC'
								},
								{
									headerName: this.props.intl.formatMessage({ id: 'general.empty' }),
									field: 'qtyC'
								},
							]
						}
					]
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'scenes.stock.totalValueInnDKK' }),
					field: 'totalValueInnDKK',
					cellRenderer: (params: ICellRendererParams) => {
						let data = params.data;
						return formatDollar(data.priceA * data.qtyA) + (data.priceB * data.qtyB) + (data.priceC * data.qtyC);
					}
				}
			]
		}
	};

	constructor(props: Props & InjectedIntlProps) {
		super(props);
		this.locationChanged = this.locationChanged.bind(this);
	}

	componentDidMount() {
		this.props.getStockList(this.props.companyID);
		this.props.getLocationsList(this.props.companyID);
	}

	locationChanged(v: any) {
		// VARS
		let locationID = v.target.value;
		this.props.getStockList(this.props.companyID, true, locationID);
	}

	render() {
		let rowData = (this.props.stockList && this.props.stockList.stocks) ? this.props.stockList.stocks : [];
		let locations = (this.props.locationsList && this.props.locationsList.locations) ? this.props.locationsList.locations : false;
		return (
			<div className="page-layout stock-page">
				<div className="container-fluid">
					{
						locations ?
							<div className="row mb-30">
								<div className="col-sm-3">
									<h1><FormattedMessage id="general.locationFilter" /></h1>
									<Select2 options={{ width: '100%' }} className="select2" onSelect={this.locationChanged}>
										<option value="0">{this.props.intl.formatMessage({ id: 'general.allLocations' })}</option>
										{(locations ? locations.map((i) => <option value={i.id} key={i.id}>{i.name}</option>) : '')}
									</Select2>
								</div>
							</div> : ''
					}
					<div className="row mb-15">
						<div className="col-sm-12">
							<div className="grid-st1 ag-theme-balham">
								<AgGridTheme rowData={rowData} gridOptions={this.state.gridOptions} />
							</div>
						</div>
					</div>
				</div>
				<Loader status={this.props.stockListStatus} />
			</div>
		);
	}
}

export default injectIntl(StockTemplate);

interface State {
	gridOptions: GridOptions;
}
interface Actions {
	getStockList(CompanyID: string, LocationFiltered?: boolean, LocationID?: string);
	getLocationsList(CompanyID: string);
	postSavePriceInformationForProduct(payload: SavePriceInformationForProductBody);
}
interface Props extends Actions {
	companyID: string;
	stockList: GetStockListPayload;
	stockListStatus: boolean;
	locationsList: GetLocationsPayload;
	locationsListFetchStatus: boolean;
}
