import { connect } from 'react-redux';
import { getStockList, getLocationsList, savePriceInformationForProduct } from './actions';
import StockTemplate from './comp';
import { SavePriceInformationForProductBody } from './types';

const mapDispatchToProps = (dispatch): Actions => ({
	getStockList(CompanyID: string, LocationFiltered?: boolean, LocationID?: string) {
		return dispatch(getStockList(CompanyID, LocationFiltered, LocationID));
	},
	getLocationsList(CompanyID: string) {
		return dispatch(getLocationsList(CompanyID));
	},
	postSavePriceInformationForProduct(payload: SavePriceInformationForProductBody) {
		return dispatch(savePriceInformationForProduct(payload));
	}
});

const mapStateToProps = (state) => {
	return {
		'companyID': state.Login.authentication.companyID,
		'stockList': state.Stock.stockList,
		'stockListStatus': state.Stock.stockFetchStatus,
		'locationsList': state.Stock.locationsList,
		'locationsListFetchStatus': state.Stock.locationsListFetchStatus
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(StockTemplate);

type Actions = {
	getStockList(CompanyID: string, LocationFiltered?: boolean, LocationID?: string);
	getLocationsList(CompanyID: string);
	postSavePriceInformationForProduct(payload: SavePriceInformationForProductBody)
};
