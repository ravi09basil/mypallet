import C from './constants';
import { StockSrvc } from './services';
import { SavePriceInformationForProductBody } from './types';

export const getStockList = (CompanyID: string, LocationFiltered?: boolean, LocationID?: string) => dispatch => {
	// VARS
	let srvc: StockSrvc = new StockSrvc();
	// LOADING STATUS
	dispatch({
		type: C.FETCH_STOCK_DATA_STATUS,
		payload: true
	});
	// SEND
	return srvc.getStockList(CompanyID, LocationFiltered, LocationID).then(res => {
		// SAVE
		dispatch({
			type: C.FETCH_STOCK_DATA_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.FETCH_STOCK_DATA_STATUS,
			payload: false
		});
		return res;
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.FETCH_STOCK_DATA_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.FETCH_STOCK_DATA_STATUS,
			payload: false
		});
		return error;
	});
};

export const getLocationsList = (CompanyID: string) => dispatch => {
	// VARS
	let srvc: StockSrvc = new StockSrvc();
	// LOADING STATUS
	dispatch({
		type: C.LOCATION_LIST_FETCH_STATUS,
		payload: true
	});
	// SEND
	return srvc.getLocations(CompanyID).then(res => {
		// SAVE
		dispatch({
			type: C.LOCATION_LIST_FETCH_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.LOCATION_LIST_FETCH_STATUS,
			payload: false
		});
		return res;
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.LOCATION_LIST_FETCH_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.LOCATION_LIST_FETCH_STATUS,
			payload: false
		});
		return error;
	});
};

export const getLocationsListForCustomer = (CustomerID: string) => dispatch => {
	// VARS
	let srvc: StockSrvc = new StockSrvc();
	// LOADING STATUS
	dispatch({
		type: C.LOCATION_LIST_FETCH_STATUS,
		payload: true
	});
	// SEND
	return srvc.getLocations(CustomerID).then(res => {
		// SAVE
		dispatch({
			type: C.LOCATION_LIST_FETCH_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.LOCATION_LIST_FETCH_STATUS,
			payload: false
		});
		return res;
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.LOCATION_LIST_FETCH_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.LOCATION_LIST_FETCH_STATUS,
			payload: false
		});
		return error;
	});
};

export const savePriceInformationForProduct = (payload: SavePriceInformationForProductBody) => dispatch => {
	// VARS
	let srvc: StockSrvc = new StockSrvc();
	// LOADING STATUS
	dispatch({
		type: C.FETCH_STOCK_DATA_STATUS,
		payload: true
	});
	// SEND
	return srvc.postSavePriceInformationForProduct(payload).then(res => {
		// LOADING STATUS
		dispatch({
			type: C.FETCH_STOCK_DATA_STATUS,
			payload: false
		});
		return res;
	}).catch(error => {
		// LOADING STATUS
		dispatch({
			type: C.FETCH_STOCK_DATA_STATUS,
			payload: false
		});
		return error;
	});
};
