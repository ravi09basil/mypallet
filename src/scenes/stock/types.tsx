export interface SuccessfullPayload {
	successfull: boolean;
	message: string;
}

export interface Stock {
	name: string;
	priceA: number;
	priceB: number;
	priceC: number;
	qtyA: number;
	qtyB: number;
	qtyC: number;
}

export interface GetStockListPayload extends SuccessfullPayload {
	stocks: Stock[];
}

export interface Location {
	id: string;
	name: string;
	address: string;
	zipCode: string;
	city: string;
	country: string;
	phoneNumber: string;
	email: string;
	latitude: number;
	longitude: number;
}

export interface GetLocationsPayload extends SuccessfullPayload {
	locations: Location[];
}

export interface SavePriceInformationForProductBody {
	productID: string;
	companyID: string;
	typeAPrice: number;
	typeBPrice: number;
	typeCPrice: number;
}
