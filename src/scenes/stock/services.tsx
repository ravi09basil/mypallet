import { API } from '../../services/api';
import apisUrls from '../../config/apiUrls';
import { SuccessfullPayload, GetStockListPayload, GetLocationsPayload, SavePriceInformationForProductBody } from './types';

export class StockSrvc {
	API: API;

	constructor() {
		this.API = new API();
	}

	getStockList(CompanyID: string, LocationFiltered?: boolean, LocationID?: string): Promise<GetStockListPayload> {
		return this.API.GET(apisUrls.GetStockList, {
			queryString: {
				CompanyID,
				LocationFiltered,
				LocationID
			}
		});
	}

	getLocations(CompanyID: string): Promise<GetLocationsPayload> {
		return this.API.GET(apisUrls.GetLocations, {
			queryString: { CompanyID }
		});
	}

	getLocationsForCustomer(CustomerID: string): Promise<GetLocationsPayload> {
		return this.API.GET(apisUrls.GetLocationsForCustomer, {
			queryString: { CustomerID }
		});
	}

	postSavePriceInformationForProduct(payload: SavePriceInformationForProductBody): Promise<SuccessfullPayload> {
		return this.API.POST(apisUrls.PostSavePriceInformationForProduct, {
			body: JSON.stringify(payload)
		});
	}
}
