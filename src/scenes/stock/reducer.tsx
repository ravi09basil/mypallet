import { ActionType } from '../../config/types';
import { combineReducers } from 'redux';
import C from './constants';

function stockList(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.FETCH_STOCK_DATA_SUCCESS:
			return action.payload;
		case C.FETCH_STOCK_DATA_FAILURE:
			return null;
		default:
			return state;
	}
}

function stockFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.FETCH_STOCK_DATA_STATUS:
			return action.payload;
		default:
			return state;
	}
}

function locationsList(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.LOCATION_LIST_FETCH_SUCCESS:
			return action.payload;
		case C.LOCATION_LIST_FETCH_FAILURE:
			return null;
		default:
			return state;
	}
}

function locationsListFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.LOCATION_LIST_FETCH_STATUS:
			return action.payload;
		default:
			return state;
	}
}

const Stock = {
	stockList,
	stockFetchStatus,
	locationsList,
	locationsListFetchStatus
};

export default combineReducers(Stock);
