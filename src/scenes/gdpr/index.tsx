import * as React from 'react';
import { FormattedHTMLMessage } from 'react-intl';

class Gdpr extends React.Component {
	render() {
		return (
			<div className="page-layout gdpr-page">
				<div className="container-fluid">
					<div className="row">
						<div className="col-sm-12">
							<FormattedHTMLMessage id="scenes.gdpr" />
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Gdpr;
