import LoginTemplate from './comp';
import { login } from './actions';
import { connect } from 'react-redux';

const mapDispatchToProps = (dispatch): Actions => ({
	loginAction(username: string, password: string) {
		return dispatch(login(username, password));
	}
});

const mapStateToProps = (state) => {
	return {
		'Authentication': state.Login.authentication,
		'AuthenticationFetchStatus': state.Login.authenticationFetchStatus,
		'AdministrationUserInfo': state.Login.administrationUserInfo
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginTemplate);

type Actions = {
	loginAction(username: string, password: string);
};
