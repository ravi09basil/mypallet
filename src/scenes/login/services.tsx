import { API } from '../../services/api';
import API_URLS from '../../config/apiUrls';
import { /*GetAuthenticateAdministrationUserPayload,*/ GetAdministrationUserInfoPayload, GetLicenseInfoPayload } from './types';

export class LoginSrvc {
	API: API;

	constructor() {
		this.API = new API();
	}

	GetAuthenticateAdministrationUser(username: string, password: string)/*: Promise<GetAuthenticateAdministrationUserPayload>*/ {
		return this.API.POST(API_URLS.PostAuthenticateAdministrationUser, {
			body: JSON.stringify({
				'username': username,
				'password': password
			})
		});
	}

	GetAdministrationUserInfo(userId: string): Promise<GetAdministrationUserInfoPayload> {
		return this.API.GET(API_URLS.GetAdministrationUserInfo, {
			queryString: {
				'UserID': userId
			}
		});
	}

	GetLicenseInfo(CompanyID: string): Promise<GetLicenseInfoPayload> {
		return this.API.GET(API_URLS.GetLicenseInfo, {
			queryString: {
				CompanyID
			}
		});
	}
}
