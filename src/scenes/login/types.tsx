export interface GetAuthenticateAdministrationUserPayload {
	isAuthenticated: boolean;
	administrationUserID: string;
	companyID: string;
	successfull: boolean;
	message: string;
}

export interface GetAdministrationUserInfoPayload {
	name: string;
	email: string;
	id: string;
	imagePath: string;
	shouldChangePassword: boolean;
	successfull: true;
	message: string;
	companyLogo: string;
}

export interface LicenseInfo {
	license: string;
	hasLicense: boolean;
	licenseName: string;
}

export interface GetLicenseInfoPayload {
	licenseInfos: LicenseInfo[];
	successfull: boolean;
	message: string;
}
