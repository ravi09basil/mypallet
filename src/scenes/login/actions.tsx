import C from './constants';
import { LoginSrvc } from './services';

export const GetLicenseInfo = (CompanyID: string) => dispatch => {
	// VARS
	let loginSrvc: LoginSrvc = new LoginSrvc();
	// LOADING STATUS
	dispatch({
		type: C.LICENCE_INFO_FETCH_STATUS,
		payload: true
	});
	// SEND
	loginSrvc.GetLicenseInfo(CompanyID).then(res => {
		// LOADING STATUS
		dispatch({
			type: C.LICENCE_INFO_FETCH_STATUS,
			payload: false
		});
		// SAVE
		dispatch({
			type: C.LICENCE_INFO_FETCH_SUCCESS,
			payload: res
		});
		return res;
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.LICENCE_INFO_FETCH_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.LICENCE_INFO_FETCH_STATUS,
			payload: false
		});
		return error;
	});
};

export const GetAdministrationUserInfo = (userId: string) => dispatch => {
	// VARS
	let loginSrvc: LoginSrvc = new LoginSrvc();
	// CALL THE API
	return loginSrvc.GetAdministrationUserInfo(userId).then(user => {
		// SAVE
		user.imagePath = 'https://www.silvan.dk/~/media/images/silvan/images/om-silvan/pressemateriale/logo-uden_payoff.jpg';
		dispatch({
			type: C.ADMINISTRATION_USER_INFO,
			payload: user
		});
		// LOADING STATUS
		dispatch({
			type: C.AUTHENTICATION_FETCH_STATUS,
			payload: false
		});
	});
};

export const login = (username: string, password: string) => dispatch => {
	// VARS
	let loginSrvc: LoginSrvc = new LoginSrvc();
	// LOADING STATUS
	dispatch({
		type: C.AUTHENTICATION_FETCH_STATUS,
		payload: true
	});
	// SEND
	return loginSrvc.GetAuthenticateAdministrationUser(username, password).then(res => {
		// IF USER IS AUTHENTICATED
		if (res.isAuthenticated) {
			// SAVE AUTHENTICATION
			dispatch({
				type: C.AUTHENTICATION_LOGIN_SUCCESS,
				payload: res
			});
			return Promise.all([
				GetAdministrationUserInfo(res.administrationUserID)(dispatch),
				GetLicenseInfo(res.companyID)(dispatch)
			]);
		} else {
			// ERROR
			dispatch({
				type: C.AUTHENTICATION_LOGIN_FAILURE,
				payload: res
			});
		}
		// LOADING STATUS
		dispatch({
			type: C.AUTHENTICATION_FETCH_STATUS,
			payload: false
		});
		return res;
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.AUTHENTICATION_LOGIN_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.AUTHENTICATION_FETCH_STATUS,
			payload: false
		});
		return error;
	});
};
