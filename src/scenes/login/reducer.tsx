import { ActionType } from '../../config/types';
import { combineReducers } from 'redux';
import C from './constants';

function authentication(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.AUTHENTICATION_LOGIN_SUCCESS:
			return action.payload;
		case C.AUTHENTICATION_LOGIN_FAILURE:
			return action.payload;
		default:
			return state;
	}
}

function authenticationFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.AUTHENTICATION_FETCH_STATUS:
			return action.payload;
		default:
			return state;
	}
}

function administrationUserInfo(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.ADMINISTRATION_USER_INFO:
			return action.payload;
		default:
			return state;
	}
}

function licenseInfo(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.LICENCE_INFO_FETCH_SUCCESS:
			return action.payload;
		case C.LICENCE_INFO_FETCH_FAILURE:
			return action.payload;
		default:
			return state;
	}
}

const Login = {
	authentication,
	authenticationFetchStatus,
	administrationUserInfo,
	licenseInfo
};

export default combineReducers(Login);
