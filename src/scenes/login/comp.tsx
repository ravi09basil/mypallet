import * as React from 'react';
import { Form, Text, FormApi } from 'react-form';
import { RouteComponentProps } from 'react-router-dom';
import { InjectedIntlProps, FormattedMessage, injectIntl } from 'react-intl';
import routes from '../../routes';
import Loader from '../../components/loader';
import InputMessages from '../../components/inputMsg';
import ErrorHolder from '../../components/errorHolder';
import { GetAuthenticateAdministrationUserPayload, GetAdministrationUserInfoPayload } from './types';
import './style.scss';

class LoginTemplate extends React.Component<Props & InjectedIntlProps & RouteComponentProps<{}>> {
	componentDidMount() {
		this.redirectUser();
	}

	redirectUser() {
		// VARS
		let URL: string;
		let AdministrationUserInfo = this.props.AdministrationUserInfo;
		// CHECK IF THE USER IS AUTHONTICATED
		if (AdministrationUserInfo) {
			// IF THE USER SHOULD CHANGE THE PASSWORD
			if (AdministrationUserInfo.shouldChangePassword) {
				URL = routes.CHANGE_PASSWORD.url;
			} else {
				URL = (this.props.location.state && this.props.location.state.from && this.props.location.state.from.pathname !== '/') ? this.props.location.state.from.pathname : routes.DASHBOARD.url;
			}
			if (URL) {
				// REDIRECT THE USER
				this.props.history.push(URL);
			}
		}
	}

	validate = values => {
		return {
			username: values.username ? null : this.props.intl.formatMessage({ id: 'general.validation.username' }),
			password: values.password ? null : this.props.intl.formatMessage({ id: 'general.validation.password' })
		};
	}

	handleSubmit(e: FormInputs) {
		// LOGIN
		this.props.loginAction(e.username, e.password).then(() => {
			this.redirectUser();
		});
	}

	render() {
		// VARS
		let message: string = '';
		let successfull = true;
		let authentication = this.props.Authentication;
		if (authentication && !authentication.successfull) {
			message = authentication.message;
			successfull = authentication.successfull;
		}
		return (
			<div className="page-layout login-page">
				<Form onSubmit={(e) => { this.handleSubmit(e); }} validate={this.validate} validateOnSubmit={true}>
					{
						(formApi: FormApi) => {
							return (
								<div className="jarviswidget" role="widget">
									<header role="heading">
										<h2><FormattedMessage id="scenes.login.title" /></h2>
									</header>
									<div role="content">
										<div className="widget-body no-padding">
											<form className="form-st1 p-25" onSubmit={formApi.submitForm}>
												<fieldset>
													<ErrorHolder message={message} hide={successfull} />
													<InputMessages name="username" formApi={formApi}>
														<label><FormattedMessage id="general.form.username" /></label>
														<div className="input-group">
															<div className="input-group-addon"><i className="icon-prepend fa fa-user" /></div>
															<Text className="form-control" field="username" placeholder={this.props.intl.formatMessage({ id: 'general.form.username' })} autoComplete="username" />
														</div>
													</InputMessages>
													<InputMessages name="password" formApi={formApi}>
														<label><FormattedMessage id="general.form.password" /></label>
														<div className="input-group">
															<div className="input-group-addon"><i className="icon-prepend fa fa-lock" /></div>
															<Text className="form-control" field="password" placeholder={this.props.intl.formatMessage({ id: 'general.form.password' })} type="password" autoComplete="current-password" />
														</div>
													</InputMessages>
												</fieldset>
												<footer className="text-right">
													<button type="submit" className="btn btn-success btn-lg"><FormattedMessage id="general.login" /></button>
												</footer>
											</form>
										</div>
									</div>
								</div>
							);
						}
					}
				</Form>
				<Loader status={this.props.AuthenticationFetchStatus} />
			</div>
		);
	}
}

export default injectIntl(LoginTemplate);

interface Actions {
	loginAction(username: string, password: string);
}
interface Props extends Actions {
	Authentication: GetAuthenticateAdministrationUserPayload;
	AuthenticationFetchStatus: boolean;
	AdministrationUserInfo: GetAdministrationUserInfoPayload;
}
interface FormInputs {
	username: string;
	password: string;
}
