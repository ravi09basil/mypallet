export interface WarehouseHotel {
	id: string;
	name: string;
	adress: string;
	zipCode: string;
	city: string;
	country: string;
	phoneNumber: string;
	email: string;
	latitude: number;
	longitude: number;
	logoPath: string;
	ownerLocationID: string;
}

export interface Location {
	id: string;
	name: string;
	address: string;
	zipCode: string;
	city: string;
	country: string;
	phoneNumber: string;
	email: string;
	latitude: number;
	longitude: number;
	warehouseHotels: WarehouseHotel[];
}

export interface GetLocationsListPayload {
	locations: Location[];
	successfull: boolean;
	message: string;
}

export interface LocationBody {
	locationID?: string;
	city: string;
	country: string;
	email: string;
	name: string;
	phoneNumber: string;
	zipCode: string;
	latitude: number;
	longitude: number;
	companyID: string;
}

export interface PostLocationBody extends LocationBody {
	address: string;
}

export interface PostLocationForm extends LocationBody {
	id?: string;
	address: google.maps.places.PlaceResult;
}

export interface PostSuccessPayload {
	successfull: boolean;
	message: string;
}
