import { connect } from 'react-redux';
import { getLocationsList, createLocation, createWarehouseHotel, editLocation } from './actions';
import LocationsTemplate from './comp';

const mapDispatchToProps = (dispatch) => {
	return ({
		getLocationsList(CompanyID: string) {
			return dispatch(getLocationsList(CompanyID));
		},
		postCreateLocation: (location) => {
			return dispatch(createLocation(location));
		},
		postCreateWarehouseHotel: (location) => {
			return dispatch(createWarehouseHotel(location));
		},
		postEditLocation: (location) => {
			return dispatch(editLocation(location));
		}
	});
};

const mapStateToProps = (state) => {
	return {
		'companyID': state.Login.authentication.companyID,
		'locationsList': state.Locations.locationsList,
		'locationsFetchStatus': state.Locations.locationsFetchStatus,
		'locationModalFetchStatus': state.Locations.locationModalFetchStatus
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(LocationsTemplate);
