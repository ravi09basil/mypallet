import * as React from 'react';
import { GridOptions, ICellRendererParams } from 'ag-grid';
import { InjectedIntlProps, injectIntl } from 'react-intl';
import { returnGoogleLocationDetails } from '../../services/util';
import Loader from '../../components/loader';
import GridTools from '../../components/gridTools';
import AgGridTheme from '../../components/agGridTheme';
import { bigBox } from '../../components/smartadmin/utils/actions/MessageActions';
import { NewLocationModal, EditLocationModal } from '../../components/locationModal';
import { GetLocationsListPayload, PostLocationForm, PostLocationBody, Location } from './types';

class LocationsTemplate extends React.Component<Props & InjectedIntlProps> {
	NewLocationModalID = 'newLocationModalID';
	NewWarehouseModalID = 'newWarehouseModalID';
	EditLocationModalID = 'editLocationModalID';
	state: State = {
		location: null,
		gridOptions: {
			onGridReady: () => {
				if (this.state.gridOptions.columnApi) {
					setTimeout(() => { if (this.state.gridOptions.api) { this.state.gridOptions.api.resetRowHeights(); if (this.state.gridOptions.columnApi) { this.state.gridOptions.columnApi.autoSizeAllColumns(); } if (this.state.gridOptions.api) { this.state.gridOptions.api.sizeColumnsToFit(); } } }, 500);
				}
			},
			getNodeChildDetails: (rowItem: any) => {
				if (rowItem.warehouseHotels) {
					return {
						group: true,
						// provide ag-Grid with the children of this group
						children: rowItem.warehouseHotels,
						expanded: true,
						// the key is used by the default group cellRenderer
						key: rowItem.id
					};
				}
				return {
					group: false
				};
			},
			groupMultiAutoColumn: false,
			columnDefs: [
				{
					autoHeight: 1,
					headerName: this.props.intl.formatMessage({ id: 'general.name' }),
					field: 'name',
					cellRenderer: 'agGroupCellRenderer'
				},
				{
					autoHeight: 1,
					headerName: this.props.intl.formatMessage({ id: 'general.address' }),
					field: 'adress',
				},
				{
					autoHeight: 1,
					headerName: this.props.intl.formatMessage({ id: 'general.postCode' }),
					field: 'zipCode'
				},
				{
					autoHeight: 1,
					headerName: this.props.intl.formatMessage({ id: 'general.city' }),
					field: 'city'
				},
				{
					autoHeight: 1,
					headerName: this.props.intl.formatMessage({ id: 'general.country' }),
					field: 'country'
				},
				{
					autoHeight: 1,
					headerName: this.props.intl.formatMessage({ id: 'general.phoneNumber' }),
					field: 'phoneNumber'
				},
				{
					autoHeight: 1,
					headerName: 'Email',
					field: 'email'
				},
				{
					autoHeight: 40,
					headerName: this.props.intl.formatMessage({ id: 'general.functions' }),
					field: 'functions',
					suppressSizeToFit: true,
					cellRenderer: (params: ICellRendererParams) => {
						if (params.data.ownerLocationID) {
							let divWrap = document.createElement('div');
							let createChild = document.createElement('a');
							createChild.innerHTML = `<a class="btn btn-primary btn-s mr-10 mb-10 fa fa-external-link" title="Open"></a>`;
							
							divWrap.appendChild(createChild);
							divWrap.className = 'actions';
							return divWrap;
						} else {
							let divWrap = document.createElement('div');
							let edit = document.createElement('a');
							edit.innerHTML = `<a class="btn btn-primary btn-s mr-10 mb-10 fa fa-edit title="Edit"></a>`;
							edit.addEventListener('click', () => this.openModal(params.data, this.EditLocationModalID));
							let createChild = document.createElement('a');
							createChild.innerHTML = `<a class="btn btn-primary btn-s mr-10 mb-10 fa fa-plus" title="Add warehouse hotel"></a>`;
							createChild.addEventListener('click', () => this.openModal(params.data, this.NewWarehouseModalID));
							divWrap.appendChild(createChild);
							divWrap.appendChild(edit);
							divWrap.className = 'actions';
							return divWrap;
						}
						
					}
				}
			]
		}
	};

	constructor(props: Props & InjectedIntlProps) {
		super(props);
		this.createLocation = this.createLocation.bind(this);
		this.createWarehouseHotel = this.createWarehouseHotel.bind(this);
		this.editLocation = this.editLocation.bind(this);
	}

	componentDidMount() {
		this.props.getLocationsList(this.props.companyID);
	}

	openModal(location: Location, modal: string) {
		// SAVE
		this.setState({
			location: location
		});
		// OPEN THE MODAL
		$(`#${modal}`).modal('show');
	}

	newLocationObject(location: PostLocationForm) {
		// VARS
		let locationDetails = returnGoogleLocationDetails(location.address);
		// CHANGE VALUES
		let newLocation: PostLocationBody = {
			companyID: this.props.companyID,
			address: locationDetails.address,
			city: locationDetails.city,
			country: locationDetails.country,
			email: location.email,
			name: location.name,
			phoneNumber: location.phoneNumber,
			zipCode: locationDetails.zipCode,
			latitude: locationDetails.latitude,
			longitude: locationDetails.longitude
		};
		if (location.id) {
			newLocation.locationID = location.id;
		}
		return newLocation;
	}

	createLocation(location: PostLocationForm) {
		// NEW OBJECT
		let newLocation: PostLocationBody = this.newLocationObject(location);
		// SEND THE DATA
		return this.props.postCreateLocation(newLocation).then(() => {
			// NOTE
			bigBox({
				title: this.props.intl.formatMessage({ id: 'scenes.locations.createLocation.title' }),
				content: this.props.intl.formatMessage({ id: 'scenes.locations.createLocation.content' }),
				color: '#739E73',
				timeout: 15000,
				icon: 'fa fa-check'
			});
			// UPDATE CUSTOMER LIST
			this.props.getLocationsList(this.props.companyID);
		});
	}

	createWarehouseHotel(location: PostLocationForm) {
		// NEW OBJECT
		let newLocation: PostLocationBody = this.newLocationObject(location);
		// SEND THE DATA
		return this.props.postCreateWarehouseHotel(newLocation).then(() => {
			// NOTE
			bigBox({
				title: this.props.intl.formatMessage({ id: 'scenes.locations.createWarehouseHotel.title' }),
				content: this.props.intl.formatMessage({ id: 'scenes.locations.createWarehouseHotel.content' }),
				color: '#739E73',
				timeout: 15000,
				icon: 'fa fa-check'
			});
			// UPDATE CUSTOMER LIST
			this.props.getLocationsList(this.props.companyID);
		});
	}

	editLocation(location: PostLocationForm) {
		// NEW OBJECT
		let newLocation: PostLocationBody = this.newLocationObject(location);
		// SEND THE DATA
		return this.props.postEditLocation(newLocation).then(() => {
			// NOTE
			bigBox({
				title: this.props.intl.formatMessage({ id: 'scenes.locations.editLocation.title' }),
				content: this.props.intl.formatMessage({ id: 'scenes.locations.editLocation.content' }),
				color: '#739E73',
				timeout: 15000,
				icon: 'fa fa-check'
			});
			// UPDATE CUSTOMER LIST
			this.props.getLocationsList(this.props.companyID);
		});
	}

	render() {
		let rowData = (this.props.locationsList && this.props.locationsList.locations) ? this.props.locationsList.locations : [];
		return (
			<div className="page-layout people-page">
				<div className="container-fluid">
					<div className="row mb-15">
						<div className="col-sm-12 text-right">
							<GridTools modalID={this.NewLocationModalID} gridOptions={this.state.gridOptions} />
						</div>
					</div>
					<div className="row mb-15">
						<div className="col-sm-12">
							<div className="grid-st1 ag-theme-balham">
								<AgGridTheme rowData={rowData} gridOptions={this.state.gridOptions} />
							</div>
						</div>
					</div>
				</div>
				<NewLocationModal modalID={this.NewLocationModalID} handleSubmit={this.createLocation} loaderStatus={this.props.locationModalFetchStatus} locationData={this.state.location} />
				<NewLocationModal modalID={this.NewWarehouseModalID} handleSubmit={this.createWarehouseHotel} loaderStatus={this.props.locationModalFetchStatus} locationData={this.state.location} />
				<EditLocationModal modalID={this.EditLocationModalID} handleSubmit={this.editLocation} loaderStatus={this.props.locationModalFetchStatus} locationData={this.state.location} />
				<Loader status={this.props.locationsFetchStatus} />
			</div>
		);
	}
}

export default injectIntl(LocationsTemplate);

interface State {
	location: Location | null;
	gridOptions: GridOptions;
}
interface Actions {
	getLocationsList(CompanyID: string);
	postCreateLocation(location: PostLocationBody);
	postCreateWarehouseHotel(location: PostLocationBody);
	postEditLocation(location: PostLocationBody);
}
interface Props extends Actions {
	companyID: string;
	locationsList: GetLocationsListPayload;
	locationsFetchStatus: boolean;
	locationModalFetchStatus: boolean;
}
