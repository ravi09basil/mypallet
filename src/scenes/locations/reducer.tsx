import { ActionType } from '../../config/types';
import { combineReducers } from 'redux';
import C from './constants';

function locationsList(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.LOCATIONS_FETCH_DATA_SUCCESS:
			return action.payload;
		case C.LOCATIONS_FETCH_DATA_FAILURE:
			return null;
		default:
			return state;
	}
}

function locationsFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.PAGE_FETCH_DATA_STATUS:
			return action.payload;
		default:
			return state;
	}
}

function locationModalFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.LOCATION_MODAL_FETCH_DATA_STATUS:
			return action.payload;
		default:
			return state;
	}
}

const People = {
	locationsList,
	locationsFetchStatus,
	locationModalFetchStatus
};

export default combineReducers(People);
