import { API } from '../../services/api';
import apisUrls from '../../config/apiUrls';
import { GetLocationsListPayload, PostLocationBody, PostSuccessPayload } from './types';

export class LocationSrvc {
	API: API;

	constructor() {
		this.API = new API();
	}

	getLocationsList(CompanyID: string): Promise<GetLocationsListPayload> {
		return this.API.GET(apisUrls.GetLocations, {
			queryString: { CompanyID }
		});
	}

	postCreateLocation(payload: PostLocationBody): Promise<PostSuccessPayload> {
		return this.API.POST(apisUrls.PostCreateLocation, {
			body: JSON.stringify(payload)
		});
	}

	postCreateWarehouseHotel(payload: PostLocationBody): Promise<PostSuccessPayload> {
		return this.API.POST(apisUrls.PostCreateWarehouseHotel, {
			body: JSON.stringify(payload)
		});
	}

	postEditLocation(payload: PostLocationBody): Promise<PostSuccessPayload> {
		return this.API.POST(apisUrls.PostEditLocation, {
			body: JSON.stringify(payload)
		});
	}
}
