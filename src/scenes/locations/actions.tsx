import C from './constants';
import { LocationSrvc } from './services';
import { PostLocationBody } from './types';

export const getLocationsList = (CompanyID: string) => dispatch => {
	// VARS
	let srvc: LocationSrvc = new LocationSrvc();
	// LOADING STATUS
	dispatch({
		type: C.PAGE_FETCH_DATA_STATUS,
		payload: true
	});
	// SEND
	return srvc.getLocationsList(CompanyID).then(res => {
		// SAVE DATA
		dispatch({
			type: C.LOCATIONS_FETCH_DATA_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.PAGE_FETCH_DATA_STATUS,
			payload: false
		});
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.LOCATIONS_FETCH_DATA_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.PAGE_FETCH_DATA_STATUS,
			payload: false
		});
	});
};

export const createLocation = (location: PostLocationBody) => dispatch => {
	// VARS
	let srvc: LocationSrvc = new LocationSrvc();
	// LOADING STATUS
	dispatch({
		type: C.LOCATION_MODAL_FETCH_DATA_STATUS,
		payload: true
	});
	// SEND
	return srvc.postCreateLocation(location).then(res => {
		// LOADING STATUS
		dispatch({
			type: C.LOCATION_MODAL_FETCH_DATA_STATUS,
			payload: false
		});
	}).catch(error => {
		// LOADING STATUS
		dispatch({
			type: C.LOCATION_MODAL_FETCH_DATA_STATUS,
			payload: false
		});
	});
};
export const createWarehouseHotel = (location: PostLocationBody) => dispatch => {
	// VARS
	let srvc: LocationSrvc = new LocationSrvc();
	// LOADING STATUS
	dispatch({
		type: C.LOCATION_MODAL_FETCH_DATA_STATUS,
		payload: true
	});
	// SEND
	return srvc.postCreateWarehouseHotel(location).then(res => {
		// LOADING STATUS
		dispatch({
			type: C.LOCATION_MODAL_FETCH_DATA_STATUS,
			payload: false
		});
	}).catch(error => {
		// LOADING STATUS
		dispatch({
			type: C.LOCATION_MODAL_FETCH_DATA_STATUS,
			payload: false
		});
	});
};

export const editLocation = (location: PostLocationBody) => dispatch => {
	// VARS
	let srvc: LocationSrvc = new LocationSrvc();
	// LOADING STATUS
	dispatch({
		type: C.LOCATION_MODAL_FETCH_DATA_STATUS,
		payload: true
	});
	// SEND
	return srvc.postEditLocation(location).then(res => {
		// LOADING STATUS
		dispatch({
			type: C.LOCATION_MODAL_FETCH_DATA_STATUS,
			payload: false
		});
	}).catch(error => {
		// LOADING STATUS
		dispatch({
			type: C.LOCATION_MODAL_FETCH_DATA_STATUS,
			payload: false
		});
	});
};
