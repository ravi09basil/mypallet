import * as React from 'react';
import './style.scss';
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl';

const Page404 = (p) => {
	return (
		<div className="page-layout notfound-page">
			<div className="container-fluid">
				<div className="row">
					<div className="col-sm-12">
						<div className="text-center error-box">
							<h1 className="error-text-2 bounceInDown animated">
								<FormattedMessage id="scenes.404.title" />
								<span className="particle particle--c" />
								<span className="particle particle--a" />
								<span className="particle particle--b" />
							</h1>
							<h2 className="font-xl">
								<strong>
									<i className="fa fa-fw fa-warning fa-lg text-warning" />
									<FormattedHTMLMessage id="scenes.404.subtitle" />
								</strong>
							</h2>
							<br />
							<p className="lead">
								<FormattedHTMLMessage id="scenes.404.body" />
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Page404;
