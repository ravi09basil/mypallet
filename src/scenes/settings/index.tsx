import { connect } from 'react-redux';
import { getAdminUsers, getCompanyInfo, createAdminUserFile, removeAdminUser, changeCompanyLogoFile, resetPassword, editAdministrationUser } from './actions';
import { CreateAdministrationUserBody, ChangeCompanyLogoBody, EditAdministrationUserBody } from './types';
import SettingsTemplate from './comp';

const mapDispatchToProps = (dispatch): Actions => ({
	getCompanyInfo(CompanyID: string) {
		return dispatch(getCompanyInfo(CompanyID));
	},
	getAdministrationUsers(CompanyID: string) {
		return dispatch(getAdminUsers(CompanyID));
	},
	postCreateAdminUserWithImage(file: File, payload: CreateAdministrationUserBody) {
		return dispatch(createAdminUserFile(file, payload));
	},
	postRemoveAdminUser(CompanyID: string, userID: string) {
		return dispatch(removeAdminUser(CompanyID, userID));
	},
	postChangeCompanyLogo(image: File, payload: ChangeCompanyLogoBody) {
		return dispatch(changeCompanyLogoFile(image, payload));
	},
	postResetPassword(userID: string) {
		return dispatch(resetPassword(userID));
	},
	postEditAdministrationUser(user: EditAdministrationUserBody) {
		return dispatch(editAdministrationUser(user));
	}
});

const mapStateToProps = (state) => {
	return {
		'companyID': state.Login.authentication.companyID,
		'adminUsersList': state.Settings.adminUsers,
		'pageSettingsFetchStatus': state.Settings.pageSettingsFetchStatus,
		'companyInfo': state.Settings.companyInfo,
		'companyInfoFetchStatus': state.Settings.companyInfoFetchStatus,
		'adminUserModalFetchStatus': state.Settings.adminUserModalFetchStatus,
		'removeAdminUserFetchStatus': state.Settings.removeAdminUserFetchStatus
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingsTemplate);

type Actions = {
	getCompanyInfo(CompanyID: string);
	getAdministrationUsers(CompanyID: string);
	postCreateAdminUserWithImage(file: File, payload: CreateAdministrationUserBody);
	postRemoveAdminUser(CompanyID: string, userID: string);
	postChangeCompanyLogo(image: File, payload: ChangeCompanyLogoBody);
	postResetPassword(userID: string);
	postEditAdministrationUser(user: EditAdministrationUserBody);
};
