import C from './constants';
import { SettingsSrvc } from './services';
import { CreateAdministrationUserBody, ChangeCompanyLogoBody, EditAdministrationUserBody } from './types';
import { AzureURL, azureCreateBlobFromBrowserFile, conatiners } from '../../services/azure';

export const removeAdminUser = (CompanyID: string, userID: string) => dispatch => {
	// VARS
	let srvc: SettingsSrvc = new SettingsSrvc();
	// LOADING STATUS
	dispatch({
		type: C.REMOVE_ADMIN_USER_FETCH_STATUS,
		payload: true
	});
	// SEND
	return srvc.postRemoveAppUser(CompanyID, userID).then(res => {
		// SAVE DATA
		dispatch({
			type: C.REMOVE_ADMIN_USER_FETCH_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.REMOVE_ADMIN_USER_FETCH_STATUS,
			payload: false
		});
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.REMOVE_ADMIN_USER_FETCH_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.REMOVE_ADMIN_USER_FETCH_STATUS,
			payload: false
		});
	});
};

export const getAdminUsers = (CompanyID) => dispatch => {
	// VARS
	let srvc: SettingsSrvc = new SettingsSrvc();
	// LOADING STATUS
	dispatch({
		type: C.PAGE_SETTINGS_FETCH_STATUS,
		payload: true
	});
	// SEND
	return srvc.getAdministrationUsers(CompanyID).then(res => {
		// SAVE
		dispatch({
			type: C.ADMIN_USERS_FETCH_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.PAGE_SETTINGS_FETCH_STATUS,
			payload: false
		});
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.ADMIN_USERS_FETCH_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.PAGE_SETTINGS_FETCH_STATUS,
			payload: false
		});
	});
};

export const getCompanyInfo = (CompanyID) => dispatch => {
	// VARS
	let srvc: SettingsSrvc = new SettingsSrvc();
	// LOADING STATUS
	dispatch({
		type: C.COMPANY_INFO_FETCH_STATUS,
		payload: true
	});
	// SEND
	return srvc.getCompanyInfo(CompanyID).then(res => {
		// SAVE
		dispatch({
			type: C.COMPANY_INFO_FETCH_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.COMPANY_INFO_FETCH_STATUS,
			payload: false
		});
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.COMPANY_INFO_FETCH_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.COMPANY_INFO_FETCH_STATUS,
			payload: false
		});
	});
};

export const createAdminUser = (payload: CreateAdministrationUserBody) => (dispatch) => {
	// VARS
	let srvc: SettingsSrvc = new SettingsSrvc();
	// LOADING STATUS
	dispatch({
		type: C.ADMIN_USER_MODAL_FETCH_STATUS,
		payload: true
	});
	// SEND
	return srvc.postCreateAdministrationUser(payload).then(res => {
		// SAVE
		dispatch({
			type: C.CREATE_ADMIN_USER_FETCH_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.ADMIN_USER_MODAL_FETCH_STATUS,
			payload: false
		});
		return res;
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.CREATE_ADMIN_USER_FETCH_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.ADMIN_USER_MODAL_FETCH_STATUS,
			payload: false
		});
		return error;
	});
};

export const createAdminUserFile = (image: File, payload: CreateAdministrationUserBody) => dispatch => {
	// VARS
	let imageName = payload.username;
	// LOADING STATUS
	dispatch({
		type: C.ADMIN_USER_MODAL_FETCH_STATUS,
		payload: true
	});
	// CREATE USER IMAGE
	return new Promise((resolve, reject) => {
		// VARS
		let container = conatiners.userscontainer;
		let callback = (error: boolean) => {
			if (error) {
				// LOADING STATUS
				dispatch({
					type: C.ADMIN_USER_MODAL_FETCH_STATUS,
					payload: false
				});
				// ERROR
				reject('Faild in uploading the image to the server !!');
			} else {
				// NEW IMAGE URL
				payload.profileImagePath = AzureURL(imageName, container);
				return resolve(createAdminUser(payload)(dispatch));
			}
			return false;
		};
		azureCreateBlobFromBrowserFile(container, imageName, image, callback);
	});
};

export const changeCompanyLogo = (payload: ChangeCompanyLogoBody) => (dispatch) => {
	// VARS
	let srvc: SettingsSrvc = new SettingsSrvc();
	// LOADING STATUS
	dispatch({
		type: C.PAGE_SETTINGS_FETCH_STATUS,
		payload: true
	});
	// SEND
	return srvc.postChangeCompanyLogo(payload.companyID, payload.imagePath).then(res => {
		// SAVE
		dispatch({
			type: C.CHANGE_COMPANY_LOGO_FETCH_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.PAGE_SETTINGS_FETCH_STATUS,
			payload: false
		});
		return res;
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.CHANGE_COMPANY_LOGO_FETCH_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.PAGE_SETTINGS_FETCH_STATUS,
			payload: false
		});
		return error;
	});
};

export const changeCompanyLogoFile = (image: File, payload: ChangeCompanyLogoBody) => dispatch => {
	// VARS
	let imageName = image.name;
	// LOADING STATUS
	dispatch({
		type: C.PAGE_SETTINGS_FETCH_STATUS,
		payload: true
	});
	// CREATE USER IMAGE
	return new Promise((resolve, reject) => {
		// VARS
		let container = conatiners.companyLogo;
		let callback = (error: boolean) => {
			if (error) {
				// LOADING STATUS
				dispatch({
					type: C.PAGE_SETTINGS_FETCH_STATUS,
					payload: false
				});
				// ERROR
				reject('Faild in uploading the image to the server !!');
			} else {
				// NEW IMAGE URL
				payload.imagePath = AzureURL(imageName, container);
				return resolve(changeCompanyLogo(payload)(dispatch));
			}
			return false;
		};
		azureCreateBlobFromBrowserFile(container, imageName, image, callback);
	});
};

export const resetPassword = (userID: string) => dispatch => {
	// VARS
	let srvc: SettingsSrvc = new SettingsSrvc();
	// LOADING STATUS
	dispatch({
		type: C.ADMIN_USER_MODAL_FETCH_STATUS,
		payload: true
	});
	// SEND
	return srvc.postResetPassword(userID).then(res => {
		// SAVE
		dispatch({
			type: C.RESET_PASS_ADMIN_USER_FETCH_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.ADMIN_USER_MODAL_FETCH_STATUS,
			payload: false
		});
		return res;
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.RESET_PASS_ADMIN_USER_FETCH_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.ADMIN_USER_MODAL_FETCH_STATUS,
			payload: false
		});
		return error;
	});
};

export const editAdministrationUser = (user: EditAdministrationUserBody) => dispatch => {
	// VARS
	let srvc: SettingsSrvc = new SettingsSrvc();
	// LOADING STATUS
	dispatch({
		type: C.ADMIN_USER_MODAL_FETCH_STATUS,
		payload: true
	});
	// SEND
	return srvc.postEditAdministrationUser(user).then(res => {
		// SAVE
		dispatch({
			type: C.UPDATE_ADMIN_USER_FETCH_SUCCESS,
			payload: res
		});
		// LOADING STATUS
		dispatch({
			type: C.ADMIN_USER_MODAL_FETCH_STATUS,
			payload: false
		});
		return res;
	}).catch(error => {
		// ERROR
		dispatch({
			type: C.UPDATE_ADMIN_USER_FETCH_FAILURE,
			payload: error
		});
		// LOADING STATUS
		dispatch({
			type: C.ADMIN_USER_MODAL_FETCH_STATUS,
			payload: false
		});
		return error;
	});
};
