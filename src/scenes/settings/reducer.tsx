import { ActionType } from '../../config/types';
import { combineReducers } from 'redux';
import C from './constants';

function adminUsers(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.ADMIN_USERS_FETCH_SUCCESS:
			return action.payload;
		case C.ADMIN_USERS_FETCH_FAILURE:
			return null;
		default:
			return state;
	}
}

function pageSettingsFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.PAGE_SETTINGS_FETCH_STATUS:
			return action.payload;
		default:
			return state;
	}
}

function companyInfo(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.COMPANY_INFO_FETCH_SUCCESS:
			return action.payload;
		case C.COMPANY_INFO_FETCH_FAILURE:
			return null;
		default:
			return state;
	}
}

function companyInfoFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.COMPANY_INFO_FETCH_STATUS:
			return action.payload;
		default:
			return state;
	}
}

function createAdminUser(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.CREATE_ADMIN_USER_FETCH_SUCCESS:
			return action.payload;
		case C.CREATE_ADMIN_USER_FETCH_FAILURE:
			return null;
		default:
			return state;
	}
}

function adminUserModalFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.ADMIN_USER_MODAL_FETCH_STATUS:
			return action.payload;
		default:
			return state;
	}
}

function removeAdminUser(state: object | null = null, action: ActionType) {
	switch (action.type) {
		case C.REMOVE_ADMIN_USER_FETCH_SUCCESS:
			return action.payload;
		case C.REMOVE_ADMIN_USER_FETCH_FAILURE:
			return null;
		default:
			return state;
	}
}

function removeAdminUserFetchStatus(state: boolean = false, action: ActionType) {
	switch (action.type) {
		case C.REMOVE_ADMIN_USER_FETCH_STATUS:
			return action.payload;
		default:
			return state;
	}
}

const Settings = {
	adminUsers,
	pageSettingsFetchStatus,
	companyInfo,
	companyInfoFetchStatus,
	createAdminUser,
	adminUserModalFetchStatus,
	removeAdminUser,
	removeAdminUserFetchStatus
};

export default combineReducers(Settings);
