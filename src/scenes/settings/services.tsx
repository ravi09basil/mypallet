import { API } from '../../services/api';
import apisUrls from '../../config/apiUrls';
import { GetAdministrationUsersPayload, GetCompanyInfoPayload, CreateAdministrationUserBody, SuccessfullPayload, EditAdministrationUserBody } from './types';

export class SettingsSrvc {
	API: API;

	constructor() {
		this.API = new API();
	}

	getAdministrationUsers(CompanyID: string): Promise<GetAdministrationUsersPayload> {
		return this.API.GET(apisUrls.GetAdministrationUsers, {
			queryString: { CompanyID }
		});
	}

	getCompanyInfo(CompanyID: string): Promise<GetCompanyInfoPayload> {
		return this.API.GET(apisUrls.GetCompanyInfo, {
			queryString: { CompanyID }
		});
	}

	postCreateAdministrationUser(payload: CreateAdministrationUserBody): Promise<SuccessfullPayload> {
		return this.API.POST(apisUrls.PostCreateAdministrationUser, {
			body: JSON.stringify(payload)
		});
	}

	postRemoveAppUser(companyID: string, userID: string): Promise<SuccessfullPayload> {
		return this.API.POST(apisUrls.PostRemoveAdministrationUser, {
			body: JSON.stringify({
				companyID,
				userID
			})
		});
	}

	postChangeCompanyLogo(companyID: string, imagePath: string): Promise<SuccessfullPayload> {
		return this.API.POST(apisUrls.PostChangeCompanyLogo, {
			body: JSON.stringify({
				companyID,
				imagePath
			})
		});
	}

	postResetPassword(userID: string) {
		return this.API.POST(apisUrls.PostResetAdministrationUserPassword, {
			body: JSON.stringify({
				userID
			})
		});
	}

	postEditAdministrationUser(payload: EditAdministrationUserBody) {
		return this.API.POST(apisUrls.PostEditAdministrationUser, {
			body: JSON.stringify({
				payload
			})
		});
	}
}
