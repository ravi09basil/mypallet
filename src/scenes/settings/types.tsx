export interface GetAdministrationUsersPayload {
	administrationUsers: AdminUser[];
	successfull: true;
	message: string;
}

export interface AdminUser {
	id: string;
	name: string;
	username: string;
	password: string;
	email: string;
	profileImage: string;
	shouldChangePassword: boolean;
}

export interface GetCompanyInfoPayload {
	company: {
		name: string;
		logo: string;
		address: string;
		postCode: string;
		city: string;
		vatNumber: string;
		phoneNumber: string
	};
	successfull: true;
	message: string;
}

export interface CreateAdministrationUserBody {
	name: string;
	username: string;
	email: string;
	profileImagePath: string;
	companyID: string;
}

export interface ChangeCompanyLogoBody {
	companyID: string;
	imagePath: string;
}

export interface SuccessfullPayload {
	successfull: boolean;
	message: string;
}

export interface EditAdministrationUserBody {
	userID: string;
	profileImagePath: string;
	email: string;
	phonenumber: string;
	name: string;
}

export interface EditUserFormBody {
	name: string;
	username: string;
	email: string;
	phoneNumber: string;
	profileImagePath: string;
}
