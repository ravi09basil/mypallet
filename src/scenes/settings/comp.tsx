import * as React from 'react';
import { GridOptions, ICellRendererParams } from 'ag-grid';
import { InjectedIntlProps, FormattedMessage, injectIntl } from 'react-intl';
import Loader from '../../components/loader';
import GridTools from '../../components/gridTools';
import AgGridTheme from '../../components/agGridTheme';
import CompanyInfoCard from '../../components/companyInfoCard';
import ConfirmationModal from '../../components/confirmationModal';
import { bigBox } from '../../components/smartadmin/utils/actions/MessageActions';
import { NewAdminUserModal, EditAdminUserModal } from '../../components/adminUserModal';
import { GetAdministrationUsersPayload, GetCompanyInfoPayload, CreateAdministrationUserBody, AdminUser, ChangeCompanyLogoBody, EditAdministrationUserBody, EditUserFormBody } from './types';

class SettingsTemplate extends React.Component<Props & InjectedIntlProps> {
	RemoveUserModalID = 'RemoveUserModalID';
	NewAdminUserModalID = 'NewAdminUserModalID';
	EditAdminUserModalID = 'EditAdminUserModalID';
	state: State = {
		userData: null,
		gridOptions: {
			rowHeight: 55,
			enableSorting: true,
			onGridReady: () => {
				if (this.state.gridOptions.columnApi) {
					setTimeout(() => { if (this.state.gridOptions.api) { this.state.gridOptions.api.resetRowHeights(); if (this.state.gridOptions.columnApi) { this.state.gridOptions.columnApi.autoSizeAllColumns(); } if (this.state.gridOptions.api) { this.state.gridOptions.api.sizeColumnsToFit(); } } }, 500);
				}
			},
			columnDefs: [
				{
					headerName: this.props.intl.formatMessage({ id: 'general.name' }),
					field: 'profileImage',
					autoHeight: 80,
					cellRenderer: (params: ICellRendererParams) => {
						let p = document.createElement('p');
						let divWrap = document.createElement('div');
						p.innerHTML = `<p><img class="user-image" src="${params.value}" alt=''/> ${params.data.name}</p>`;
						divWrap.appendChild(p);
						divWrap.className = 'text-left';
						return divWrap;
					}
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.form.username' }),
					field: 'username'
				},
				{
					headerName: this.props.intl.formatMessage({ id: 'general.form.email' }),
					field: 'email'
				},
				{
					autoHeight: 1,
					headerName: this.props.intl.formatMessage({ id: 'general.functions' }),
					field: 'isAwaiting',
					cellRenderer: (params: ICellRendererParams) => {
						let divWrap = document.createElement('div');
						let remove = document.createElement('a');
						if (this.state.gridOptions.rowData) {
							if (this.state.gridOptions.rowData.length > 1) {
								console.log('>1');
								remove.innerHTML = `<a class="btn btn-danger btn-s ml-10 fa fa-minus-square"></a>`;
								remove.addEventListener('click', () => this.openUserModal(params.data, this.RemoveUserModalID));
							}
							if (this.state.gridOptions.rowData.length <= 1) {
								console.log('<=1');
								remove.innerHTML = `<a class="btn btn-danger disabled btn-s ml-10 fa fa-minus-square"></a>`;
							}
						}
						// console.log(params);
						// remove.innerHTML = `<a class="btn btn-danger btn-s ml-10 fa fa-minus-square"></a>`;
						// remove.innerHTML = `<a class="btn btn-danger btn-s ml-10">${this.props.intl.formatMessage({ id: 'general.delete' })}</a>`;
						// remove.addEventListener('click', () => this.openUserModal(params.data, this.RemoveUserModalID));
						divWrap.appendChild(remove);
						let edit = document.createElement('a');
						edit.innerHTML = `<a class="btn btn-primary btn-s ml-10 fa fa-edit"></a>`;
						// remove.innerHTML = `<a class="btn btn-danger btn-s ml-10">${this.props.intl.formatMessage({ id: 'general.delete' })}</a>`;
						edit.addEventListener('click', () => this.openUserModal(params.data, this.EditAdminUserModalID));
						divWrap.appendChild(edit);
						divWrap.className = 'actions';
						return divWrap;
					}
				}
			]
		}
	};

	constructor(props: Props & InjectedIntlProps) {
		super(props);
		this.resetValue = this.resetValue.bind(this);
		this.removeUser = this.removeUser.bind(this);
		this.createAdminUser = this.createAdminUser.bind(this);
		this.changeCompanyLogo = this.changeCompanyLogo.bind(this);
		this.resetPassword = this.resetPassword.bind(this);
		this.updateAdministrationUserInfo = this.updateAdministrationUserInfo.bind(this);
	}

	componentDidMount() {
		this.props.getCompanyInfo(this.props.companyID);
		this.props.getAdministrationUsers(this.props.companyID);
	}

	createAdminUser(payload: CreateAdministrationUserBody) {
		// VARS
		let profileImageInput = ($('#profileImagePath')[0] as HTMLInputElement);
		var profileImageInputFiles = profileImageInput.files;
		if (profileImageInputFiles) {
			// CHANGE THE COMPANY ID
			payload.companyID = this.props.companyID;
			// SEND THE DATA
			return this.props.postCreateAdminUserWithImage(profileImageInputFiles[0], payload).then((res) => {
				if (res.successfull) {
					// NOTE
					bigBox({
						title: this.props.intl.formatMessage({ id: 'scenes.settings.createNewAdminUser.title' }),
						content: this.props.intl.formatMessage({ id: 'scenes.settings.createNewAdminUser.content' }, { name: payload.username }),
						color: '#739E73',
						timeout: 15000,
						icon: 'fa fa-check'
					});
					// UPDATE THE LIST
					this.props.getAdministrationUsers(this.props.companyID);
				}
			});
		}
	}

	resetPassword() {
		if (this.state.userData) {
			return this.props.postResetPassword(this.state.userData.id).then((res) => {
				if (res.successfull) {
					// NOTE
					bigBox({
						title: this.props.intl.formatMessage({ id: 'scenes.settings.resetPassword.title' }),
						content: this.props.intl.formatMessage({ id: 'scenes.settings.resetPassword.content' }),
						color: '#739E73',
						timeout: 15000,
						icon: 'fa fa-check'
					});
				}
			});
		}
	}

	updateAdministrationUserInfo(user: EditUserFormBody) {
		if (this.state.userData) {
			let newUser: EditAdministrationUserBody = {
				email: user.email,
				name: user.name,
				phonenumber: user.phoneNumber,
				profileImagePath: user.profileImagePath,
				userID: this.state.userData.id
			};
			return this.props.postEditAdministrationUser(newUser);
		}
	}

	openUserModal(user: AdminUser, modal: string) {
		// SAVE THE USER
		this.setState({
			userData: user
		});
		// OPEN THE MODAL
		$(`#${modal}`).modal('show');
	}

	removeUser() {
		if (this.state.userData) {
			// REMOVE USER
			this.props.postRemoveAdminUser(this.props.companyID, this.state.userData.id).then(() => {
				// UPDATE THE LIST
				this.props.getAdministrationUsers(this.props.companyID);
			}).then(() => {
				// HIDE THE MODAL
				$(`#${this.RemoveUserModalID}`).modal('hide');
			});
		}
	}

	resetValue(e: any) {
		e.target.value = '';
	}

	changeCompanyLogo(event: any) {
		// VARS
		console.log(this.props.companyID);

		let payload = {
			companyID: this.props.companyID,
			imagePath: ''
		};
		let companyLogoInput = (event.target as HTMLInputElement);
		var companyLogoInputFiles = companyLogoInput.files;
		// CHECK
		if (companyLogoInputFiles) {
			// IMAGE PATH
			let image = companyLogoInputFiles[0];
			// SEND THE DATA
			return this.props.postChangeCompanyLogo(image, payload).then((res) => {
				if (res.successfull) {
					// NOTE
					bigBox({
						title: this.props.intl.formatMessage({ id: 'scenes.settings.changeCompanyLogo.title' }),
						content: this.props.intl.formatMessage({ id: 'scenes.settings.changeCompanyLogo.content' }),
						color: '#739E73',
						timeout: 15000,
						icon: 'fa fa-check'
					});
				}
			});
		} else {
			alert('File API not allowed in this browser !!');
		}
	}

	render() {
		let rowData = (this.props.adminUsersList && this.props.adminUsersList.administrationUsers) ? this.props.adminUsersList.administrationUsers : [];
		let companyInfo = (this.props.companyInfo) ? this.props.companyInfo.company : false;
		let userData = (this.state.userData) ? this.state.userData : false;
		// let rowDataCount = rowData.length;
		return (
			<div className="page-layout settings-page">
				<div className="container-fluid">
					<div className="row mb-15">
						<div className="col-sm-6">
							{companyInfo ? <CompanyInfoCard {...companyInfo} /> : ''}
						</div>
						<div className="col-sm-6 text-right">
							<GridTools modalID={this.NewAdminUserModalID} gridOptions={this.state.gridOptions} />
							<a className="ico-btn color-black">
								<i className="fa fa-file-image-o" /> <span className="txt"> <FormattedMessage id="scenes.settings.changeLogo" /></span>
								<input type="file" onClick={this.resetValue} onChange={this.changeCompanyLogo} className="hide-file-input" accept="image/*" />
							</a>
						</div>
					</div>
					<div className="row mb-15">
						<div className="col-sm-12">
							<div className="grid-st1 ag-theme-balham">
								<AgGridTheme rowData={rowData} gridOptions={this.state.gridOptions} />
							</div>
						</div>
					</div>
				</div>
				<ConfirmationModal
					modalID={this.RemoveUserModalID}
					title={this.props.intl.formatMessage({ id: 'scenes.settings.deleteAdminModal.title' })}
					cancelTxt={this.props.intl.formatMessage({ id: 'general.cancel' })}
					confirmTxt={this.props.intl.formatMessage({ id: 'general.ok' })}
					loaderStatus={this.props.removeAdminUserFetchStatus}
					confirmEvent={this.removeUser}
				>
					<h1><FormattedMessage id="general.deleteConfirmation" values={{ name: (userData) ? userData.username : '' }} /></h1>
				</ConfirmationModal>
				<NewAdminUserModal modalID={this.NewAdminUserModalID} loaderStatus={this.props.adminUserModalFetchStatus} handleSubmit={this.createAdminUser} />
				<EditAdminUserModal modalID={this.EditAdminUserModalID} loaderStatus={this.props.adminUserModalFetchStatus} handleSubmit={this.updateAdministrationUserInfo} userData={userData} resetPassword={this.resetPassword} />
				<Loader status={this.props.pageSettingsFetchStatus} />
			</div>
		);
	}
}

export default injectIntl(SettingsTemplate);

interface State {
	userData: AdminUser | null;
	gridOptions: GridOptions;
}
interface Actions {
	getCompanyInfo(companyID: string);
	getAdministrationUsers(companyID: string);
	postCreateAdminUserWithImage(file: File, payload: CreateAdministrationUserBody);
	postRemoveAdminUser(CompanyID: string, userID: string);
	postChangeCompanyLogo(image: File, payload: ChangeCompanyLogoBody);
	postResetPassword(userID: string);
	postEditAdministrationUser(user: EditAdministrationUserBody);
}
interface Props extends Actions {
	companyID: string;
	adminUsersList: GetAdministrationUsersPayload;
	pageSettingsFetchStatus: boolean;
	companyInfo: GetCompanyInfoPayload;
	companyInfoFetchStatus: boolean;
	adminUserModalFetchStatus: boolean;
	removeAdminUserFetchStatus: boolean;
}
