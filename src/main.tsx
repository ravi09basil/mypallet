import * as React from 'react';
import { connect } from 'react-redux';
import { IntlProvider } from 'react-intl';
import RoutesComp from './components/RoutesComp';
import { languages, flattenMessages } from './config/localization';
import './styles/style.scss';

class Main extends React.Component<Props> {
	render() {
		return (
			<IntlProvider locale={this.props.currentLanguage} messages={flattenMessages(languages[this.props.currentLanguage])}>
				<RoutesComp />
			</IntlProvider>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		currentLanguage: state.Language.currentLanguage
	};
};

export default connect(mapStateToProps)(Main);

interface Props {
	currentLanguage: string;
}
