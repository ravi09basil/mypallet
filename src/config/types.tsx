// ACTION TYPE
export interface ActionType {
	type: string;
	payload: object | string | boolean | object;
}
