let API_ROOT = 'https://mypalletlive.azurewebsites.net/api';

if (location.hostname.includes('mypalletcustomerdev')) {
	API_ROOT = 'https://mypalletdevelopment.azurewebsites.net/api';
}
if (location.hostname.includes('localhost')) {
	API_ROOT = 'https://mypalletdevelopment.azurewebsites.net/api';
	 // API_ROOT = 'http://localhost:5000/api';
}
const API_URLS = {
	GetLicenseInfo: `${API_ROOT}/License/GetLicenseInfo`,
	PostAuthenticateAdministrationUser: `${API_ROOT}/Authentication/AuthenticateAdministrationUser`,
	GetAdministrationUserInfo: `${API_ROOT}/User/GetAdministrationUserInfo`,
	GetAppUsers: `${API_ROOT}/AppUser/GetAppUsers`,
	PostCreateAppUser: `${API_ROOT}/AppUser/CreateAppUser`,
	PostRemoveAppUser: `${API_ROOT}/AppUser/RemoveAppUser`,
	GetAggregatedStock: `${API_ROOT}/Stock/GetAggregatedStock`,
	GetStockList: `${API_ROOT}/Stock/GetStockList`,
	PostSavePriceInformationForProduct: `${API_ROOT}/Stock/SavePriceInformationForProduct`,
	GetCompanyTransactionsHistory: `${API_ROOT}/Transaction/GetCompanyTransactionsHistory`,
	GetReceipts: `${API_ROOT}/Transaction/GetReceipts`,
	GetTransactionInfo: `${API_ROOT}/Transaction/GetTransactionInfo`,
	PostChangeCompanyLogo: `${API_ROOT}/Company/ChangeCompanyLogo`,
	GetCompanyInfo: `${API_ROOT}/Company/GetCompanyInfo`,
	PostCreateLocation: `${API_ROOT}/Location/CreateLocation`,
	PostCreateWarehouseHotel: `${API_ROOT}/Location/CreateWarehouseHotel`,
	PostEditLocation: `${API_ROOT}/Location/EditLocation`,
	GetLocations: `${API_ROOT}/Location/GetLocations`,
	GetLocationsForCustomer: `${API_ROOT}/Location/GetLocationsForCustomer`,
	PostChangeAdministrationUserPassword: `${API_ROOT}/User/ChangeAdministrationUserPassword`,
	PostCreateAdministrationUser: `${API_ROOT}/User/CreateAdministrationUser`,
	PostRemoveAdministrationUser: `${API_ROOT}/User/RemoveAdministrationUser`,
	PostEditAdministrationUser: `${API_ROOT}/User/EditAdministrationUser`,
	GetAdministrationUsers: `${API_ROOT}/User/GetAdministrationUsers`,
	PostResetAdministrationUserPassword: `${API_ROOT}/User/ResetAdministrationUserPassword`,
	GetCustomers: `${API_ROOT}/Customer/GetCustomers`,
	PostCreateCustomer: `${API_ROOT}/Customer/CreateCustomer`,
	PostChangeCustomer: `${API_ROOT}/Customer/ChangeCustomer`,
	GetCustomerInfo: `${API_ROOT}/Customer/GetCustomerInfo`,
	GetCheckVATNumber: `${API_ROOT}/Customer/CheckVATNumber`,
	PostUpdatePalletBankStatus: `${API_ROOT}/Customer/UpdatePalletBankStatus`,
	PostSendReceipt: `${API_ROOT}/Transaction/SendReceipt`,
};

export default API_URLS;
