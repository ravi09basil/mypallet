import { createStore, applyMiddleware } from 'redux';
// https://www.npmjs.com/package/redux-save-state
import saveState from 'redux-save-state/localStorage';
import rootReducer from './rootReducer';
import { appStateKey, getUserState } from '../services/appState';
import thunkMiddleware from 'redux-thunk';
import loggerMiddleware from 'redux-logger';

const store = applyMiddleware(
	saveState(appStateKey, {
		filter: (state) => {
			state.Login.authenticationFetchStatus = false;
			return {
				Login: state.Login,
				Language: state.Language
			};
		}
	}),
	thunkMiddleware,
	loggerMiddleware
)(createStore)(rootReducer, getUserState());

export default store;
