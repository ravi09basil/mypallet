import en from './langs/en';
import da from './langs/da';

export const languages = {
	en,
	da
};

export const flattenMessages = ((nestedMessages, prefix = '') => {
	if (nestedMessages === null) {
		return {};
	}
	let O = {};
	let F = (messages, key) => {
		const value = nestedMessages[key];
		const prefixedKey = prefix ? `${prefix}.${key}` : key;
		if (typeof value === 'string') {
			Object.assign(messages, { [prefixedKey]: value });
		} else {
			Object.assign(messages, flattenMessages(value, prefixedKey));
		}
		return messages;
	};
	return Object.keys(nestedMessages).reduce(F, O);
});

export const getLanguages = (): string[] => {
	return Object.keys(languages);
};
