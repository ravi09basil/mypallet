import C from './constants';

export const logout = () => ({
	type: C.LOGOUT,
	payload: null
});

export const changeLanguage = (lan) => ({
	type: C.CHANGE_LANGUAGE,
	payload: lan
});
