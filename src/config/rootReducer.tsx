import { combineReducers } from 'redux';
import C from './constants';
import Login from '../scenes/login/reducer';
import ChangePassword from '../scenes/changePassword/reducer';
import Stock from '../scenes/stock/reducer';
import Receipts from '../scenes/receipts/reducer';
import Customers from '../scenes/customers/reducer';
import Customer from '../scenes/customer/reducer';
import People from '../scenes/people/reducer';
import Locations from '../scenes/locations/reducer';
import Settings from '../scenes/settings/reducer';
import Language from './reducer';

export default (state, action) => {
	if (action.type === C.LOGOUT) {
		state = undefined;
	}
	return combineReducers({
		Login,
		ChangePassword,
		Stock,
		Receipts,
		Customers,
		Customer,
		People,
		Locations,
		Settings,
		Language
	})(state, action);
};
