import { ActionType } from './types';
import { combineReducers } from 'redux';
import C from './constants';

function currentLanguage(state: string | null = 'en', action: ActionType) {
	switch (action.type) {
		case C.CHANGE_LANGUAGE:
			return action.payload;
		default:
			return state;
	}
}

const Language = {
	currentLanguage,
};

export default combineReducers(Language);
