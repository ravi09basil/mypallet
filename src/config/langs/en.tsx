export default {
	general: {
		functions: 'Functions',
		open: 'Open',
		palletBank: 'Pallet Bank',
		customerMigrationTitle: 'Change customer',
		customer: 'Customer',
		sender: 'Sender',
		reciever: 'Reciever',
		locationFilter: 'Location Filter',
		allLocations: 'All locations',
		resetPass: 'Reset Password',
		empty: 'Empty',
		edit: 'Edit',
		cancel: 'Cancel',
		ok: 'Ok',
		create: 'Create',
		close: 'Close',
		change: 'Change',
		createNew: 'Create New',
		print: 'Print',
		extract: 'Extract',
		gathered: 'Account Overview',
		overview: 'Receipts',
		flashback: 'History',
		customersettings: 'Customer settings',
		name: 'Name',
		received: 'Received',
		delivered: 'Delivered',
		total: 'Total',
		selectAll: 'Select all',
		info: 'Info',
		transactionID: 'Transaction ID',
		company: 'Company',
		land: 'Land',
		receivingUserName: 'Receiving Username',
		time: 'Time',
		date: 'Date',
		typeA: 'Type A',
		typeB: 'Type B',
		typeC: 'Type C',
		palletypes: 'Pallet Types',
		selectPeriod: 'Select period',
		selectADate: 'Select a date',
		contactPerson: 'Contact person',
		address: 'Address',
		city: 'City',
		country: 'Country',
		phoneNumber: 'Phonenumber',
		usesmyPallet: 'Uses myPallet',
		status: 'Status',
		sum: 'Sum',
		showAll: 'Show all',
		receivable: 'Account Receivable',
		owes: 'Account Payable',
		newCustomers: 'New customers',
		search: 'Search',
		postCode: 'Post Code',
		login: 'Login',
		ref: 'Reference',
		pending: 'Pending',
		active: 'Active',
		form: {
			name: 'Name',
			username: 'Username',
			password: 'Password',
			address: 'Address',
			email: 'Email',
			adresse: 'Adresse',
			postCode: 'Post Code',
			city: 'City',
			country: 'Country',
			phoneNumber: {
				label: 'Phonenumber',
				placeholder: 'Start typing a phone number'
			},
			vatNumber: 'Vat number',
			profileImage: 'Profile Image',
			userType: 'User type',
			userTypeOptions: {
				driver: 'Driver',
				warehouse: 'Warehouse',
				shippingAgent: 'ShippingAgent'
			},
			zipCode: 'Zip Code',
			contactPerson: 'Contact person',
		},
		validation: {
			name: 'Name is required',
			username: 'Username is required',
			password: 'Password is required',
			email: 'Email is required',
			profileImagePath: 'Profile image is required',
			adresse: {
				required: 'Adresse is required',
				notFound: 'The address could not be found'
			},
			postCode: 'Post Code is required',
			city: 'City is required',
			country: 'Country is required',
			phoneNumber: 'Phonenumber is required',
			vatNumber: 'Vat number is required',
			userType: 'User type is required',
			address: 'Address is required',
			zipCode: 'Zip Code is required',
			contactPerson: 'Contact person is required',
		},
		delete: 'Delete',
		deleteConfirmation: 'Are you sure you want to delete "{name}" ?',
		searchFields: 'Search the fields',
		roles: {
			driver: 'Driver',
			warehouse: 'Warehouse',
		},
	},
	navigation: {
		dashboard: 'DASHBOARD',
		stock: 'STOCK',
		customers: 'CUSTOMERS',
		receipts: 'RECEIPTS',
		people: 'PEOPLE',
		locations: 'LOCATIONS',
		gdpr: 'GDPR',
		settings: 'ADMINISTRATION'
	},
	comps: {
		enterEmail: {
			submitButton: 'Send receipt',
			title: 'E-mail receipt'
		},
		infoCard: {
			seeConsignmentNote: 'See Consignment Note'
		},
		newAdminUserModal: {
			title: 'Create admin user',
			submitButton: 'Create'
		},
		editAdminUserModal: {
			title: 'Edit admin user',
			submitButton: 'Update'
		},
		newCustomerModal: {
			title: 'Create new customer'
		},
		changeCustomerModal: {
			title: 'Change customer'
		},
		newUserModal: {
			title: 'Create new user'
		},
		newLocationModalID: {
			title: 'Create Location',
			submitButton: 'Create'
		},
		newWarehouseModalID: {
			title: 'Create Warehouse under "{name}"',
			submitButton: 'Create'
		},
		editLocationModalID: {
			title: 'Edit Location',
			submitButton: 'Update'
		},
		transactionInfoModal: {
			title: 'Transaction info',
			columns: {
				product: 'Product',
				received: 'Received',
				delivered: 'Delivered',
				pictures: 'Pictures',
			},
			cards: {
				receiverTitle: 'Receiver',
				senderTitle: 'Sender',
				note: {
					title: 'Note',
					defaultText: 'There was no note attached to Transaction'
				},
				consignmentNote: {
					title: 'Consignment Note',
					defaultText: 'There was no Consignment Note attached to Transaction'
				},
				trailerNumber: {
					title: 'Trailer Number',
					defaultText: 'There was no Trailer number registered to Transaction'
				},
				registrationNumber: {
					title: 'Registration Number',
					defaultText: 'There was no Registration number registered to Transaction'
				},
				imageCard: {
					title: 'Product images',
					seeProductImage: 'See product image',
					defaultText: 'There was no Product images attached to transaction'
				},
				signaturePath: {
					title: 'Signature',
					openSignature: 'Open Signature'
				}
			}
		}
	},
	scenes: {
		404: {
			title: 'Error 404',
			subtitle: 'Page <u>Not</u> Found',
			body: 'The page you requested could not be found, either contact your webmaster or try again. Use your browsers <b>Back</b> button to navigate to the page you have prevously come from'
		},
		login: {
			title: 'Sign In'
		},
		changePassword: {
			title: 'Change password',
			validation: {
				newPassword: 'new password is required field',
				confirmPass: {
					required: 'confirm password is required field',
					match: 'Confirm password should match with the new password'
				}
			},
			form: {
				newPass: 'New Password',
				confPass: 'Confirm Password'
			}
		},
		locations: {
			createLocation: {
				title: 'Location created',
				content: 'The location is created now and we will refresh the list'
			},
			createWarehouseHotel: {
				title: 'Lokation oprettet',
				content: 'Lokationen er oprettet og listen opdateres'
			},
			editLocation: {
				title: 'Location edited',
				content: 'The location is edited now and we will refresh the list'
			}
		},
		customers: {
			newCustomer: {
				title: 'Customer created',
				content: 'The customer is created now and we will refresh the list.'
			}
		},
		customer: {
			customerSettingsTab: {
				userHasPalletBank: 'Customer has Pallet Bank',
				updatedPalletBankSuccessfullTitle: 'Status updated',
				updatedPalletBankSuccessfullMessage: 'Pallet Bank Status was updated'
			}
		},
		dashboard: {
			welcome: 'Welcome to myPallet'
		},
		settings: {
			deleteAdminModal: {
				title: 'Deleting admin user'
			},
			createNewAdminUser: {
				title: 'User created',
				content: '`Username: {name} is created.'
			},
			changeCompanyLogo: {
				title: 'Company logo',
				content: 'Company logo changed successfully'
			},
			changeLogo: 'Change logo',
			resetPassword: {
				title: 'Reset Password',
				content: 'The user will receive an email for new password'
			}
		},
		people: {
			deleteUserModal: {
				title: 'Deleting user'
			},
			createNewUser: {
				title: 'User created',
				content: '`The User is created now and we will refresh the list'
			}
		},
		stock: {
			totalValueInnDKK: 'Total value in DKK'
		},
		gdpr: `<h1>GENEREL FORORDNING OM DATABESKYTTELSE</h1>
		<p>Det er vigtigt for myPallet, at vores kunder, og deres medarbejder er trygge ved, vores h&aring;ndtering og opbevaring af, og anvendelse af brugerens persondata.</p>
		<p>Vi har stor respekt for brugerens privatliv, og derfor opfylder vi alle g&aelig;ldende krav p&aring; omr&aring;det. Det er vores politik, at h&aring;ndtering af persondata, er af h&oslash;jeste priotet.</p>
		<p>Vi opbevarer og bruger virksomhedens/brugerens persondata til l&oslash;bende administration af:</p>
		<p>Virksomhedernes tjeneste og abonnementer som de har hos os</p>
		<p>Virksomhedernes og dennes brugers registreringer i systemet</p>
		<p>&nbsp;</p>
		<p>Vi opbevarer og anvender kun brugerens persondata i det omfang og den periode, som det er n&oslash;dvendigt ift. form&aring;let med det. De virksomheder som anvender myPallet, og deres medarbejder har selv adgang til den opbevaret data.
			myPallet kan og vil ved samtykke fra virksomheden og deres/den ansatte slette opbevaret data.</p>
		<p>myPallet &nbsp;er dataansvarlig for dine persondata, og dermed er myPallet &nbsp;ansvarlig for opbevaringen og brugen af brugerens persondata. Dette g&aelig;lder ogs&aring; i de tilf&aelig;lde,
			hvor vi bruger dem sammen med vores samarbejdspartnere. Ved nogle af de till&aelig;gstjenester, som virksomheden kan k&oslash;be hos os, er vi ikke dataansvarlige. Hvis det er tilf&aelig;ldet,
	st&aring;r dette tydeligt beskrevet i vores forretningsbetingelser.<br />
			<br />
			Brugerens personoplysninger stammer fra de oplysninger, som virksomheden har givet os i forbindelse med oprettelsen af virksomhedens aftale eller ved virksomhedens k&oslash;b af vores produkter.
			Derudover er der en r&aelig;kke personoplysninger, som f.eks. trafikdata, der indsamles i forbindelse med brugerens brug af myPallets tjenester og produkter.</p>
		<p>Vi gemmer oplysninger, som vi har f&aring;et fra virksomheden, da virksomheden oprettede virksomhedens aftale, og de er vores kunde.</p>
		<p>Oplysninger, som vi indhenter fra tredjeparter, vil typisk v&aelig;re oplysninger til kreditvurderinger, som vi f&aring;r fra uafh&aelig;ngige.</p>
		<p><strong>Hvilke oplysninger indsamler myPallet</strong><br />Vi indsamler f&oslash;lgende oplysninger:</p>
		<ol>
			<li>
				Personlige oplysninger. N&aring;r du foretager en registering / emballagebyt via vores appss. Her opbevarer vi den lokation som den anvendte smartphone/tablet har sendyt til os. Dette er basis
				de bruger som er logget p&aring; systemet. Disse data kan blandt andet indeholde:
				<ol>
					<li>Din arbejdsgivers detaljer</li>
					<li>Nummerplade p&aring; din tr&aelig;kker</li>
					<li>Nummerplade p&aring; din trailer</li>
					<li>GEOlokation fra din smartphone</li>
					<li>De billeder som du uploader</li>
				</ol>
			</li>
			<li>Hvis &nbsp;du f.eks. tilmelder dig vores nyhedsbrev eller deltager i en konkurrence, s&aring; indsamler vi de oplysninger, som du selv indtaster. Det kan v&aelig;re navn og adresse,
				f&oslash;dselsdato, e-mail og telefonnumre.</li>
			<li>Hvis du opretter dig som bruger, og betaler via via mypallet.dk. Disse betalingsoplysninger ved k&oslash;b af hardware, abonnementer og ydelser, vil ligeledes blive gemt i vores
				database&nbsp;- det er oplysninger om dit betalingskort - herunder kortnummer, udl&oslash;bsdato og CVC-kode.</li>
			<li>Oplysninger om brug af hjemmesiden. Oplysninger om, hvordan du navigerer rundt p&aring; hjemmesiden, og hvilke dele af hjemmesiden, du bes&oslash;ger samt tekniske oplysninger om din browser
				og dit styresystem. Dette indsamles via webanalysev&aelig;rkt&oslash;jerne Google Analytics og Adform.</li>
		</ol>
		<p><strong>Dine personlige oplysninger, er DINE personlige oplysninger<br />></strong> Personlige oplysninger anvendes kun, hvis du er kunde hos os og har en registeret brugerprofil hos myPallet.DK. Vi
			bruger oplysningerne til at levere m&aring;lrettet indhold og m&aring;lrettede tilbud samt til at give dig korrekte informationer og services. Vi gemmer dine personlige oplysninger for at kunne
			sikre et h&oslash;jtsikkerhedes niveau af vores system. Det vil sige, at mist&aelig;nkelige forhold, kan sikre virksomhederne et let adgang til opf&oslash;lgning herom.</p>
		<p>&nbsp;Personlige oplysninger indsamlet i forbindelse med en konkurrence bliver dog kun brugt til at udtr&aelig;kke vindere, fremsende pr&aelig;mier og til statistiske form&aring;l, med mindre
			andet udtrykkeligt er angivet. Herefter slettes de.</p>
		<p>Betalingsoplysninger ved k&oslash;b af hardware og abonnementer h&aring;ndteres gennem det autoriserede Dansk InternetBetalingsSystem (DIBS). N&aring;r du indtaster dit kortnummer,
			udl&oslash;bsdato og CVC i vores webshop, overf&oslash;res disse data til DIBS i krypteret form. Krypteringen giver en h&oslash;j sikkerhed for, at uvedkommende ikke kan skaffe sig adgang til
			oplysningerne. Vi gemmer ikke betalingsoplysninger (kortnummer, udl&oslash;bsdato og CVC-kode).</p>
		<p>Oplysninger om, hvilke sider p&aring; hjemmesiden du bes&oslash;ger, bruges til at tilpasse hjemmesiden til dine behov, f.eks. ved at vise tilbud, som vi vurderer, kan v&aelig;re relevante for
			dig. Oplysningerne bliver ogs&aring; brugt til at forbedre hjemmesiden generelt.</p>
		<p>Vi kontakter dig ikke elektronisk med henblik p&aring; markedsf&oslash;ring og brugerunders&oslash;gelser, medmindre du har givet dit samtykke hertil, eller det p&aring; anden vis er tilladt efter
			lovgivningen.</p>
		<p>Det kan dog forekomme, at myPallet.dk systemer, udsender informationer p&aring; vegne af vores kunder. Systemet kan benyttes som en portal indenfor transport og logistik &ndash; og firmaer der
			&oslash;nsker at anvender systemet til udsendelse af nyhedsbreve tilrettet mod mypallet.dk kunder, kan tilk&oslash;be sig denne rettighed. myPallet APS videregiveer ikke markedsf&oslash;rings
			relateret oplysninger.</p>`
	}
};
