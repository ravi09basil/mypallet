﻿export default {
	general: {
		functions: 'Funktioner',
		open: 'Åben',
		palletBank: 'Pallebank',
		customerMigrationTitle: 'Ændre kunde',
		customer: 'Kunde',
		sender: 'Afsender',
		reciever: 'Modtager',
		locationFilter: 'Lokations filter',
		allLocations: 'Alle steder',
		resetPass: 'Nulstille kodeord',
		empty: 'Tom',
		edit: 'Redigere',
		cancel: 'Afbryd',
		ok: 'Okay',
		create: 'Opret',
		close: 'Luk',
		change: 'Ændre',
		createNew: 'Opret ny',
		print: 'Print',
		extract: 'Udtræk',
		gathered: 'Konto oversigt',
		overview: 'Kvitteringer',
		flashback: 'Historik',
		customersettings: 'Kunde opsætning',
		name: 'Navn',
		received: 'Modtaget',
		delivered: 'Leveret',
		total: 'Total',
		selectAll: 'Vælg alle',
		info: 'Info',
		transactionID: 'Transaktions ID',
		company: 'Selskab',
		land: 'Land',
		receivingUserName: 'Modtager brugernavn',
		time: 'Tid',
		date: 'Dato',
		typeA: 'Type A',
		typeB: 'Type B',
		typeC: 'Type C',
		palletypes: 'Palletyper',
		selectPeriod: 'Vælg periode',
		selectADate: 'Vælg en dato',
		contactPerson: 'Kontaktperson',
		address: 'Adresse',
		city: 'By',
		country: 'Land',
		phoneNumber: 'Telefonnummer',
		usesmyPallet: 'Anvender myPallet',
		status: 'Status',
		sum: 'Sum',
		showAll: 'Vis alt',
		receivable: 'Debitorer',
		owes: 'Kreditorer',
		newCustomers: 'Nye kunder',
		search: 'Søg',
		postCode: 'Postnummer',
		login: 'Log på',
		ref: 'Reference',
		pending: 'Afventer',
		active: 'Aktiv',
		form: {
			name: 'Navn',
			username: 'Brugernavn',
			password: 'Kodeord',
			address: 'Adresse',
			email: 'Email',
			adresse: 'Adresse',
			postCode: 'Post nummer',
			city: 'By',
			country: 'Land',
			phoneNumber: {
				label: 'Telefonnummer',
				placeholder: 'Skriv telefon nummer'
			},
			vatNumber: 'CVR',
			profileImage: 'Profil billed',
			userType: 'Bruger type',
			userTypeOptions: {
				driver: 'Chauffør',
				warehouse: 'Lagerpersonale',
				shippingAgent: 'Shipper'
			},
			zipCode: 'Post nummer',
			contactPerson: 'Kontaktperson',
		},
		validation: {
			name: 'Navn er påkrævet',
			username: 'Brugernavn er påkrævet',
			password: 'Kodeord er påkrævet',
			email: 'Email er påkrævet',
			profileImagePath: 'Profilbilled er påkrævet',
			adresse: {
				required: 'Adresse er påkrævet',
				notFound: 'Adresse ikke fundet'
			},
			postCode: 'Post nummer påkrævet',
			city: 'By er påkrævet',
			country: 'Lan er påkrævet',
			phoneNumber: 'Telefon nummer er påkrævet',
			vatNumber: 'Cvr nummer er påkrævet',
			userType: 'Brugertype er påkrævet',
			address: 'Adresse er påkrævet',
			zipCode: 'Post nummer er påkrævet',
			contactPerson: 'Kontaktperson er påkrævet'
		},
		delete: 'Slet',
		deleteConfirmation: 'Er du sikker på du vil slette "{name}" ?',
		searchFields: 'Søg i felter',
		roles: {
			driver: 'Chauffør',
			warehouse: 'Lagermand',
		},
	},
	navigation: {
		dashboard: 'DASHBOARD',
		stock: 'LAGER',
		customers: 'KUNDER',
		receipts: 'KVITTERINGER',
		people: 'ANSATTE',
		locations: 'LOKATIONER',
		gdpr: 'GDPR',
		settings: 'ADMINISTRATION'
	},
	comps: {
		enterEmail: {
			submitButton: 'Send kvittering',
			title: 'E-mail kvittering'
		},
		infoCard: {
			seeConsignmentNote: 'Se forsendelses Note'
		},
		newAdminUserModal: {
			title: 'Opret admin bruger',
			submitButton: 'Opret'
		},
		editAdminUserModal: {
			title: 'Rediger admin bruger',
			submitButton: 'Updater'
		},
		newCustomerModal: {
			title: 'Opret ny kunde'
		},
		changeCustomerModal: {
			title: 'Skift kunde'
		},
		newUserModal: {
			title: 'Opret ny bruger'
		},
		newLocationModalID: {
			title: 'Opret Lokation',
			submitButton: 'Opret'
		},
		newWarehouseModalID: {
			title: 'Opret Warehouse under "{name}"',
			submitButton: 'Opret'
		},
		editLocationModalID: {
			title: 'Rediger Location',
			submitButton: 'Updater'
		},
		transactionInfoModal: {
			title: 'Transaktion info',
			columns: {
				product: 'Produkt',
				received: 'Modtaget',
				delivered: 'Afleveret',
				pictures: 'Billed',
			},
			cards: {
				receiverTitle: 'Modtager',
				senderTitle: 'Afsender',
				note: {
					title: 'Note',
					defaultText: 'Ingen note vedhæftet transaktionen'
				},
				consignmentNote: {
					title: 'Forsendelses note',
					defaultText: 'Ingen forsendelses note vedhæftet transaktionen'
				},
				trailerNumber: {
					title: 'Trailer Nummer',
					defaultText: 'Intet trailer nummer registreret på transaktionen'
				},
				registrationNumber: {
					title: 'Registrerings Nummer',
					defaultText: 'Intet registrerings nummer registreret på transaktionen'
				},
				imageCard: {
					title: 'Produkt billed',
					seeProductImage: 'Se produktbilled',
					defaultText: 'Intet billede vedhæftet transaktionen'
				},
				signaturePath: {
					title: 'Underskrift',
					openSignature: 'Åben signatur'
				}
			}
		}
	},
	scenes: {
		404: {
			title: 'Error 404',
			subtitle: 'Side <u>ikke</u> fundet',
			body: 'Siden kunne ikke findes, prøv igen eller kontakt wenmaster. Brug browsers <b>Tilbge</b> knappen for at komme tilbage'
		},
		login: {
			title: 'Log ind'
		},
		changePassword: {
			title: 'Skift kode',
			validation: {
				newPassword: 'Nyt kodeord er påkrævet',
				confirmPass: {
					required: 'Bekræft kodeord er påkrævet',
					match: 'Sikre dig at kodeordene er ens'
				}
			},
			form: {
				newPass: 'nyt kodeord',
				confPass: 'Bekræft kodeord'
			}
		},
		locations: {
			createLocation: {
				title: 'Lokation oprettet',
				content: 'Lokationen er oprettet og listen opdateres'
			},
			createWarehouseHotel: {
				title: 'Lokation oprettet',
				content: 'Lokationen er oprettet og listen opdateres'
			},
			editLocation: {
				title: 'Lokation redigeret',
				content: 'Placeringen er oprettet nu, og vi opdaterer listen'
			}
		},
		customers: {
			newCustomer: {
				title: 'Bruger oprettet',
				content: 'Brugeren er oprettet og listen opdateres.'
			}
		},
		customer: {
			customerSettingsTab: {
				userHasPalletBank: 'Kunden har pallebank',
				updatedPalletBankSuccessfullTitle: 'Status opdateret',
				updatedPalletBankSuccessfullMessage: 'Pallebanks Status blev opdateret'
			}
		},
		dashboard: {
			welcome: 'Velkommen til myPallet'
		},
		settings: {
			deleteAdminModal: {
				title: 'Sletter admin bruger'
			},
			createNewAdminUser: {
				title: 'Bruger oprettet',
				content: '`Brugernavn: {name} er oprettet.'
			},
			changeCompanyLogo: {
				title: 'Firma logo',
				content: 'Firma logo ændret'
			},
			changeLogo: 'Ændre logo',
			resetPassword: {
				title: 'Nulstil kodeord',
				content: 'Brugeren vil modtage en email for ny kode'
			}
		},
		people: {
			deleteUserModal: {
				title: 'Sletter bruger'
			},
			createNewUser: {
				title: 'Bruger oprettet',
				content: '`Brugeren er oprettet og listen opdateres'
			}
		},
		stock: {
			totalValueInnDKK: 'Total værdi i DKK'
		},
		gdpr: `<h1>GENEREL FORORDNING OM DATABESKYTTELSE</h1>
		<p>Det er vigtigt for myPallet, at vores kunder, og deres medarbejder er trygge ved, vores h&aring;ndtering og opbevaring af, og anvendelse af brugerens persondata.</p>
		<p>Vi har stor respekt for brugerens privatliv, og derfor opfylder vi alle g&aelig;ldende krav p&aring; omr&aring;det. Det er vores politik, at h&aring;ndtering af persondata, er af h&oslash;jeste priotet.</p>
		<p>Vi opbevarer og bruger virksomhedens/brugerens persondata til l&oslash;bende administration af:</p>
		<p>Virksomhedernes tjeneste og abonnementer som de har hos os</p>
		<p>Virksomhedernes og dennes brugers registreringer i systemet</p>
		<p>&nbsp;</p>
		<p>Vi opbevarer og anvender kun brugerens persondata i det omfang og den periode, som det er n&oslash;dvendigt ift. form&aring;let med det. De virksomheder som anvender myPallet, og deres medarbejder har selv adgang til den opbevaret data.
			myPallet kan og vil ved samtykke fra virksomheden og deres/den ansatte slette opbevaret data.</p>
		<p>myPallet &nbsp;er dataansvarlig for dine persondata, og dermed er myPallet &nbsp;ansvarlig for opbevaringen og brugen af brugerens persondata. Dette g&aelig;lder ogs&aring; i de tilf&aelig;lde,
			hvor vi bruger dem sammen med vores samarbejdspartnere. Ved nogle af de till&aelig;gstjenester, som virksomheden kan k&oslash;be hos os, er vi ikke dataansvarlige. Hvis det er tilf&aelig;ldet,
	st&aring;r dette tydeligt beskrevet i vores forretningsbetingelser.<br />
			<br />
			Brugerens personoplysninger stammer fra de oplysninger, som virksomheden har givet os i forbindelse med oprettelsen af virksomhedens aftale eller ved virksomhedens k&oslash;b af vores produkter.
			Derudover er der en r&aelig;kke personoplysninger, som f.eks. trafikdata, der indsamles i forbindelse med brugerens brug af myPallets tjenester og produkter.</p>
		<p>Vi gemmer oplysninger, som vi har f&aring;et fra virksomheden, da virksomheden oprettede virksomhedens aftale, og de er vores kunde.</p>
		<p>Oplysninger, som vi indhenter fra tredjeparter, vil typisk v&aelig;re oplysninger til kreditvurderinger, som vi f&aring;r fra uafh&aelig;ngige.</p>
		<p><strong>Hvilke oplysninger indsamler myPallet</strong><br />Vi indsamler f&oslash;lgende oplysninger:</p>
		<ol>
			<li>
				Personlige oplysninger. N&aring;r du foretager en registering / emballagebyt via vores appss. Her opbevarer vi den lokation som den anvendte smartphone/tablet har sendyt til os. Dette er basis
				de bruger som er logget p&aring; systemet. Disse data kan blandt andet indeholde:
				<ol>
					<li>Din arbejdsgivers detaljer</li>
					<li>Nummerplade p&aring; din tr&aelig;kker</li>
					<li>Nummerplade p&aring; din trailer</li>
					<li>GEOlokation fra din smartphone</li>
					<li>De billeder som du uploader</li>
				</ol>
			</li>
			<li>Hvis &nbsp;du f.eks. tilmelder dig vores nyhedsbrev eller deltager i en konkurrence, s&aring; indsamler vi de oplysninger, som du selv indtaster. Det kan v&aelig;re navn og adresse,
				f&oslash;dselsdato, e-mail og telefonnumre.</li>
			<li>Hvis du opretter dig som bruger, og betaler via via mypallet.dk. Disse betalingsoplysninger ved k&oslash;b af hardware, abonnementer og ydelser, vil ligeledes blive gemt i vores
				database&nbsp;- det er oplysninger om dit betalingskort - herunder kortnummer, udl&oslash;bsdato og CVC-kode.</li>
			<li>Oplysninger om brug af hjemmesiden. Oplysninger om, hvordan du navigerer rundt p&aring; hjemmesiden, og hvilke dele af hjemmesiden, du bes&oslash;ger samt tekniske oplysninger om din browser
				og dit styresystem. Dette indsamles via webanalysev&aelig;rkt&oslash;jerne Google Analytics og Adform.</li>
		</ol>
		<p><strong>Dine personlige oplysninger, er DINE personlige oplysninger<br />></strong> Personlige oplysninger anvendes kun, hvis du er kunde hos os og har en registeret brugerprofil hos myPallet.DK. Vi
			bruger oplysningerne til at levere m&aring;lrettet indhold og m&aring;lrettede tilbud samt til at give dig korrekte informationer og services. Vi gemmer dine personlige oplysninger for at kunne
			sikre et h&oslash;jtsikkerhedes niveau af vores system. Det vil sige, at mist&aelig;nkelige forhold, kan sikre virksomhederne et let adgang til opf&oslash;lgning herom.</p>
		<p>&nbsp;Personlige oplysninger indsamlet i forbindelse med en konkurrence bliver dog kun brugt til at udtr&aelig;kke vindere, fremsende pr&aelig;mier og til statistiske form&aring;l, med mindre
			andet udtrykkeligt er angivet. Herefter slettes de.</p>
		<p>Betalingsoplysninger ved k&oslash;b af hardware og abonnementer h&aring;ndteres gennem det autoriserede Dansk InternetBetalingsSystem (DIBS). N&aring;r du indtaster dit kortnummer,
			udl&oslash;bsdato og CVC i vores webshop, overf&oslash;res disse data til DIBS i krypteret form. Krypteringen giver en h&oslash;j sikkerhed for, at uvedkommende ikke kan skaffe sig adgang til
			oplysningerne. Vi gemmer ikke betalingsoplysninger (kortnummer, udl&oslash;bsdato og CVC-kode).</p>
		<p>Oplysninger om, hvilke sider p&aring; hjemmesiden du bes&oslash;ger, bruges til at tilpasse hjemmesiden til dine behov, f.eks. ved at vise tilbud, som vi vurderer, kan v&aelig;re relevante for
			dig. Oplysningerne bliver ogs&aring; brugt til at forbedre hjemmesiden generelt.</p>
		<p>Vi kontakter dig ikke elektronisk med henblik p&aring; markedsf&oslash;ring og brugerunders&oslash;gelser, medmindre du har givet dit samtykke hertil, eller det p&aring; anden vis er tilladt efter
			lovgivningen.</p>
		<p>Det kan dog forekomme, at myPallet.dk systemer, udsender informationer p&aring; vegne af vores kunder. Systemet kan benyttes som en portal indenfor transport og logistik &ndash; og firmaer der
			&oslash;nsker at anvender systemet til udsendelse af nyhedsbreve tilrettet mod mypallet.dk kunder, kan tilk&oslash;be sig denne rettighed. myPallet APS videregiveer ikke markedsf&oslash;rings
			relateret oplysninger.</p>`
	}
};
