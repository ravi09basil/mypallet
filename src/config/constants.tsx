export const C = {
	LOGOUT: 'LOGOUT',
	CHANGE_LANGUAGE: 'CHANGE_LANGUAGE'
};

export default C;
