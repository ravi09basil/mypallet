import store from './config/store';
import Login from './scenes/login';
import ChangePassword from './scenes/changePassword';
import Dashboard from './scenes/dashboard';
import Stock from './scenes/stock';
import Customers from './scenes/customers';
import Customer from './scenes/customer';
import Receipts from './scenes/receipts';
import People from './scenes/people';
import Locations from './scenes/locations';
import Gdpr from './scenes/gdpr';
import Settings from './scenes/settings';
import Page404 from './scenes/404';
import Receipt from './scenes/receipt';

let state = store.getState();
let ROUTES = {
	LOGIN: {
		comp: Login,
		name: 'login',
		url: '/login',
		accessible: true,
		showInNav: false,
		needAuth: false
	},
	CHANGE_PASSWORD: {
		comp: ChangePassword,
		name: 'change_password',
		url: '/change-password',
		accessible: true,
		showInNav: false,
		needAuth: false
	},
	DASHBOARD: {
		comp: Dashboard,
		name: 'dashboard',
		url: '/dashboard',
		accessible: true,
		showInNav: true,
		needAuth: true
	},
	STOCK: {
		comp: Stock,
		name: 'stock',
		url: '/stock',
		accessible: true,
		showInNav: true,
		needAuth: true
	},
	CUSTOMERS: {
		comp: Customers,
		name: 'customers',
		url: '/customers',
		accessible: true,
		showInNav: true,
		needAuth: true
	},
	CUSTOMER: {
		comp: Customer,
		name: 'customer',
		url: '/customer/:id',
		accessible: true,
		showInNav: false,
		needAuth: true
	},
	RECEIPTS: {
		comp: Receipts,
		name: 'receipts',
		url: '/receipts',
		accessible: true,
		showInNav: true,
		needAuth: true
	},
	PEOPLE: {
		comp: People,
		name: 'people',
		url: '/people',
		accessible: true,
		showInNav: true,
		needAuth: true
	},
	LOCATIONS: {
		comp: Locations,
		name: 'locations',
		url: '/locations',
		accessible: true,
		showInNav: true,
		needAuth: true
	},
	GDPR: {
		comp: Gdpr,
		name: 'gdpr',
		url: '/gdpr',
		accessible: true,
		showInNav: true,
		needAuth: true
	},
	SETTINGS: {
		comp: Settings,
		name: 'settings',
		url: '/settings',
		accessible: true,
		showInNav: true,
		needAuth: true
	},
	Page404: {
		comp: Page404,
		name: 'page404',
		url: '',
		accessible: true,
		showInNav: false,
		needAuth: false
	},
	RECEIPT: {
		comp: Receipt,
		name: 'receipt',
		url: '/receipt/:id',
		accessible: true,
		showInNav: false,
		needAuth: false,
	}
};

let licenseInfo: any = (state.Login.licenseInfo) ? state.Login.licenseInfo : null;

if (licenseInfo) {
	let licenseInfos = licenseInfo.licenseInfos;
	let license = licenseInfos[0];
	if (!(license.license === 0 && license.hasLicense)) {
		ROUTES.STOCK.showInNav = false;
		ROUTES.STOCK.accessible = false;
	}
}
export default ROUTES;
